﻿namespace TVSI.Web.ChangeSP.Lib.Constant
{
    public class StaffStatusConst
    {
        public const int HOAT_DONG = 10; // Hoạt động
        public const int NGUNG_HOAT_DONG = 99; // Ngừng hoạt động
    }
}