﻿namespace TVSI.Web.ChangeSP.Lib.Constant
{
    public class CustomerLevelCodeConst
    {
        public const int NORMAL = 1; // NORMAL
        public const int DIAMOND = 2; // DIAMOND
        public const int PLATINUM = 3; // PLATINUM
        public const int GOLD = 4; // GOLD
        public const int SILVER = 5; // SILVER
    }
}