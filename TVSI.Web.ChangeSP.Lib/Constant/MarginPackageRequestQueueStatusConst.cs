﻿namespace TVSI.Web.ChangeSP.Lib.Constant
{
    public class MarginPackageRequestQueueStatusConst
    {
        public const int KHOI_TAO = 10; // Khởi tạo
        public const int CHO_XU_LY = 20; // Chờ xử lý
        public const int DANG_XU_LY = 30; // Đang xử lý
        public const int THANH_CONG = 40; // Thành công
        public const int THAT_BAI = 50; // Thất bại
    }
}