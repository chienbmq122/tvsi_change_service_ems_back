﻿namespace TVSI.Web.ChangeSP.Lib.Constant
{
    public class VipStatusConst
    {
        public const int HOAT_DONG = 1; // Hoạt động
        public const int NGUNG_HOAT_DONG = 99; // Ngừng hoạt động
    }
}