﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Common.DAL;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Service.WebService;
using TVSI.Web.ChangeSP.Lib.Service.WebService.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class ChangeUserServicePackagePreferential
    {
        public List<UserServicePackageInfoModel> SearchUserServicePackList(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                var service = new GetEEWebService();

                Logger.Info(typeof(ChangeUserServicePackagePreferential)," sql SearchUserServicePackList - " + accountNo + userName + curUser + curStatus + fromDate + toDate + currentpack + packnew);
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    var sql = "select b.ngay_mo_tai_khoan,a.* from TVSI_DOI_GOI_DICH_VU_UU_DAI a  left join TVSI_THONG_TIN_TAI_KHOAN b on a.so_tai_khoan = b.so_tai_khoan where (CAST(a.ngay_dang_ky AS DATE  ) >= CAST(@fromDate as date) or a.ngay_dang_ky is NULL) and (CAST(a.ngay_dang_ky AS DATE  ) <= CAST(@toDate AS DATE) or a.ngay_dang_ky is NULL )";

                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and a.nguoi_tao = @curUser";
                    }

                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and a.so_tai_khoan like @accountNo";
                    }

                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and a.ho_ten_khach_hang like @userName";
                    }

                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_moi like @packnew";
                    }
                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and a.trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();
                    var result = new List<UserServicePackageInfoModel>();
                    foreach (var user in userInfo)
                    {
                        result.Add(new UserServicePackageInfoModel()
                        {
                            goi_dich_vu_uu_daiid = user.goi_dich_vu_uu_daiid,
                            so_tai_khoan =  user.so_tai_khoan,
                            ho_ten_khach_hang = user.ho_ten_khach_hang,
                            ngay_dang_ky = user.ngay_dang_ky,
                            ngay_hieu_luc = user.ngay_hieu_luc,
                            ngay_ket_thuc = user.ngay_ket_thuc,
                            ngay_mo_tai_khoan = user.ngay_mo_tai_khoan,
                            goi_dich_vu_hien_tai = user.goi_dich_vu_hien_tai,
                            goi_dich_vu_moi = user.goi_dich_vu_moi,
                            ly_do_tu_choi = user.ly_do_tu_choi,
                            trang_thai = user.trang_thai,
                            ngay_tao = user.ngay_tao,
                            nguoi_tao = user.nguoi_tao,
                            ngay_phe_duyet = user.ngay_phe_duyet,
                            nguoi_phe_duyet = user.nguoi_phe_duyet,
                            note = user.note

                        });
                    }
                    
                    
                    Logger.Info(typeof(ChangeUserServicePackagePreferential),"Sql userInfo to list " + userInfo);
                    return result;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }       
        public List<UserServicePackageInfoModel> SearchUserServicePackListHist(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                Logger.Info(typeof(ChangeUserServicePackagePreferential)," sql SearchUserServicePackList - " + accountNo + userName + curUser + curStatus + fromDate + toDate + currentpack + packnew);
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sql = "select b.ngay_mo_tai_khoan,a.* from TVSI_DOI_GOI_DICH_VU_UU_DAI a left join TVSI_THONG_TIN_TAI_KHOAN b on a.so_tai_khoan = b.so_tai_khoan where (CAST(a.ngay_hieu_luc AS DATE  ) >= CAST(@fromDate as date)) and (CAST(a.ngay_hieu_luc AS DATE  ) <= CAST(@toDate AS DATE))";
                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and a.nguoi_tao = @curUser";
                    }
                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and a.so_tai_khoan like @accountNo";
                    }
                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and a.ho_ten_khach_hang like @userName";
                    }

                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_moi like @packnew";
                    }
                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and a.trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();
                    var result = new List<UserServicePackageInfoModel>();

                    foreach (var user in userInfo)
                    {
                        result.Add(new UserServicePackageInfoModel()
                        {
                            goi_dich_vu_uu_daiid = user.goi_dich_vu_uu_daiid,
                            so_tai_khoan =  user.so_tai_khoan,
                            ho_ten_khach_hang = user.ho_ten_khach_hang,
                            ngay_dang_ky = user.ngay_dang_ky,
                            ngay_hieu_luc = user.ngay_hieu_luc,
                            ngay_ket_thuc = user.ngay_ket_thuc,
                            ngay_mo_tai_khoan = user.ngay_mo_tai_khoan,
                            goi_dich_vu_hien_tai = user.goi_dich_vu_hien_tai,
                            goi_dich_vu_moi = user.goi_dich_vu_moi,
                            ly_do_tu_choi = user.ly_do_tu_choi,
                            trang_thai = user.trang_thai,
                            ngay_tao = user.ngay_tao,
                            nguoi_tao = user.nguoi_tao,
                            ngay_phe_duyet = user.ngay_phe_duyet,
                            nguoi_phe_duyet = user.nguoi_phe_duyet,
                            note = user.note,
                            
                        });
                    }
                    Logger.Info(typeof(ChangeUserServicePackagePreferential),"Sql userInfo to list " + userInfo);
                    return result;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }   
        
        
        
        public List<UserServicePackageInfoModel> SearchUserServicePackListCancel(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                Logger.Info(typeof(ChangeUserServicePackagePreferential)," sql SearchUserServicePackList - " + accountNo + userName + curUser + curStatus + fromDate + toDate + currentpack + packnew);
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    var sql = "select * from TVSI_DOI_GOI_DICH_VU_UU_DAI where (CAST(ngay_dang_ky AS DATE  ) >= CAST(@fromDate as date) or ngay_dang_ky is NULL) and (CAST(ngay_dang_ky AS DATE  ) <= CAST(@toDate AS DATE) or ngay_dang_ky is NULL )";

                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and nguoi_tao = @curUser";
                    }

                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and so_tai_khoan like @accountNo";
                    }

                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and ho_ten_khach_hang like @userName";
                    }

                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and goi_dich_vu_moi like @packnew";
                    }
                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();
                    Logger.Info(typeof(ChangeUserServicePackagePreferential),"Sql userInfo to list " + userInfo);
                    return userInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }   
        public void ExportListUserPreApprove(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    var sql = "select b.ngay_mo_tai_khoan,a.* from TVSI_DOI_GOI_DICH_VU_UU_DAI a left join TVSI_THONG_TIN_TAI_KHOAN b on a.so_tai_khoan = b.so_tai_khoan where (CAST(a.ngay_dang_ky AS DATE  ) >= CAST(@fromDate as date) or a.ngay_dang_ky is NULL) and (CAST(a.ngay_dang_ky AS DATE  ) <= CAST(@toDate AS DATE) or a.ngay_dang_ky is NULL )";
                    
                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and a.nguoi_tao = @curUser";
                    }

                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and a.so_tai_khoan like @accountNo";
                    }

                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and a.ho_ten_khach_hang like @userName";
                    }
                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_moi like @packnew";
                    }

                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and a.trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();

                     ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Danh sách duyệt gói");
                    workSheet.DefaultRowHeight = 12;
                    //Header of table  
                    //  
                    workSheet.Row(1).Height = 20;

                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "STT";
                    workSheet.Cells[1, 2].Value = "Gdv ưu đãi ID";
                    workSheet.Cells[1, 3].Value = "Số tài khoản";
                    workSheet.Cells[1, 4].Value = "Tên khách hàng";
                    workSheet.Cells[1, 5].Value = "Ngày đăng ký";
                    workSheet.Cells[1, 6].Value = "Ngày hiệu lực";
                    workSheet.Cells[1, 7].Value = "Ngày kết thúc";
                    workSheet.Cells[1, 8].Value = "GDV hiện tại";
                    workSheet.Cells[1, 9].Value = "GDV mới";
                    workSheet.Cells[1, 10].Value = "Trạng thái";
                    workSheet.Cells[1, 11].Value = "Ngày tạo";
                    workSheet.Cells[1, 12].Value = "người tạo";
                    workSheet.Cells[1, 13].Value = "Ngày phê duyệt";
                    workSheet.Cells[1, 14].Value = "Người phê duyệt";
                    workSheet.Cells[1, 15].Value = "Note";

                    workSheet.Cells[1, 1, 1, 15].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    workSheet.Cells[1, 1, 1, 15].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.BurlyWood);

                    int recordIndex = 2;
                    foreach (var item in userInfo)
                    {
                        workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 2].Value = item.goi_dich_vu_uu_daiid;
                        workSheet.Cells[recordIndex, 3].Value = item.so_tai_khoan;
                        workSheet.Cells[recordIndex, 4].Value = item.ho_ten_khach_hang;
                        workSheet.Cells[recordIndex, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 5].Value = item.ngay_dang_ky;
                        workSheet.Cells[recordIndex, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 6].Value = item.ngay_hieu_luc;
                        workSheet.Cells[recordIndex, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 7].Value = item.ngay_ket_thuc.Value.ToString("dd/MM/yyyy");
                        workSheet.Cells[recordIndex, 8].Value = item.goi_dich_vu_hien_tai;
                        workSheet.Cells[recordIndex, 9].Value = item.goi_dich_vu_moi;
                        workSheet.Cells[recordIndex, 10].Value = RenderAccountStatus(Convert.ToString(item.trang_thai));
                        workSheet.Cells[recordIndex, 11].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 11].Value = item.ngay_tao;
                        workSheet.Cells[recordIndex, 12].Value = item.nguoi_tao;
                        workSheet.Cells[recordIndex, 13].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 13].Value = item.ngay_phe_duyet;
                        workSheet.Cells[recordIndex, 14].Value = item.nguoi_phe_duyet;
                        workSheet.Cells[recordIndex, 15].Value = item.note;
                        recordIndex++;
                    }
                    for (int i = 1; i < 20; i++)
                    {
                        workSheet.Column(i).AutoFit();
                    }
                    string excelName = "Export_List_" + String.Format("{0:ddMMyyyyHHmmss}", DateTime.Now);
                    using (var memoryStream = new MemoryStream())
                    {

                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();

                    }
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }  
        public void ExportListUserPreCancel(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    var sql = "select * from TVSI_DOI_GOI_DICH_VU_UU_DAI where (CAST(ngay_dang_ky AS DATE  ) >= CAST(@fromDate as date) or ngay_dang_ky is NULL) and (CAST(ngay_dang_ky AS DATE  ) <= CAST(@toDate AS DATE) or ngay_dang_ky is NULL )";


                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and nguoi_tao = @curUser";
                    }

                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and so_tai_khoan like @accountNo";
                    }

                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and ho_ten_khach_hang like @userName";
                    }
                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and goi_dich_vu_moi like @packnew";
                    }

                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();

                     ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Danh sách duyệt gói");
                    workSheet.DefaultRowHeight = 12;
                    //Header of table  
                    //  
                    workSheet.Row(1).Height = 20;

                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "STT";
                    workSheet.Cells[1, 2].Value = "Gdv ưu đãi ID";
                    workSheet.Cells[1, 3].Value = "Số tài khoản";
                    workSheet.Cells[1, 4].Value = "Tên khách hàng";
                    workSheet.Cells[1, 5].Value = "Ngày đăng ký";
                    workSheet.Cells[1, 6].Value = "Ngày hiệu lực";
                    workSheet.Cells[1, 7].Value = "Ngày kết thúc";
                    workSheet.Cells[1, 8].Value = "GDV hiện tại";
                    workSheet.Cells[1, 9].Value = "GDV mới";
                    workSheet.Cells[1, 10].Value = "Trạng thái";
                    workSheet.Cells[1, 11].Value = "Ngày tạo";
                    workSheet.Cells[1, 12].Value = "người tạo";
                    workSheet.Cells[1, 13].Value = "Ngày phê duyệt";
                    workSheet.Cells[1, 14].Value = "Người phê duyệt";

                    workSheet.Cells[1, 1, 1, 14].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    workSheet.Cells[1, 1, 1, 14].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.BurlyWood);

                    int recordIndex = 2;
                    foreach (var item in userInfo)
                    {
                        workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 2].Value = item.goi_dich_vu_uu_daiid;
                        workSheet.Cells[recordIndex, 3].Value = item.so_tai_khoan;
                        workSheet.Cells[recordIndex, 4].Value = item.ho_ten_khach_hang;
                        workSheet.Cells[recordIndex, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 5].Value = item.ngay_dang_ky;
                        workSheet.Cells[recordIndex, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 6].Value = item.ngay_hieu_luc;
                        workSheet.Cells[recordIndex, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 7].Value = item.ngay_ket_thuc.Value.ToString("dd/MM/yyyy");
                        workSheet.Cells[recordIndex, 8].Value = item.goi_dich_vu_hien_tai;
                        workSheet.Cells[recordIndex, 9].Value = item.goi_dich_vu_moi;
                        workSheet.Cells[recordIndex, 10].Value = RenderAccountStatus(Convert.ToString(item.trang_thai));
                        workSheet.Cells[recordIndex, 11].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 11].Value = item.ngay_tao;
                        workSheet.Cells[recordIndex, 12].Value = item.nguoi_tao;
                        workSheet.Cells[recordIndex, 13].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 13].Value = item.ngay_phe_duyet;
                        workSheet.Cells[recordIndex, 14].Value = item.nguoi_phe_duyet;
                        recordIndex++;
                    }
                    for (int i = 1; i < 19; i++)
                    {
                        workSheet.Column(i).AutoFit();
                    }
                    string excelName = "Export_List_" + String.Format("{0:ddMMyyyyHHmmss}", DateTime.Now);
                    using (var memoryStream = new MemoryStream())
                    {

                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();

                    }
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }   
        
        public void ExportListUserPreHis(string accountNo, string userName, string curUser,
            string curStatus, DateTime fromDate, DateTime toDate,string currentpack,string packnew)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    if (toDate == DateTime.MinValue)
                        toDate = (DateTime) SqlDateTime.MaxValue;
                    if (fromDate == DateTime.MinValue)
                        fromDate = (DateTime) SqlDateTime.MinValue;

                    var sFromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var sToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var maxSQLDate = (DateTime) SqlDateTime.MaxValue;
                    maxSQLDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    var sql = "select b.ngay_mo_tai_khoan,a.* from TVSI_DOI_GOI_DICH_VU_UU_DAI a  left join TVSI_THONG_TIN_TAI_KHOAN b on a.so_tai_khoan = b.so_tai_khoan where (CAST(a.ngay_hieu_luc AS DATE  ) >= CAST(@fromDate as date)) and (CAST(a.ngay_hieu_luc AS DATE  ) <= CAST(@toDate AS DATE))";
                    if (!string.IsNullOrEmpty(curUser))
                    {
                        sql += " and a.nguoi_tao = @curUser";
                    }

                    if (!string.IsNullOrEmpty(accountNo))
                    {
                        sql += " and a.so_tai_khoan like @accountNo";
                    }

                    if (!string.IsNullOrEmpty(userName))
                    {
                        sql += " and a.ho_ten_khach_hang like @userName";
                    }
                    if (!string.IsNullOrEmpty(currentpack) & !(currentpack == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_hien_tai like @currentpack";
                    }

                    if (!string.IsNullOrEmpty(packnew) & !(packnew == "--Tất cả--"))
                    {
                        sql += " and a.goi_dich_vu_moi like @packnew";
                    }

                    var iStatus = 0;
                    if (!string.IsNullOrEmpty(curStatus) & !(curStatus == "--Tất cả--"))
                    {
                        sql += " and a.trang_thai = @curStatus";
                        iStatus = int.Parse(curStatus);
                    }

                    var userlist = conn.Query<UserServicePackageInfoModel>(sql, new
                    {
                        accountNo = "%" + accountNo + "%",
                        userName = "%" + userName + "%",
                        currentpack = "%" + currentpack + "%",
                        packnew = "%" + packnew + "%",
                        curUser = curUser,
                        fromDate = sFromDate,
                        toDate = sToDate,
                        curStatus = iStatus,
                        maxDate = maxSQLDate
                    }).Reverse().ToList();
                    
                    var userInfo = new List<UserServicePackageInfoModel>();

                    foreach (var user in userlist)
                    {
                        userInfo.Add(new UserServicePackageInfoModel()
                        {
                            goi_dich_vu_uu_daiid = user.goi_dich_vu_uu_daiid,
                            so_tai_khoan =  user.so_tai_khoan,
                            ho_ten_khach_hang = user.ho_ten_khach_hang,
                            ngay_dang_ky = user.ngay_dang_ky,
                            ngay_hieu_luc = user.ngay_hieu_luc,
                            ngay_ket_thuc = user.ngay_ket_thuc,
                            ngay_mo_tai_khoan = user.ngay_mo_tai_khoan,
                            goi_dich_vu_hien_tai = user.goi_dich_vu_hien_tai,
                            goi_dich_vu_moi = user.goi_dich_vu_moi,
                            ly_do_tu_choi = user.ly_do_tu_choi,
                            trang_thai = user.trang_thai,
                            ngay_tao = user.ngay_tao,
                            nguoi_tao = user.nguoi_tao,
                            ngay_phe_duyet = user.ngay_phe_duyet,
                            nguoi_phe_duyet = user.nguoi_phe_duyet,
                            note = user.note
                            
                        });
                    }

                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Danh sách duyệt gói");
                    workSheet.DefaultRowHeight = 12;
                    //Header of table  
                    //  
                    workSheet.Row(1).Height = 20;

                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "STT";
                    workSheet.Cells[1, 2].Value = "Gdv ưu đãi ID";
                    workSheet.Cells[1, 3].Value = "Số tài khoản";
                    workSheet.Cells[1, 4].Value = "Tên khách hàng";
                    workSheet.Cells[1, 5].Value = "Ngày đăng ký";
                    workSheet.Cells[1, 6].Value = "Ngày hiệu lực";
                    workSheet.Cells[1, 7].Value = "Ngày kết thúc";
                    workSheet.Cells[1, 8].Value = "GDV hiện tại";
                    workSheet.Cells[1, 9].Value = "GDV mới";
                    workSheet.Cells[1, 10].Value = "Trạng thái";
                    workSheet.Cells[1, 11].Value = "Ngày tạo";
                    workSheet.Cells[1, 12].Value = "người tạo";
                    workSheet.Cells[1, 13].Value = "Ngày phê duyệt";
                    workSheet.Cells[1, 14].Value = "Người phê duyệt";
                    workSheet.Cells[1, 15].Value = "Ngày mở tài khoản";
                    workSheet.Cells[1, 16].Value = "Note";

                    workSheet.Cells[1, 1, 1, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    workSheet.Cells[1, 1, 1, 16].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.BurlyWood);

                    int recordIndex = 2;
                    foreach (var item in userInfo)
                    {
                        workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 2].Value = item.goi_dich_vu_uu_daiid;
                        workSheet.Cells[recordIndex, 3].Value = item.so_tai_khoan;
                        workSheet.Cells[recordIndex, 4].Value = item.ho_ten_khach_hang;
                        workSheet.Cells[recordIndex, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 5].Value = item.ngay_dang_ky;
                        workSheet.Cells[recordIndex, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 6].Value = item.ngay_hieu_luc;
                        workSheet.Cells[recordIndex, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 7].Value = item.ngay_ket_thuc.Value.ToString("dd/MM/yyyy");
                        workSheet.Cells[recordIndex, 8].Value = item.goi_dich_vu_hien_tai;
                        workSheet.Cells[recordIndex, 9].Value = item.goi_dich_vu_moi;
                        workSheet.Cells[recordIndex, 10].Value = RenderAccountStatus(Convert.ToString(item.trang_thai));
                        workSheet.Cells[recordIndex, 11].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 11].Value = item.ngay_tao;
                        workSheet.Cells[recordIndex, 12].Value = item.nguoi_tao;
                        workSheet.Cells[recordIndex, 13].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 13].Value = item.ngay_phe_duyet;
                        workSheet.Cells[recordIndex, 14].Value = item.nguoi_phe_duyet;
                        workSheet.Cells[recordIndex, 15].Style.Numberformat.Format = "dd/mm/yyyy";
                        workSheet.Cells[recordIndex, 15].Value = item.ngay_mo_tai_khoan;
                        workSheet.Cells[recordIndex, 16].Value = item.note;
                        recordIndex++;
                    }
                    for (int i = 1; i < 22; i++)
                    {
                        workSheet.Column(i).AutoFit();
                    }
                    string excelName = "Export_List_" + String.Format("{0:ddMMyyyyHHmmss}", DateTime.Now);
                    using (var memoryStream = new MemoryStream())
                    {

                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();

                    }
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }

        public UserServicePackageInfoModel GetUserSPByID(long sp_id)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select * from TVSI_DOI_GOI_DICH_VU_UU_DAI where goi_dich_vu_uu_daiid = @searchKey";
                    var userInfo = conn.Query<UserServicePackageInfoModel>(sql, new {searchKey = sp_id}).FirstOrDefault();

                    return userInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }
        
        public List<ServicePackPreferentialModel> getNamePackPre()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select ten_goi_dich_vu_uu_dai from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where trang_thai='1'";

                    var histLogMargin = conn.Query<ServicePackPreferentialModel>(sql).Reverse().ToList();
                    
                    return histLogMargin;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                return null;
            }
        }
        public bool ChangeUserSPStatusByID(long sp_id, int newStatus, string declineReason)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql = "update TVSI_DOI_GOI_DICH_VU_UU_DAI set trang_thai = @status, ly_do_tu_choi = @declineReason, " +
                              "nguoi_phe_duyet = @editPerson, ngay_phe_duyet = @editDate, ngay_hieu_luc = @fromDate where goi_dich_vu_uu_daiid = @id";

                    var today = DateTime.Now;
                    var tomorrow = ConstParam.AddBusinessDays(today); 

                    var affectedRow = conn.Execute(sql, new
                    {
                        status = newStatus,
                        declineReason = declineReason,
                        editPerson = SessionHelper.GetUserName(),
                        editDate = today,
                        fromDate = tomorrow,
                        id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack),""+ ex.Message);
                throw;
            }
        }
        public bool ChangeUserSPStatusByIDCancel(long sp_id, int newStatus, string declineReason)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql = "update TVSI_DOI_GOI_DICH_VU_UU_DAI set trang_thai = @status, ly_do_tu_choi = @declineReason, " +
                              "nguoi_phe_duyet = @editPerson, ngay_phe_duyet = @editDate, ngay_hieu_luc = @fromdate where goi_dich_vu_uu_daiid = @id";

                    var today = DateTime.Now;
                    var tomorrow = ConstParam.AddBusinessDays(today);

                    var affectedRow = conn.Execute(sql, new
                    {
                        status = newStatus,
                        declineReason = declineReason,
                        editPerson = SessionHelper.GetUserName(),
                        fromdate = tomorrow,
                        editDate = today,
                        
                        id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack),""+ ex.Message);
                throw;
            }
        }

        public bool UpdateUserSPByID(long sp_id, string accountNo, string userName, int curStatus,
            string curServicePack, string newServicePack, DateTime signUpDate, DateTime fromDate, DateTime toDate,
            string declineReason)
        {
            try
            {
                if (toDate == DateTime.MinValue)
                    toDate = (DateTime) SqlDateTime.MaxValue;

                if (curStatus != -1)
                {
                    declineReason = "";
                }

                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql =
                        "update TVSI_DOI_GOI_DICH_VU_UU_DAI set so_tai_khoan = @accountNo, ho_ten_khach_hang = @userName, trang_thai = @curStatus, " +
                        "goi_dich_vu_hien_tai = @curServicePack, goi_dich_vu_moi = @newServicePack, ngay_hieu_luc = @fromDate, ngay_ket_thuc = @toDate," +
                        "ngay_dang_ky = @signUpDate, ly_do_tu_choi = @declineReason, nguoi_phe_duyet = @editPerson, ngay_phe_duyet = @editDate " +
                        "where goi_dich_vu_uu_daiid = @id";

                    var today = DateTime.Now;
                    var tomorrow = ConstParam.AddBusinessDays(today);

                    var affectedRow = conn.Execute(sql, new
                    {
                        accountNo = accountNo,
                        userName = userName,
                        curStatus = curStatus,
                        curServicePack = curServicePack,
                        newServicePack = newServicePack,
                        signUpDate = signUpDate,
                        fromDate = tomorrow,
                        toDate = toDate,
                        declineReason = declineReason,
                        editPerson = SessionHelper.GetUserName(),
                        editDate = today,
                        id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }

        public bool CheckExistingUserByAccountNo(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql =
                        "select count(goi_dich_vu_uu_daiid) from TVSI_DOI_GOI_DICH_VU_UU_DAI where so_tai_khoan = @accountNo and trang_thai = @curStatus";
                    var count = conn.Query<int>(sql, new {@accountNo = accountNo, @curStatus = 0}).FirstOrDefault();
                    return count > 0;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }

        public bool AddUserSP(string accountNo, string userName, string curSP, string newSP, DateTime signUpDate,
            DateTime toDate,string note)
        {
            try
            {
                if (toDate == DateTime.MinValue)
                    toDate = (DateTime) SqlDateTime.MaxValue;

                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql =
                        "insert into TVSI_DOI_GOI_DICH_VU_UU_DAI(So_tai_khoan, Ho_ten_khach_hang, Ngay_dang_ky, Trang_thai, " +
                        " Ngay_ket_thuc, Goi_dich_vu_hien_tai, Goi_dich_vu_moi, Nguoi_tao, Ngay_tao,Note)" +
                        " values(@accountNo, @userName, @signUpDate, @curStatus, @toDate, @curSP, @newSP, @addPerson, @addDate,@Note)";

                    var affectedRow = conn.Execute(sql, new
                    {
                        accountNo = accountNo,
                        userName = userName,
                        signUpDate = signUpDate,
                        curStatus = 0,
                        toDate = toDate,
                        curSP = curSP,
                        newSP = newSP,
                        addPerson = SessionHelper.GetUserName(),
                        addDate = DateTime.Now,
                        Note = note
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }

        public string GetUserEmailByAccNo(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();

                    var sql =
                        "select dia_chi_email from TVSI_THONG_TIN_LIEN_HE_KHACH_HANG where so_tai_khoan = @accountNo";

                    var email = conn.Query<String>(sql, new
                    {
                        accountNo = accountNo
                    }).ToList();

                    if (email.Count != 0)
                    {
                        return email[0];
                    }

                    return "";
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }
        public GoiDichVuModel GetCancelLenhGoiDichVuUuDai(string sotaikhoan)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql =
                        "select so_tai_khoan from TVSI_DOI_GOI_DICH_VU_UU_DAI where so_tai_khoan = @so_tai_khoan and trang_thai = 5";
                    return conn.Query<GoiDichVuModel>(sql, new
                    {
                        so_tai_khoan = sotaikhoan,
                        
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(GoiDichVuModel), ex.Message);
                return null;
            }
        }

        public void SendConfirmEmailToUser(string userName, string userEmail, string curSP, string newSP,
            DateTime fromDate)
        {
            try
            {
                var AStrWeb = ConstParam.ConnectionString_WebsiteDB;

                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đổi GDV tài khoản của Ông(Bà): " + userName;

                var templateFile =
                    HttpContext.Current.Server.MapPath("~/templates/Mail_ChangeMarginTemplateConfirm.html");
                Logger.Info(typeof(ChangeUserServicePackagePreferential),"Path Mail" +templateFile);
                var content = "";

                using (var readFile =
                    new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }

                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Không load được template");
                }

                content = ReplaceEmailContent(content, userName, curSP, newSP, fromDate);
                TVSI.Utility.Logger.Info(typeof(ChangeUserServicePackagePreferential),
                    string.Format("User {0} gửi mail cho {1}", SessionHelper.GetUserName(), userEmail));

                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sQEM_NOI_DUNG_EMAIL_INSERT_2TABLE11", subject, content, 0, 0,
                    "system", DateTime.Now, "system", DateTime.Now, DateTime.Now, DateTime.Now, 0, userEmail, 0,
                    DateTime.Now, DateTime.Now, "");
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Gửi email thất bại " + ex.Message);
            }
        }   
        public void SendDeclineEmailToUser(string userName, string userEmail ,string lydotuchoi)
        {
            try
            {
                var AStrWeb = ConstParam.ConnectionString_WebsiteDB;

                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đổi GDV tài khoản của Ông(Bà): " + userName;

                var templateFile =
                    HttpContext.Current.Server.MapPath("~/templates/Mail_ChangeMarginTemplateReject.html");
                Logger.Info(typeof(ChangeUserServicePackagePreferential),"Path Mail" +templateFile);

                var content = "";

                using (var readFile =
                    new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }

                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Không load được template");
                }

                /*
                content = ReplaceEmailContent(content, userName, curSP, newSP, fromDate);
                */
                content = ReplaceEmailRejectContent(content, userName,lydotuchoi );
                TVSI.Utility.Logger.Info(typeof(ChangeUserServicePackagePreferential), string.Format("User {0} gửi mail cho {1}", SessionHelper.GetUserName(), userEmail));

                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sQEM_NOI_DUNG_EMAIL_INSERT_2TABLE11", subject, content, 0, 0,
                    "system", DateTime.Now, "system", DateTime.Now, DateTime.Now, DateTime.Now, 0, userEmail, 0,
                    DateTime.Now, DateTime.Now, "");
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Gửi email thất bại " + ex.Message);
            }
        }

        public string SearchNameByAccNumber(string accNumber)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql = "Select ten_khach_hang from TVSI_THONG_TIN_KHACH_HANG where ma_khach_hang=@accNumber";
                    var userName = conn.Query<string>(sql, new
                    {
                        accNumber = accNumber
                    }).Reverse().FirstOrDefault();
                    if (!string.IsNullOrEmpty(userName))
                    {
                        return userName;   
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }
        private string ReplaceEmailRejectContent(string content, string userName, string lydotuchoi)
        {
            content = content.Replace("{ten_khach_hang}", userName);
            content = content.Replace("{ly_do_tu_choi}", lydotuchoi);
            return content;
        }

        private string ReplaceEmailContent(string content, string userName, string curSP, string newSP,
            DateTime fromDate)
        {
            content = content.Replace("{ten_khach_hang}", userName);
            content = content.Replace("{goi_dich_vu_hien_tai}", curSP);
            content = content.Replace("{goi_dich_vu_moi}", newSP);
            content = content.Replace("{ngay_hieu_luc}", fromDate.ToString("dd/MM/yyyy"));
            return content;
        }

        public List<string> GetSP()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select distinct ten_goi_dich_vu_uu_dai from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where trang_thai = 1";

                    var result = conn.Query<string>(sql).ToList();

                    return result;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), ex.Message);
                throw;
            }
        }
        
        private string RenderAccountStatus(string value)
        {
            if (value == "-1")
                return "Từ chối";
            if (value == "0")
                return "Tạo mới";
            if (value == "1")
                return "Hoạt động";
            if (value == "99")
                return "Xóa logic";
            if (value == "-2")
                return "Đã Hủy";   
            if (value == "-3")
                return "Ngừng sử dụng";
            if (value == "5")
                return "Hủy đăng ký gói";
            return value;
        }
        
        public bool CheckEKYCProfile(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();
                    var sql =
                        "select ma_khach_hang from TVSI_DANG_KY_MO_TAI_KHOAN where trang_thai_ho_so = 200 and ma_khach_hang = @CustCode and (nguon_du_lieu = 4 or nguon_du_lieu = 6)";
                    return conn.Query<string>(sql, new
                    {
                        @CustCode = accountNo
                    }).Any();
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePackagePreferential), ex.Message);
                throw;
            }
        }

        public bool CheckSourceAccount(string accountNo)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();
                    var sql =
                        "select ma_khach_hang from TVSI_DANG_KY_MO_TAI_KHOAN where ma_khach_hang = @CustCode and (nguon_du_lieu = 4 or nguon_du_lieu = 6)";
                    return conn.Query<string>(sql, new
                    {
                        @CustCode = accountNo
                    }).Any();
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePackagePreferential), ex.Message);
                throw;
            }
        }
        
        public AccountModel GetChangeMargin(string custcode, string marginGroupCurrent,string marginGroupNew,string marginGroupPrefer, int type)
        {
            try
            {
                using (var conn  = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
                {
                    conn.Open();
                    var stored = "TVSI_sREGSERVICE_CheckChangeMargin";
                    return conn.Query<AccountModel>(stored, new
                    {
                        @CustCode = custcode,
                        @MarginGroupCurrent = marginGroupCurrent,
                        @MarginGroupNew = marginGroupNew,
                        @Combo = marginGroupPrefer,
                        @Type = type,
                        @Source = "B"
                    },commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(ChangeUserServicePack),"GetChangeMargin" + ex.Message);
                throw;
            }
        }
    }
}