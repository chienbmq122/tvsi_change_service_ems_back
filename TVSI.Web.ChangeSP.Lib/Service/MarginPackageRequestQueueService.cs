﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Newtonsoft.Json;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class MarginPackageRequestQueueService
    {
        public long UpdateBasicInfo(MarginPackageRequestQueueModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_UPDATE_BASIC_INFO";
                        
                        var p = new DynamicParameters();
                        p.Add("margin_package_request_id", myRequest.margin_package_request_id);
                        p.Add("preferential_package_request_id", myRequest.preferential_package_request_id);
                        p.Add("preferential_package_name", myRequest.preferential_package_name);
                        p.Add("customer_account", myRequest.customer_account);
                        p.Add("current_service_package", myRequest.current_service_package);
                        p.Add("new_service_package", myRequest.new_service_package);
                        p.Add("interest_type", myRequest.interest_type);
                        p.Add("account_interest_rate", myRequest.account_interest_rate);
                        p.Add("basic_interest_rate", myRequest.basic_interest_rate);
                        p.Add("adjust_initial_rate", myRequest.adjust_initial_rate);
                        p.Add("adjust_call_rate", myRequest.adjust_call_rate);
                        p.Add("adjust_force_sell_rate", myRequest.adjust_force_sell_rate);
                        p.Add("customer_level_name", myRequest.customer_level_name);
                        p.Add("staff_checker", myRequest.staff_checker);
                        p.Add("remark_sba", myRequest.remark_sba);
                        p.Add("effective_date", myRequest.effective_date);
                        p.Add("interest_rate_checker", myRequest.interest_rate_checker);
                        p.Add("margin_rate_checker", myRequest.margin_rate_checker);
                        p.Add("margin_package_request_queue_status", myRequest.margin_package_request_queue_status);
                        p.Add("current_ee", myRequest.current_ee);
                        p.Add("new_ee", myRequest.new_ee);
                        p.Add("updated_by", myRequest.updated_by);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(MarginPackageRequestQueueService), "UpdateBasicInfo: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public MarginPackageRequestQueueModel TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT(long marginPackageRequestId)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT";
                    var values = new
                    {
                        @margin_package_request_id = marginPackageRequestId
                    };

                    return conn.Query<MarginPackageRequestQueueModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(MarginPackageRequestQueueService), "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT: " + e.Message);
                return null;
            }
        }
        
        public MarginPackageRequestQueueModel TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT_BY_ID(long queueId)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT_BY_ID";
                    var values = new
                    {
                        @margin_package_request_queue_id = queueId
                    };

                    return conn.Query<MarginPackageRequestQueueModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(MarginPackageRequestQueueService), "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT_BY_ID: " + e.Message);
                return null;
            }
        }
        
        public List<MarginPackageRequestQueueModel> Search(string customerAccount, DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SEARCH";
                    var values = new
                    {
                        @customer_account = customerAccount,
                        @fromDate = (fromDate == DateTime.MinValue) ? (DateTime?) null : fromDate,
                        @toDate = (toDate == DateTime.MinValue) ? (DateTime?) null : toDate
                    };

                    return conn.Query<MarginPackageRequestQueueModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(MarginPackageRequestQueueService), "Search: " + e.Message);
                return null;
            }
        }
        
    }
}