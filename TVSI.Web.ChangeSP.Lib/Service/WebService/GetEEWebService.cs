﻿using System;
using Newtonsoft.Json;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service.WebService.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service.WebService
{
    public class GetEEWebService
    {
        readonly string host = ConstParam.Host_API;

        public double GetEEModelNew(string custcode,string package)
        {
            try
            {
                var param = "&so_tai_khoan=" + custcode + "";
                using (var client = new System.Net.WebClient())
                {
                    var uri = host + "api/website/GetSucMuaTheoGoiChon?ten_goi_dich_vu=" + package.Replace(" ","") + "" + param;
                    var method = "POST";
                    var response_data = client.UploadString(uri,method);
                    var result =  JsonConvert.DeserializeObject<LAY_SUC_MUA_GOI_MARGIN_Model>(response_data);
                    return result.EE_GOI_MOI;
                }

            }
            catch (Exception e)
            {
                Logger.Error(typeof(PackAgeWebService),"Call Api /api/website/GetSucMuaTheoGoiChon?ten_goi_dich_vu= "+ package +"&so_tai_khoan="+custcode+"" + e.Message);
                throw;
            }
        }
        public double GetEEModelCurrent(string custcode,string package)
        {
            try
            {
                var param = "&so_tai_khoan=" + custcode + "";
                using (var client = new System.Net.WebClient())
                {
                    var uri = host + "api/website/GetSucMuaTheoGoiChon?ten_goi_dich_vu=" + package.Replace(" ","") + "" + param;
                    var method = "POST";
                    var response_data = client.UploadString(uri,method);
                    var result =  JsonConvert.DeserializeObject<LAY_SUC_MUA_GOI_MARGIN_Model>(response_data);
                    return result.EE_HIEN_TAI;
                }

            }
            catch (Exception e)
            {
                Logger.Error(typeof(PackAgeWebService),"Call Api /api/website/GetSucMuaTheoGoiChon?ten_goi_dich_vu= "+ package +"&so_tai_khoan="+custcode+"" + e.Message);
                throw;
            }
        }
        
        

    }
}