﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service.WebService.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service.WebService
{
    public class PackAgeWebService
    {
        readonly string host = ConstParam.Host_API;
        
        public GetPackageCurrentModel GetPackageCurrentModel(string id)
        {
            try
            {
                var packcurrent = new GetPackageCurrentModel();
                var param = "so_tai_khoan=" + id + "";
                using (var client = new System.Net.WebClient())
                {
                    var uri = host + "api/website/GetMarginGroup?so_tai_khoan=" + id + "";
                    Logger.Info(typeof(PackAgeWebService),"host API" + host);
                    var method = "POST";
                    string data = JsonConvert.SerializeObject(packcurrent);
                    var response_data = client.UploadString(uri,method,param);
                    Logger.Info(typeof(PackAgeWebService),"Call API "+ uri);
                    Logger.Info(typeof(PackAgeWebService),"Return "+ JsonConvert.DeserializeObject<GetPackageCurrentModel>(response_data) );
                    return JsonConvert.DeserializeObject<GetPackageCurrentModel>(response_data);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(PackAgeWebService),"Call Api api/website/GetMarginGroup?so_tai_khoan= " + id + ex.Message);
                return new GetPackageCurrentModel()
                {
                    GOI_DICH_VU = "BASIC"
                };
            }
        }

        public CurrentPServicePack CurrentPServicePack(string id)
        {
            try
            {
                var packp = new CurrentPServicePack();
                var param = "ma_khach_hang" + id + "";
                using (var client = new System.Net.WebClient())
                {
                    var uri = host + "api/website/GetGoiDichVuUuDaiDangDung?ma_khach_hang=" + id + "";
                    Logger.Info(typeof(PackAgeWebService),"host API" + host);
                    var method = "POST";
                    string data = JsonConvert.SerializeObject(packp);
                    var response_data = client.UploadString(uri,method,param);
                    Logger.Info(typeof(PackAgeWebService),"Call API "+ uri);
                    Logger.Info(typeof(PackAgeWebService),"Return" + JsonConvert.DeserializeObject<CurrentPServicePack>(response_data));
                    return JsonConvert.DeserializeObject<CurrentPServicePack>(response_data);;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(PackAgeWebService),"Call Api api/website/GetMarginGroup?so_tai_khoan= " + id + ex.Message);
                return new CurrentPServicePack()
                {
                    ma_dich_vu = "Chưa đăng ký"
                };
            }
        }
        
    }
}