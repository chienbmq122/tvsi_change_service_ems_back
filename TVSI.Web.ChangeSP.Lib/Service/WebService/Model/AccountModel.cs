﻿namespace TVSI.Web.ChangeSP.Lib.Service.WebService.Model
{
    public class AccountModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Note { get; set; }
        public int IsConfirm { get; set; }
    }
}