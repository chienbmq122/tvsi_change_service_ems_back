﻿namespace TVSI.Web.ChangeSP.Lib.Service.WebService.Model
{
    public class LAY_SUC_MUA_GOI_MARGIN_Model
    {
        public string ACCOUNTNO { get; set; }
        public string MARGIN_GROUP { get; set; }
        public string SUC_MUA { get; set; }
        public string SUC_MUA_TOI_DA { get; set; }
        public string LMV_NEXT { get; set; }
        public double EE_HIEN_TAI { get; set; }
        public double EE_GOI_MOI { get; set; }

    }
}