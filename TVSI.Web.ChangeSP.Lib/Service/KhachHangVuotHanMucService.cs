﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Newtonsoft.Json;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class KhachHangVuotHanMucService
    {
        public int Insert(KhachHangVuotHanMucModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_KHACH_HANG_VUOT_HAN_MUC_INSERT";

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        };

                        settings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFFZ" });

                        var json = JsonConvert.SerializeObject(myRequest, Formatting.Indented, settings);
                        
                        var p = new DynamicParameters();
                        p.Add("khach_hang_vuot_han_muc_json", json);

                        var result = conn.Query<int>(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction).FirstOrDefault();
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(KhachHangVuotHanMucService), "Insert: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public long UpdateStatus(long customerId, int customerStatus, string updatedBy)
        {
            using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_KHACH_HANG_VUOT_HAN_MUC_UPDATE_STATUS";
                        
                        var p = new DynamicParameters();
                        p.Add("CustomerId", customerId);
                        p.Add("CustomerStatus", customerStatus);
                        p.Add("UpdatedBy", updatedBy);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(KhachHangVuotHanMucService), "UpdateStatus: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public KhachHangVuotHanMucModel SelectById(long customerId)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_KHACH_HANG_VUOT_HAN_MUC_UPDATE_SELECT_BY_ID";
                    var values = new
                    {
                        @CustomerId = customerId
                    };

                    return conn.Query<KhachHangVuotHanMucModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(KhachHangVuotHanMucService), "SelectById: " + e.Message);
                return null;
            }
        }
        
        public List<KhachHangVuotHanMucModel> Search(string customerCode, string customerName, int customerStatus)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_KHACH_HANG_VUOT_HAN_MUC_UPDATE_SEARCH";
                    var values = new
                    {
                        @CustomerCode = customerCode,
                        @CustomerName = customerName,
                        @CustomerStatus = customerStatus
                    };

                    return conn.Query<KhachHangVuotHanMucModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(KhachHangVuotHanMucService), "Search: " + e.Message);
                return null;
            }
        }
    }
}