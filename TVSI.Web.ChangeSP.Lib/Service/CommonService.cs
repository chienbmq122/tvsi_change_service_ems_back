﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Newtonsoft.Json;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class CommonService
    {
        public dynamic TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_MA_KHACH_HANG(string customerCode)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_MA_KHACH_HANG";
                    var values = new
                    {
                        @ma_khach_hang = customerCode
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_MA_KHACH_HANG: " + e.Message);
                return null;
            }
        }
        
        public VipModel TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_ID(long vipId)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_ID";
                    var values = new
                    {
                        @tai_khoan_vip_id = vipId
                    };

                    return conn.Query<VipModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_ID: " + e.Message);
                return null;
            }
        }
        
        public List<VipModel> TVSI_sSPM_KHACH_HANG_VIP_SEARCH(string customerCode, string customerName, string branchName, int customerLevelCode, int vipStatus)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_KHACH_HANG_VIP_SEARCH";
                    var values = new
                    {
                        @ma_khach_hang = customerCode,
                        @customer_name = customerName,
                        @branch_name = branchName,
                        @loai_vip = customerLevelCode,
                        @trang_thai = vipStatus
                    };

                    return conn.Query<VipModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_KHACH_HANG_VIP_SEARCH: " + e.Message);
                return null;
            }
        }
        
        public long TVSI_sSPM_KHACH_HANG_VIP_INSERT(VipModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_KHACH_HANG_VIP_INSERT";
                        
                        var p = new DynamicParameters();
                        p.Add("ma_khach_hang", myRequest.ma_khach_hang);
                        p.Add("loai_vip", myRequest.loai_vip);
                        p.Add("mo_ta", myRequest.mo_ta);
                        p.Add("customer_name", myRequest.customer_name);
                        p.Add("branch_name", myRequest.branch_name);
                        p.Add("effective_date", myRequest.effective_date);
                        p.Add("end_date", myRequest.end_date);
                        p.Add("nguoi_tao", myRequest.nguoi_tao);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "TVSI_sSPM_KHACH_HANG_VIP_INSERT: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public long ImportVip(List<VipModel> lstRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_KHACH_HANG_VIP_INSERT";

                        foreach (var myRequest in lstRequest)
                        {
                            var p = new DynamicParameters();
                            p.Add("ma_khach_hang", myRequest.ma_khach_hang);
                            p.Add("loai_vip", myRequest.loai_vip);
                            p.Add("mo_ta", myRequest.mo_ta);
                            p.Add("customer_name", myRequest.customer_name);
                            p.Add("branch_name", myRequest.branch_name);
                            p.Add("effective_date", myRequest.effective_date);
                            p.Add("end_date", myRequest.end_date);
                            p.Add("nguoi_tao", myRequest.nguoi_tao);

                            var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        }
                        
                        transaction.Commit();

                        return 1;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "ImportVip: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public long TVSI_sSPM_KHACH_HANG_VIP_UPDATE(VipModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_CommonDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_KHACH_HANG_VIP_UPDATE";
                        
                        var p = new DynamicParameters();
                        p.Add("tai_khoan_vip_id", myRequest.tai_khoan_vip_id);
                        p.Add("ma_khach_hang", myRequest.ma_khach_hang);
                        p.Add("loai_vip", myRequest.loai_vip);
                        p.Add("mo_ta", myRequest.mo_ta);
                        p.Add("trang_thai", myRequest.trang_thai);
                        p.Add("customer_name", myRequest.customer_name);
                        p.Add("branch_name", myRequest.branch_name);
                        p.Add("effective_date", myRequest.effective_date);
                        p.Add("end_date", myRequest.end_date);
                        p.Add("updated_by", myRequest.updated_by);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "TVSI_sSPM_KHACH_HANG_VIP_UPDATE: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public dynamic TVSI_sSPM_STAFF_SELECT_BY_CUSTOMER_CODE(string customerCode)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_STAFF_SELECT_BY_CUSTOMER_CODE";
                    var values = new
                    {
                        @customer_code = customerCode
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_STAFF_SELECT_BY_CUSTOMER_CODE: " + e.Message);
                return null;
            }
        }
        
        public StaffModel TVSI_sSPM_STAFF_SELECT_BY_ID(long staffId)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_STAFF_SELECT_BY_ID";
                    var values = new
                    {
                        @staff_id = staffId
                    };

                    return conn.Query<StaffModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_STAFF_SELECT_BY_ID: " + e.Message);
                return null;
            }
        }
        
        public List<StaffModel> TVSI_sSPM_STAFF_SEARCH(string customerCode, string customerName, string departmentName, int staffStatus)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_STAFF_SEARCH";
                    var values = new
                    {
                        @customer_code = customerCode,
                        @customer_name = customerName,
                        @department_name = departmentName,
                        @staff_status = staffStatus
                    };

                    return conn.Query<StaffModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_STAFF_SEARCH: " + e.Message);
                return null;
            }
        }
        
        public long TVSI_sSPM_STAFF_INSERT(StaffModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_STAFF_INSERT";
                        
                        var p = new DynamicParameters();
                        p.Add("customer_code", myRequest.customer_code);
                        p.Add("customer_name", myRequest.customer_name);
                        p.Add("department_name", myRequest.department_name);
                        p.Add("identity_number", myRequest.identity_number);
                        p.Add("effective_date", myRequest.effective_date);
                        p.Add("end_date", myRequest.end_date);
                        p.Add("created_by", myRequest.created_by);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "TVSI_sSPM_STAFF_INSERT: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public long ImportStaff(List<StaffModel> lstRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_STAFF_INSERT";

                        foreach (var myRequest in lstRequest)
                        {
                            var p = new DynamicParameters();
                            p.Add("customer_code", myRequest.customer_code);
                            p.Add("customer_name", myRequest.customer_name);
                            p.Add("department_name", myRequest.department_name);
                            p.Add("identity_number", myRequest.identity_number);
                            p.Add("effective_date", myRequest.effective_date);
                            p.Add("end_date", myRequest.end_date);
                            p.Add("created_by", myRequest.created_by);

                            var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        }
                        
                        transaction.Commit();

                        return 1;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "ImportStaff: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public long TVSI_sSPM_STAFF_UPDATE(StaffModel myRequest)
        {
            using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var procedure = "TVSI_sSPM_STAFF_UPDATE";
                        
                        var p = new DynamicParameters();
                        p.Add("staff_id", myRequest.staff_id);
                        p.Add("customer_code", myRequest.customer_code);
                        p.Add("customer_name", myRequest.customer_name);
                        p.Add("department_name", myRequest.department_name);
                        p.Add("identity_number", myRequest.identity_number);
                        p.Add("effective_date", myRequest.effective_date);
                        p.Add("end_date", myRequest.end_date);
                        p.Add("staff_status", myRequest.staff_status);
                        p.Add("updated_by", myRequest.updated_by);

                        var result = conn.Execute(procedure, p, commandType: CommandType.StoredProcedure, transaction: transaction);
                        
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Logger.Debug(typeof(CommonService), "TVSI_sSPM_STAFF_UPDATE: " + e.Message);
                        return -1;
                    }
                }
            }
        }
        
        public DateTime GetNextTradingDay(DateTime currentDate, int step)
        {
            try
            {
                var strQuery = "SELECT dbo.[TVSI_fLAY_NGAY_GIAO_DICH_SAP_TOI](@ngay_hien_tai, @t_ngay_tinh_tu_ngay_hien_tai)";
                var parameters = new
                {
                    ngay_hien_tai = currentDate,
                    t_ngay_tinh_tu_ngay_hien_tai = step
                };
                using (var connection = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    connection.Open();
                    return connection.Query<DateTime>(strQuery, parameters, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal(typeof(CommonService), "GetNextTradingDay", ex);
                throw;
            }
        }
        
        public dynamic TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_ACTIVE_RECORD(string customerAccount, DateTime effectiveDate)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_ACTIVE_RECORD";
                    var values = new
                    {
                        @so_tai_khoan = customerAccount,
                        @ngay_hieu_luc = effectiveDate
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_ACTIVE_RECORD: " + e.Message);
                return null;
            }
        }
        
        public dynamic TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_CURRENT_EFFECTIVE_RECORD(string customerAccount)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.RetrievalQuery_IPGDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_CURRENT_EFFECTIVE_RECORD";
                    var values = new
                    {
                        @so_tai_khoan = customerAccount
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_CURRENT_EFFECTIVE_RECORD: " + e.Message);
                return null;
            }
        }
        
        public List<MarginRateParameterModel> SelectMarginRateParameter()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_SELECT_MARGIN_RATE_PARAMETER";
                    var values = new
                    {
                    };

                    return conn.Query<MarginRateParameterModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_SELECT_MARGIN_RATE_PARAMETER: " + e.Message);
                return null;
            }
        }
        
        public List<InterestRateParameterModel> SelectInterestRateParameter()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_SELECT_INTEREST_RATE_PARAMETER";
                    var values = new
                    {
                    };

                    return conn.Query<InterestRateParameterModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_SELECT_INTEREST_RATE_PARAMETER: " + e.Message);
                return null;
            }
        }
        
        public List<CustomerLevelModel> TVSI_sSPM_CUSTOMER_LEVEL_SELECT_ALL()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_CUSTOMER_LEVEL_SELECT_ALL";
                    var values = new
                    {
                    };

                    return conn.Query<CustomerLevelModel>(procedure, values,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sSPM_CUSTOMER_LEVEL_SELECT_ALL: " + e.Message);
                return null;
            }
        }
        
        public dynamic TVSI_sREGSERVICE_GET_CUSTINFO(string customerCode)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sREGSERVICE_GET_CUSTINFO";
                    var values = new
                    {
                        @ma_khach_hang = customerCode
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(CommonService), "TVSI_sREGSERVICE_GET_CUSTINFO: " + e.Message);
                return null;
            }
        }
        
    }
}