﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class ManageServicePackServicePreferential 
    {
        public List<ServicePackPreferentialModel> SearchServicePackList(string status)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select * from TVSI_GOI_DICH_VU_UU_DAI_MARGIN";

                    var iStatus = 1;
                    if (!(status == "--Tất cả--"))
                    {
                        sql += " where trang_thai = @status";
                        if (!string.IsNullOrEmpty(status))
                        {
                            iStatus = int.Parse(status);
                        }
                    }

                    var servicePackInfo = conn.Query<ServicePackPreferentialModel>(sql, new
                    {
                        status = iStatus
                    }).ToList();

                    return servicePackInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                return null;
            }
        }

        public ServicePackPreferentialModel GetServicePackByID(long sp_id)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select * from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where goi_dich_vu_uu_daiID = @searchKey";
                    var servicePackInfo = conn.Query<ServicePackPreferentialModel>(sql, new { searchKey = sp_id }).FirstOrDefault();

                    return servicePackInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool UpdateServicePackByID(long sp_id, string spName, string laisuattoithieu, 
            string lephitoithieu,string phigiaodichtoithieu,double songaymienlai,string vongquaydunothang,
            string dunotoida, string phidichvugoi,string tylevaytoida,string soluongmachovay,string doituong,string mota
            , int status)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql =
                        "update TVSI_GOI_DICH_VU_UU_DAI_MARGIN set ten_goi_dich_vu_uu_dai = @spName, lai_suat_toi_thieu = @laisuattoithieu, ly_le_phi_toi_thieu = @lephitoithieu,phi_giao_dich_toi_thieu = @phigiaodichtoithieu " +
                        ",so_ngay_mien_lai = @songaymienlai , vong_quay_du_no_thang = @vongquaydunothang , du_no_toi_da = @dunotoida" +
                        ",phi_dich_vu_goi = @phidichvugoi,ty_le_vay_toi_da =@tylevaytoida, so_luong_ma_cho_vay = @soluongmachovay" +
                        ",doi_tuong = @doituong, mo_ta = @mota " +
                        ",trang_thai = @status, nguoi_cap_nhat = @editPerson, ngay_cap_nhat = @editDate where goi_dich_vu_uu_daiid = @id";

                    var today = DateTime.Now;
                    var tomorrow = today.AddDays(1);

                    var affectedRow = conn.Execute(sql, new
                    {
                        @spName = spName,
                        @laisuattoithieu = laisuattoithieu,
                        @lephitoithieu = lephitoithieu,
                        @phigiaodichtoithieu = phigiaodichtoithieu,
                        @songaymienlai = songaymienlai,
                        @vongquaydunothang = vongquaydunothang,
                        @dunotoida = dunotoida,
                        @phidichvugoi = phidichvugoi,
                        @tylevaytoida = tylevaytoida,
                        @soluongmachovay = soluongmachovay,
                        @doituong = doituong,
                        @mota = mota,
                        @status = status,
                        @editPerson = SessionHelper.GetUserName(),
                        @editDate = DateTime.Now,
                        @id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool CheckExistingSPByName(string spName)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql = "select count(goi_dich_vu_uu_daiid) from TVSI_GOI_DICH_VU_UU_DAI_MARGIN where ten_goi_dich_vu_uu_dai = @spName and trang_thai = @status";
                    var count = conn.Query<int>(sql, new { spName = spName, status = 1 }).FirstOrDefault();
                    return count > 0;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool AddServicePack(string spName, string laisuattoithieu, 
            string lephitoithieu,string phigiaodichtoithieu,double songaymienlai,string vongquaydunothang,
            string dunotoida, string phidichvugoi,string tylevaytoida,string soluongmachovay,string doituong,string mota)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "insert into TVSI_GOI_DICH_VU_UU_DAI_MARGIN"
                    + "(ten_goi_dich_vu_uu_dai, lai_suat_toi_thieu, ly_le_phi_toi_thieu,phi_giao_dich_toi_thieu,"+
                    "so_ngay_mien_lai, vong_quay_du_no_thang, du_no_toi_da, phi_dich_vu_goi, ty_le_vay_toi_da, so_luong_ma_cho_vay" +
                    ",doi_tuong,mo_ta" +
                    ",trang_thai, nguoi_tao, Ngay_tao,nguoi_cap_nhat,ngay_cap_nhat)" +
                    "values(@spName, @laisuattoithieu, @lephitoithieu,@phigiaodichtoithieu,@songaymienlai,@vongquaydunothang,@dunotoida" +
                    ",@phidichvugoi,@tylevaytoida,@soluongmachovay,@doituong,@mota,@status, @addPerson,@addDate,@addPersonUpdate,@addDateUpdate)";
                    var affectedRow = conn.Execute(sql, new
                    {
                       
                        
                        @spName = spName,
                        @laisuattoithieu = laisuattoithieu,
                        @lephitoithieu = lephitoithieu,
                        @phigiaodichtoithieu = phigiaodichtoithieu,
                        @songaymienlai = songaymienlai,
                        @vongquaydunothang = vongquaydunothang,
                        @dunotoida = dunotoida,
                        @phidichvugoi = phidichvugoi,
                        @tylevaytoida = tylevaytoida,
                        @soluongmachovay = soluongmachovay,
                        @doituong = doituong,
                        @mota = mota,
                        @status = 1,
                        @addPerson = SessionHelper.GetUserName(),
                        @addDate = DateTime.Now,
                        @addPersonUpdate = SessionHelper.GetUserName(),
                        @addDateUpdate = DateTime.Now
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool ChangeSPStatusByID(long sp_id, int status)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql = "update TVSI_GOI_DICH_VU_UU_DAI_MARGIN set trang_thai = @status, nguoi_cap_nhat = @editPerson, ngay_cap_nhat = @editDate " +
                        "where goi_dich_vu_uu_daiID = @id";

                    var affectedRow = conn.Execute(sql, new
                    {
                        @status = status,
                        @editPerson = SessionHelper.GetUserName(),
                        @editDate = DateTime.Now,
                        @id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }
    }
}