﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.Lib.Service
{
    public class ManageServicePack
    {
        public List<ServicePackModel> SearchServicePackList(string status)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select * from TVSI_GOI_DICH_VU_MARGIN";

                    var iStatus = 1;
                    if (!(status == "--Tất cả--"))
                    {
                        sql += " where trang_thai = @status";
                        if (!string.IsNullOrEmpty(status))
                        {
                            iStatus = int.Parse(status);
                        }
                    }

                    var servicePackInfo = conn.Query<ServicePackModel>(sql, new
                    {
                        status = iStatus
                    }).ToList();

                    return servicePackInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public ServicePackModel GetServicePackByID(long sp_id)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql = "select * from TVSI_GOI_DICH_VU_MARGIN where goi_dich_vuid = @searchKey";
                    var servicePackInfo = conn.Query<ServicePackModel>(sql, new {searchKey = sp_id}).FirstOrDefault();
                    return servicePackInfo;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool UpdateServicePackByID(long sp_id, string spName, double lai_suat, double cap_nhat_suc_mua
            , double so_ngay_mien_lai, double ty_le_ky_quy, double ty_le_duy_tri, double callRate,
            double ty_le_canh_bao,
            double ty_le_vay, string dunotoida, int status,
            string lai_suat_bang_chu, double lai_suat_van_hanh, double adjust_initial_rate, double adjust_call_rate, double adjust_force_sell_rate)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql =
                        "update TVSI_GOI_DICH_VU_MARGIN set ten_goi_dich_vu = @spName, lai_suat = @laisuat,cap_nhat_suc_mua = @capnhatsucmua,so_ngay_mien_lai = @songaymienlai,ty_le_ky_quy = @tylekyquy,ty_le_duy_tri = @tyleduytri," +
                        " ty_le_call = @callRate,ty_le_canh_bao = @tylecanhbao,ty_le_vay = @tylevay , " +
                        "du_no_toi_da = @dunotoida,trang_thai = @status, nguoi_cap_nhat = @editPerson, ngay_cap_nhat = @editDate, " +
                        "lai_suat_bang_chu = @lai_suat_bang_chu, lai_suat_van_hanh = @lai_suat_van_hanh, adjust_initial_rate = @adjust_initial_rate, adjust_call_rate = @adjust_call_rate, adjust_force_sell_rate = @adjust_force_sell_rate " +
                        "where goi_dich_vuid = @id";

                    var today = DateTime.Now;
                    var tomorrow = today.AddDays(1);

                    var affectedRow = conn.Execute(sql, new
                    {
                        spName = spName,
                        laisuat = lai_suat,
                        capnhatsucmua = cap_nhat_suc_mua,
                        songaymienlai = so_ngay_mien_lai,
                        tylekyquy = ty_le_ky_quy,
                        tyleduytri = ty_le_duy_tri,
                        callRate = callRate,
                        tylecanhbao = ty_le_canh_bao,
                        tylevay = ty_le_vay,
                        dunotoida = dunotoida,
                        status = status,
                        editPerson = SessionHelper.GetUserName(),
                        editDate = DateTime.Now,
                        id = sp_id,
                        lai_suat_bang_chu = lai_suat_bang_chu,
                        lai_suat_van_hanh = lai_suat_van_hanh,
                        adjust_initial_rate = adjust_initial_rate,
                        adjust_call_rate = adjust_call_rate,
                        adjust_force_sell_rate = adjust_force_sell_rate
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool CheckExistingSPByName(string spName)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql =
                        "select count(goi_dich_vuid) from TVSI_GOI_DICH_VU_MARGIN where ten_goi_dich_vu = @spName and trang_thai = @status";
                    var count = conn.Query<int>(sql, new {spName = spName, status = 1}).FirstOrDefault();
                    return count > 0;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool AddServicePack(string spName, double lai_suat, double cap_nhat_suc_mua
            , double so_ngay_mien_lai, double ty_le_ky_quy, double ty_le_duy_tri, double callRate,
            double ty_le_canh_bao,
            double ty_le_vay, string du_no_toi_da,
            string lai_suat_bang_chu, double lai_suat_van_hanh, double adjust_initial_rate, double adjust_call_rate, double adjust_force_sell_rate)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();

                    var sql =
                        "insert into TVSI_GOI_DICH_VU_MARGIN(ten_goi_dich_vu, lai_suat,cap_nhat_suc_mua,so_ngay_mien_lai,ty_le_ky_quy," +
                        "" +
                        "ty_le_duy_tri,ty_le_call,ty_le_canh_bao,ty_le_vay,du_no_toi_da, trang_thai, nguoi_tao, Ngay_tao," +
                        "lai_suat_bang_chu,lai_suat_van_hanh,adjust_initial_rate,adjust_call_rate,adjust_force_sell_rate) " +
                        " values(@spName, @interestRate,@capnhatsucmua,@sogaymienlai,@tylekyquy,@tyleduytri, @callRate,@tylecanhbao,@tylevay,@dunotoida, @status, @addPerson, @addDate, @lai_suat_bang_chu,@lai_suat_van_hanh,@adjust_initial_rate,@adjust_call_rate,@adjust_force_sell_rate)";

                    var affectedRow = conn.Execute(sql, new
                    {
                        @spName = spName,
                        @interestRate = lai_suat,
                        @capnhatsucmua = cap_nhat_suc_mua,
                        @sogaymienlai = so_ngay_mien_lai,
                        @tylekyquy = ty_le_ky_quy,
                        @tyleduytri = ty_le_duy_tri,
                        @callRate = callRate,
                        @tylecanhbao = ty_le_canh_bao,
                        @tylevay = ty_le_vay,
                        @dunotoida = du_no_toi_da,
                        @status = 1,
                        @addPerson = SessionHelper.GetUserName(),
                        @addDate = DateTime.Now,
                        @lai_suat_bang_chu = lai_suat_bang_chu,
                        @lai_suat_van_hanh = lai_suat_van_hanh,
                        @adjust_initial_rate = adjust_initial_rate,
                        @adjust_call_rate = adjust_call_rate,
                        @adjust_force_sell_rate = adjust_force_sell_rate
                    });

                    if (affectedRow > 0)
                    {
                        var log = AddLogSP(spName, 1, lai_suat, cap_nhat_suc_mua, so_ngay_mien_lai, ty_le_ky_quy,
                            ty_le_duy_tri, callRate, ty_le_canh_bao, ty_le_vay, du_no_toi_da, 0,
                            lai_suat_bang_chu,lai_suat_van_hanh,adjust_initial_rate,adjust_call_rate,adjust_force_sell_rate);
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public bool AddLogSP(string spName, int status, double laisuat, double capnhatsucmua,
            double songaymienlai, double tylekyquy, double tyleduytri, double tylecall, double tylecanhbao,
            double tylevay, string dunoitoida, int trangthailog,
            string lai_suat_bang_chu, double lai_suat_van_hanh, double adjust_initial_rate, double adjust_call_rate, double adjust_force_sell_rate)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql =
                        "insert into TVSI_LOG_GOI_DICH_VU_MARGIN(ten_goi_dich_vu,trang_thai,ngay_cap_nhat_cuoi,nguoi_cap_nhat_cuoi,lai_suat,cap_nhat_suc_mua,so_ngay_mien_lai,ty_le_ky_quy,ty_le_duy_tri,ty_le_call,ty_le_canh_bao,ty_le_vay,du_no_toi_da,trang_thai_log, lai_suat_bang_chu, lai_suat_van_hanh, adjust_initial_rate, adjust_call_rate, adjust_force_sell_rate) " +
                        "values (@spName,@status,@datetimenow,@addPerson,@laisuat,@capnhatsucmua,@songaymienlai,@tylekyquy,@tyleduytri,@tylecall,@tylecanhbao,@tylevay,@dunotoida,@trangthailog,@lai_suat_bang_chu,@lai_suat_van_hanh,@adjust_initial_rate,@adjust_call_rate,@adjust_force_sell_rate)";
                    var affectedRow = conn.Execute(sql, new
                    {
                        @spName = spName,
                        @status = status,
                        @datetimenow = DateTime.Now,
                        @addPerson = SessionHelper.GetUserName(),
                        @laisuat = laisuat,
                        @dunotoida = dunoitoida,
                        @capnhatsucmua = capnhatsucmua,
                        @songaymienlai = songaymienlai,
                        @tylekyquy = tylekyquy,
                        @tyleduytri = tyleduytri,
                        @tylecall = tylecall,
                        @tylecanhbao = tylecanhbao,
                        @tylevay = tylevay,
                        @trangthailog = trangthailog,
                        @lai_suat_bang_chu = lai_suat_bang_chu,
                        @lai_suat_van_hanh = lai_suat_van_hanh,
                        @adjust_initial_rate = adjust_initial_rate,
                        @adjust_call_rate = adjust_call_rate,
                        @adjust_force_sell_rate = adjust_force_sell_rate
                    });
                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }


        public bool ChangeSPStatusByID(long sp_id, int status)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var sql =
                        "update TVSI_GOI_DICH_VU_MARGIN set trang_thai = @status, nguoi_cap_nhat = @editPerson, ngay_cap_nhat = @editDate " +
                        "where goi_dich_vuid = @id";

                    var affectedRow = conn.Execute(sql, new
                    {
                        status = status,
                        editPerson = SessionHelper.GetUserName(),
                        editDate = DateTime.Now,
                        id = sp_id
                    });

                    if (affectedRow > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ManageServicePack), ex.Message);
                throw;
            }
        }

        public dynamic TVSI_sSPM_SELECT_DEFAULT_MARGIN_PACKAGE()
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_SELECT_DEFAULT_MARGIN_PACKAGE";
                    var values = new
                    {
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(ManageServicePack), "TVSI_sSPM_SELECT_DEFAULT_MARGIN_PACKAGE: " + e.Message);
                return null;
            }
        }
        
        public dynamic TVSI_sSPM_SELECT_MARGIN_PACKAGE_BY_NAME(string packageName)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_SELECT_MARGIN_PACKAGE_BY_NAME";
                    var values = new
                    {
                        @ten_goi_dich_vu = packageName
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(ManageServicePack), "TVSI_sSPM_SELECT_MARGIN_PACKAGE_BY_NAME: " + e.Message);
                return null;
            }
        }
        
        public dynamic TVSI_sSPM_CUSTOMER_LEVEL_AND_SERVICE_PACKAGE_SELECT(string customerLevelName, string servicePackageName)
        {
            try
            {
                using (var conn = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    conn.Open();
                    var procedure = "TVSI_sSPM_CUSTOMER_LEVEL_AND_SERVICE_PACKAGE_SELECT";
                    var values = new
                    {
                        @customer_level_name = customerLevelName,
                        @service_package_name = servicePackageName
                    };

                    return conn.Query<dynamic>(procedure, values,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(ManageServicePack), "TVSI_sSPM_CUSTOMER_LEVEL_AND_SERVICE_PACKAGE_SELECT: " + e.Message);
                return null;
            }
        }
        
    }
}