﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class MarginPackageRequestQueueModel
    {
        public long margin_package_request_queue_id { get; set; }
        public long margin_package_request_id { get; set; }
        public int preferential_package_request_id { get; set; }
        public string preferential_package_name { get; set; }
        public string customer_account { get; set; }
        public string current_service_package { get; set; }
        public string new_service_package { get; set; }
        public string interest_type { get; set; }
        public decimal account_interest_rate { get; set; }
        public decimal basic_interest_rate { get; set; }
        public decimal adjust_initial_rate { get; set; }
        public decimal adjust_call_rate { get; set; }
        public decimal adjust_force_sell_rate { get; set; }
        public string customer_level_name { get; set; }
        public bool staff_checker { get; set; }
        public string remark_sba { get; set; }
        public DateTime? effective_date { get; set; }
        public bool interest_rate_checker { get; set; }
        public bool margin_rate_checker { get; set; }
        public int margin_package_request_queue_status { get; set; }
        public decimal current_ee { get; set; }
        public decimal new_ee { get; set; }
        public string margin_package_api_reference_number { get; set; }
        public string margin_package_api_error_msg { get; set; }
        public string interest_rate_api_reference_number { get; set; }
        public int interest_rate_api_status { get; set; }
        public string interest_rate_api_error_msg { get; set; }
        public string margin_rate_api_reference_number { get; set; }
        public int margin_rate_api_status { get; set; }
        public string margin_rate_api_error_msg { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}