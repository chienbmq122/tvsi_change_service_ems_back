﻿using System;
using System.Data.SqlTypes;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class KhachHangVuotHanMucModel
    {
        public long CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string MarginGroup { get; set; }
        public decimal InterestRate { get; set; }
        public string PreferentialPackage { get; set; }
        public string CustomerLevelName { get; set; }
        public decimal LoanLimit { get; set; }
        public decimal ApprovingLoanLimit { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AttachmentFileName { get; set; }
        public int CustomerStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}