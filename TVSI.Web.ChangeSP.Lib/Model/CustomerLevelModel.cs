﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class CustomerLevelModel
    {
        public long customer_level_id { get; set; }
        public string customer_level_name { get; set; }
        public long customer_level_code { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}