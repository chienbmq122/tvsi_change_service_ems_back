﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class ServicePackModel
    {
        public int goi_dich_vuid { get; set; }
        public string ten_goi_dich_vu { get; set; }
        public double lai_suat { get; set; }
        public double cap_nhat_suc_mua { get; set; }
        public double so_ngay_mien_lai { get; set; }
        public double ty_le_ky_quy { get; set; }
        public double ty_le_duy_tri { get; set; }
        public double ty_le_call { get; set; }
        public double ty_le_canh_bao { get; set; }
        public double ty_le_vay { get; set; }
        public string du_no_toi_da { get; set; }
        public int trang_thai { get; set; }
        public double lai_suat_van_hanh { get; set; }
        public string lai_suat_bang_chu { get; set; }
        public double adjust_initial_rate { get; set; }
        public double adjust_call_rate { get; set; }
        public double adjust_force_sell_rate { get; set; }
        public DateTime ngay_tao { get; set; }
        public string nguoi_tao { get; set; }
        public DateTime? ngay_cap_nhat { get; set; }
        public string nguoi_cap_nhat { get; set; }
    }
}