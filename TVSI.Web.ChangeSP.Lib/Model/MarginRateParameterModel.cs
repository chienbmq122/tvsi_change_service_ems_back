﻿namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class MarginRateParameterModel
    {
        public string package_name { get; set; }
        public decimal adjust_initial_rate { get; set; }
        public decimal adjust_call_rate { get; set; }
        public decimal adjust_force_sell_rate { get; set; }
    }
}