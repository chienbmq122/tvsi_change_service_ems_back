﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class UserServicePackageInfoModel
    {
        public int goi_dich_vu_uu_daiid { get; set; }
        public string so_tai_khoan { get; set; }
        public string ho_ten_khach_hang { get; set; }
        public DateTime ngay_dang_ky { get; set; }
        public DateTime? ngay_hieu_luc { get; set; }
        public DateTime? ngay_ket_thuc { get; set; }
        public DateTime? ngay_mo_tai_khoan { get; set; }
        public string goi_dich_vu_hien_tai { get; set; }
        public string goi_dich_vu_moi { get; set; }
        public string ly_do_tu_choi { get; set; }
        public int trang_thai { get; set; }
        public DateTime ngay_tao { get; set; }
        public string nguoi_tao { get; set; }
        public DateTime? ngay_phe_duyet { get; set; }
        public string nguoi_phe_duyet { get; set; }
        public string note { get; set; }

    }
}