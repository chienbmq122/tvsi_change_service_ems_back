﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class VipModel
    {
        public long tai_khoan_vip_id { get; set; }
        public string ma_khach_hang { get; set; }
        public int loai_vip { get; set; }
        public string mo_ta { get; set; }
        public int trang_thai { get; set; }
        public string customer_name { get; set; }
        public string branch_name { get; set; }
        public DateTime? effective_date { get; set; }
        public DateTime? end_date { get; set; }
        public string nguoi_tao { get; set; }
        public DateTime? ngay_tao { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}