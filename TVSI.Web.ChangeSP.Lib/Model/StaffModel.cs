﻿using System;

namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class StaffModel
    {
        public long staff_id { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string department_name { get; set; }
        public string identity_number { get; set; }
        public DateTime? effective_date { get; set; }
        public DateTime? end_date { get; set; }
        public int staff_status { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}