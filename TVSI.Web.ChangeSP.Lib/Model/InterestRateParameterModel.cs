﻿namespace TVSI.Web.ChangeSP.Lib.Model
{
    public class InterestRateParameterModel
    {
        public string package_name { get; set; }
        public decimal normal_rate { get; set; }
        public decimal silver_rate { get; set; }
        public decimal gold_rate { get; set; }
        public decimal platinum_rate { get; set; }
        public decimal diamond_rate { get; set; }
    }
}