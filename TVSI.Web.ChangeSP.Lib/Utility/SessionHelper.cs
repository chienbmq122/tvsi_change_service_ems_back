﻿using System.Web;

namespace TVSI.Web.ChangeSP.Lib.Utility
{
    public class SessionHelper
    {
        public static void CreateSession(string userName)
        {
            HttpContext.Current.Session["user_vsd"] = userName;
            HttpContext.Current.Session["user"] = userName;
        }

        public static string GetUserName()
        {
            var vsdSession = HttpContext.Current.Session["user_vsd"];
            if (vsdSession != null)
                return vsdSession.ToString();

            var emsSession = HttpContext.Current.Session["user"];
            if (emsSession != null)
                return emsSession.ToString();

            return "";
        }
        
        public static void RecreateSession()
        {
            var vsdSession = HttpContext.Current.Session["user_vsd"];
            if (vsdSession != null)
                return;

            var emsSession = HttpContext.Current.Session["user"];
            if (emsSession != null)
            {
                var userName = emsSession.ToString();
                HttpContext.Current.Session["user_vsd"] = userName;
            }
        }
    }
}