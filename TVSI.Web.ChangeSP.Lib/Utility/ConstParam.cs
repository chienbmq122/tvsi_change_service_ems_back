﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using TVSI.Utility;

namespace TVSI.Web.ChangeSP.Lib.Utility
{
    public class ConstParam
    {
        public static string ConnectionString_EMSDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["TVSI_SM_CONNECTION"].ConnectionString;
            }
        }     

        public static string RetrievalQuery_IPGDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["RetrievalQuery_IPGDB"].ConnectionString;
            }
        }

        public static string ConnectionString_CommonDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CommonDB_Connection"].ConnectionString;
            }
        }

        public static string ConnectionString_WebsiteDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ConnectionString;
            }
        }
        public static string Host_API
        {
            get
            {
                return ConfigurationManager.AppSettings["CRM_WEBAPI_URL"]+"";
            }
        }

        public static List<string> ServicePackList
        {
            get
            {
                return new List<string>()
                {
                    "BASIC",
                    "DIAMO",
                    "FS",
                    "HF",
                    "M28",
                    "SF"
                };
            }
        }

        public static Dictionary<string, string> StatusDict
        {
            get
            {
                return new Dictionary<string, string>(){
                    {"0", "Tạo mới"},
                    {"1", "Hoạt động"},
                    {"-1", "Từ chối"},
                    {"99", "Xóa logic"},
                    {"-3","Ngừng sử dụng"},
                    {"-2","đã hủy"},

                };
            }
        }
        
        public static Dictionary<string, string> CustomerStatusDict
        {
            get
            {
                return new Dictionary<string, string>(){
                    {"10", "Tạo mới"},
                    {"20", "Hoạt động"},
                    {"99", "Xóa logic"}
                };
            }
        }

        public static Dictionary<string, string> ServicePackStatusDict
        {
            get
            {
                return new Dictionary<string, string>(){
                    {"1", "Hoạt động"},
                    {"99", "Xóa logic"}
                };
            }
        }

        public static Dictionary<string, string> PurChasingPowerOfTheDay
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    {"1", "Có"},
                    {"2", "Không"}
                };
            }
        }

        public static string getStatusMess(string value)
        {
            var statusDict = ConstParam.StatusDict;

            if (statusDict.ContainsKey(value))
            {
                return statusDict[value];
            }
            return value;
        }
        public static DateTime AddBusinessDays(DateTime date)
        {
            /*switch (date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        date = date.AddDays(3);
                        break;
                    case DayOfWeek.Saturday:
                        date = date.AddDays(2);
                        break;
                    default:
                        date = date.AddDays(1);
                        break;
                }
            return date;*/
            try
            {
                var strQuery = "SELECT dbo.[TVSI_fLAY_NGAY_GIAO_DICH_SAP_TOI](@ngay_hien_tai, @t_ngay_tinh_tu_ngay_hien_tai)";
                var parameters = new
                {
                    ngay_hien_tai = date,
                    t_ngay_tinh_tu_ngay_hien_tai = 1
                };
                using (var connection = new SqlConnection(ConstParam.ConnectionString_EMSDB))
                {
                    connection.Open();
                    return connection.Query<DateTime>(strQuery, parameters, commandType: CommandType.Text).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal(typeof(ConstParam), "AddBusinessDays", ex);
                throw;
            }
        }

        public static DateTime? GetDateOpenAccount(string custcode)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionString_EMSDB))
                {
                    var sql =
                        " select ngay_mo_tai_khoan from [dbo].[TVSI_THONG_TIN_TAI_KHOAN] where ma_khach_hang = @custcode AND loai_tai_khoan = 6";
                    using(var command = new SqlCommand(sql,conn))
                    {
                        
                        command.Connection.Open();
                        command.Parameters.Add("@custcode", custcode.Substring(0,6));
                        command.ExecuteNonQuery();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return Convert.ToDateTime(reader["ngay_mo_tai_khoan"]);
                            }
                        }
                        return null;

                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(ConstParam),"GetDateOpenAccount" + e.Message);
                throw;
            }
        }
    
    }
}