﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using TVSI.Web.ChangeSP.Lib.Constant;

namespace TVSI.Web.ChangeSP.Lib.Utility
{
    public class CommonUtils
    {
        public static string ParaphraseFileName(string fileName)
        {
            int lastIndex = fileName.LastIndexOf('.');
            var name = ConvertToUnSign(fileName.Substring(0, lastIndex)) + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            var ext = fileName.Substring(lastIndex + 1);
            return name + "." + ext;
        }
        
        public static string ConvertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        
        public static string ConvertCustomerLevelCodeToName(int input)
        {
            string output = input.ToString();
            switch (input)
            {
                case CustomerLevelCodeConst.NORMAL:
                    output = "Normal";
                    break;
                case CustomerLevelCodeConst.DIAMOND:
                    output = "Diamond";
                    break;
                case CustomerLevelCodeConst.PLATINUM:
                    output = "Platinum";
                    break;
                case CustomerLevelCodeConst.GOLD:
                    output = "Gold";
                    break;
                case CustomerLevelCodeConst.SILVER:
                    output = "Silver";
                    break;
            }

            return output;
        }
        
        public static string ConvertRequestApiStatusCodeToName(int input)
        {
            string output = input.ToString();
            switch (input)
            {
                case MarginPackageRequestQueueStatusConst.KHOI_TAO:
                    output = "Khởi tạo";
                    break;
                case MarginPackageRequestQueueStatusConst.CHO_XU_LY:
                    output = "Chờ xử lý";
                    break;
                case MarginPackageRequestQueueStatusConst.DANG_XU_LY:
                    output = "Đang xử lý";
                    break;
                case MarginPackageRequestQueueStatusConst.THANH_CONG:
                    output = "Thành công";
                    break;
                case MarginPackageRequestQueueStatusConst.THAT_BAI:
                    output = "Thất bại";
                    break;
            }

            return output;
        }
        
        public static string ConvertStaffStatusCodeToName(int input)
        {
            string output = input.ToString();
            switch (input)
            {
                case StaffStatusConst.HOAT_DONG:
                    output = "Hoạt động";
                    break;
                case StaffStatusConst.NGUNG_HOAT_DONG:
                    output = "Ngừng hoạt động";
                    break;
            }

            return output;
        }
        
        public static string ConvertVipStatusCodeToName(int input)
        {
            string output = input.ToString();
            switch (input)
            {
                case VipStatusConst.HOAT_DONG:
                    output = "Hoạt động";
                    break;
                case VipStatusConst.NGUNG_HOAT_DONG:
                    output = "Ngừng hoạt động";
                    break;
            }

            return output;
        }
    }
}