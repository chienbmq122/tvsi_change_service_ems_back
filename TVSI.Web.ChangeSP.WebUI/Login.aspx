﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        #formLogin {
            background-image: url(/images/banner-ems.png);
            background-repeat: no-repeat;
        }

        #footer {
            border-top: 1px solid #CCCCCC;
            clear: both;
            margin: 0 auto;
            padding-bottom: 5px;
            padding-top: 5px;
            width: 100%;
        }

            #footer h2 {
                -x-system-font: none;
                border: 0 none;
                color: #333333;
                font-family: Verdana,Arial,Helvetica,sans-serif;
                font-size: 10px;
                font-size-adjust: none;
                font-stretch: normal;
                font-style: normal;
                font-variant: normal;
                font-weight: normal;
                line-height: normal;
                padding-right: 2px;
                text-align: center;
                width: 99%;
            }
    </style>
    <link href="login.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="margin: 0px auto; width: 950px;">
                <div>
                    <img alt="" src="/images/tvsi_logo.jpg" />
                </div>
                <div style="clear: both;"></div>
                <div>
                    <div style="width: 950px; height: 270px; margin-bottom: 20px;" id="formLogin">
                        <div style="margin-left: 600px; padding-top: 10px;">

                            <div style="margin-top: 5px;">
                                <table width="325px" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; height: 165px;">
                                    <tr>
                                        <td style="width: 300px; height: 165px;">
                                            <div id="portlet-wrapper-$portlet_id" class="portlet3">
                                                <div class="portlet3-contain">
                                                    <div class="portlet3-topper">
                                                        <div class="portlet3-topper-left">
                                                            <div class="portlet3-topper-right">
                                                                <span class="portlet3-title">Đăng nhập </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portlet3-content">
                                                    <table width="100%" border="0" style="border-collapse: collapse;" cellspacing="0"
                                                        cellpadding="0">
                                                        <tr>
                                                            <td style="height: 27px; width: 32%;">
                                                                <div>
                                                                    <label class="porlet-login-label">
                                                                        Tên đăng nhập</label>
                                                                </div>
                                                            </td>
                                                            <td style="width: 76%;">
                                                                <div>
                                                                    <asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 5px;" colspan="2">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 27px; width: 32%;">
                                                                <div>
                                                                    <label class="porlet-login-label">
                                                                        Mật khẩu</label>
                                                                </div>
                                                            </td>
                                                            <td style="width: 76%; height: 27px;">
                                                                <asp:TextBox runat="server" TextMode="Password" ID="txtPwd"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 5px;" colspan="2">
                                                                <asp:Label runat="server" ForeColor="Red" ID="lblError"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 51%;">
                                                                <span style="color: #606163">Tự động đăng nhập</span>

                                                                <input id="ctrl_login_ctrl_form_login_chkAutoLogin" type="checkbox" />
                                                            </td>
                                                            <td style="width: 49%;" align="right">
                                                                <asp:Button ID="Button1" runat="server" Text="Đăng nhập" OnClick="btnLogin_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 10px;" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" colspan="2">
                                                                <div class="navigation">
                                                                    <span>
                                                                        <img alt="Quên mật khẩu" src="/images/help.png" class="icon" />
                                                                        <a id="ctrl_login_ctrl_form_login_lnkResetPwd" href="#">Quên mật khẩu</a>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="portlet3-bottom-left">
                                                    <div class="portlet3-bottom-right">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="divInfoSalesManagement" style="float: left; width: 300px;">
                        <div id="portlet-wrapper" class="portlet">
                            <div class="portlet-contain">
                                <div class="portlet-topper">
                                    <div class="portlet-topper-left">
                                        <div class="portlet-topper-right">
                                            <span class="portlet-title">Quản lý hoạt động Sales</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-content">
                                <table border="0" style="height: 243px;" width="100%" class="portlet-section td">
                                    <tr>
                                        <td valign="top">
                                            <div class="portlet-content-title">
                                                <a target="newwin" href="/">Quản lý chỉ tiêu - hoa hồng</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Phân bổ chỉ tiêu tới từng chi nhánh, nhóm sale hoặc saleman. Thanh toán hoa hồng
                                    <span style="color: Red; font-style: italic; font-family: Arial; font-size: 12px;">(Đã cập nhật theo Quyết định 571/2011/QĐ-TGĐ về điều chỉnh quy chế Sale, Hoa hồng ngày 19/08/2011)</span>
                                            </div>
                                            <div class="portlet-separation">
                                                &nbsp;
                                            </div>
                                            <div class="portlet-content-title" style="margin-top: 5px;">
                                                <a target="newwin" href="/">Quản lý khách hàng</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Quản lý khách hành, quan hệ khách hàng theo từng chi nhánh, cấp độ hệ thống
                                            </div>
                                            <div class="portlet-separation">
                                                &nbsp;
                                            </div>
                                            <div class="portlet-content-title">
                                                <a target="newwin" href="/">Báo cáo & thống kê</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Cung cấp các báo cáo phục vụ cho việc quản lý việc thực hiện chỉ tiêu, giao dịch
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="divInfoBroadcast" style="float: left; width: 300px; margin-left: 22px;">
                        <div id="Div4" class="portlet">
                            <div class="portlet-contain">
                                <div class="portlet-topper">
                                    <div class="portlet-topper-left">
                                        <div class="portlet-topper-right">
                                            <span class="portlet-title">Quản lý thông tin nội bộ</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-content">
                                <table border="0" style="height: 243px;" width="100%" class="portlet-section td">
                                    <tr>
                                        <td valign="top">
                                            <div class="portlet-content-title">
                                                <a target="newwin" href="/">Quản lý tin tức</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Cập nhật liên tục các tin tức liên quan tới mã cổ phiếu, thông tin ngành
                                            </div>
                                            <div class="portlet-separation">
                                                &nbsp;
                                            </div>
                                            <div class="portlet-content-title" style="margin-top: 5px;">
                                                <a target="newwin" href="/">Khuyến nghị đầu tư</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Đưa ra các khuyến nghị mua bán mã cổ phiếu dựa trên một số tiêu chí do Research
                                    đưa ra
                                            </div>
                                            <div class="portlet-separation">
                                                &nbsp;
                                            </div>
                                            <div class="portlet-content-title">
                                                <a target="newwin" href="/">Thảo luận theo chủ đề</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Quản lý các topic thảo luận các thông tin liên quan thị trường chứng khoán
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="divInfoExright_iPayment" style="float: left; width: 300px; margin-left: 22px;">
                        <div id="Div7" class="portlet">
                            <div class="portlet-contain">
                                <div class="portlet-topper">
                                    <div class="portlet-topper-left">
                                        <div class="portlet-topper-right">
                                            <span class="portlet-title">Quản lý thực hiện quyền-iPayment</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-content">
                                <table border="0" style="height: 243px;" width="100%" class="portlet-section td">
                                    <tr>
                                        <td valign="top">
                                            <div class="portlet-content-title">
                                                <a target="newwin" href="/">Quản lý thực hiện quyền</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                <ul>
                                                    <li>Quản lý, gửi thông báo cho khách hàng về quyền mua phát sinh, cho phép khách hàng
                                            đăng ký quyền mua </li>
                                                    <li>Rekey quyền mua mà khách hàng đăng ký </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-separation">
                                                &nbsp;
                                            </div>
                                            <div class="portlet-content-title" style="margin-top: 5px;">
                                                <a target="newwin" href="/">iPayment</a>
                                            </div>
                                            <div class="portlet-content-description">
                                                Cho phép khách hàng nộp tiền vào tài khoản chứng khoán từ ngân hàng Vietcombank
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <h2>
                        <a target="_blank" href="http://www.tvsi.com.vn">Công ty cổ phần chứng khoán Tân Việt</a>
                        | Copyright &copy; 2009-2011 by TVSI IT Software (IT-SW@tvsi.com.vn)</h2>
                </div>
            </div>
        </div>
    </form>
</body>
</html>