﻿using Ext.Net;
using System;
using System.Web;

namespace TVSI.Web.ChangeSP.WebUI
{
    public partial class Menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SiteMapNode siteNode = SiteMap.RootNode;
            TreeNode root = CreateNode(siteNode);
            treePanelMenu.Root.Add(root);
            treePanelMenu.ExpandAll();
        }

        private Ext.Net.TreeNode CreateNode(SiteMapNode siteMapNode)
        {
            var treeNode = new TreeNode();

            if (!string.IsNullOrEmpty(siteMapNode.Url))
            {
                treeNode.Href = Page.ResolveUrl(siteMapNode.Url);
            }

            treeNode.NodeID = siteMapNode.Key;
            treeNode.Text = siteMapNode.Title;
            treeNode.Qtip = siteMapNode.Description;

            SiteMapNodeCollection children = siteMapNode.ChildNodes;

            if (children != null && children.Count > 0)
            {
                foreach (SiteMapNode mapNode in siteMapNode.ChildNodes)
                {
                    treeNode.Nodes.Add(CreateNode(mapNode));
                }
            }

            return treeNode;
        }
    }
}