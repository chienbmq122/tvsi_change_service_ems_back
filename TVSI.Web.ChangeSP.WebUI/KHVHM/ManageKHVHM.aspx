﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageKHVHM.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.KHVHM.ManageKHVHM" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript">
</script>

    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>

    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
                    
            function grdRequestListCommand(command, data) {
                if (command == "View") {
                    USERMETHOD.ViewRequest(data.CustomerId);
                }
                if (command == "Delete") {
                    USERMETHOD.DeleteRequest(data.CustomerId);
                }
            }
            
            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';

  		         var renderCustomerStatus = function (value) {
                  if (value == "10")
                      return String.format(template, "#1ae5e5", "Tạo mới");
                  if (value == "20")
                      return String.format(template, "#1cff00", "Hoạt động"); 
                  if (value == "99")
                      return String.format(template, "#de081a", "Xóa logic");
                      
  					return value;
              };
                   
            var preStatus = function (grid, toolbar, rowIndex, record) {
                if (record.data.CustomerStatus != 10) {
                    toolbar.items.itemAt(0).hide();
             }
            };
            
            UpdateUploadInfo = function (el) {
                var ret = true;
        
                if (Ext.isIE) {
                    return;
                }
                
                var file = el.files[0];
                var size = file.size;
        
                if (size > 4194304) {
                    ret = false;
                } else {
                }
        
                return ret;
            }
        </script>
    </ext:XScript>
</head>
<body>

<ext:ResourceManager runat="server"/>
<form id="form2" runat="server">
<ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
    <Items>
        <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
            <TopBar>
                <ext:Toolbar runat="server">
                    <Items>
                        <ext:Label Text="Customer Code " runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:TextField runat="server" ID="txtSearchCustomerCode" Width="80"/>
                        <ext:ToolbarSeparator/>
                        <ext:Label Text="Họ tên KH " runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:TextField runat="server" ID="txtSearchCustomerName" Width="80"/>
                        <ext:Label Text="Trạng thái " runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:ComboBox runat="server" ID="cboSearchStatus" Width="100">
                            <Listeners>
                                <Select Handler="#{storeRequestList}.reload()"></Select>
                            </Listeners>
                        </ext:ComboBox>
                        <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                            <Listeners>
                                <Click Handler="#{storeRequestList}.reload()"/>
                            </Listeners>
                        </ext:Button>
                        <ext:ToolbarSeparator/>
                        <ext:Button runat="server" Text="Thêm mới" ID="btnInsertRequest" Icon="ApplicationAdd" CausesValidation="true">
                            <DirectEvents>
                                <Click OnEvent="btnInsertRequest_Click"/>
                            </DirectEvents>
                        </ext:Button>
                    </Items>
                </ext:Toolbar>
            </TopBar>
        </ext:Panel>
        <ext:GridPanel ID="grdRequestList" runat="server" Margins="0 0 0 5" Region="Center"
                       ColumnLines="True" StripeRows="True" ButtonAlign="Center">
            <Store>
                <ext:Store ID="storeRequestList" runat="server" OnRefreshData="storeRequestList_Refresh">
                    <Reader>
                        <ext:JsonReader IDProperty="CustomerId">
                            <Fields>
                                <ext:RecordField Name="CustomerId"/>
                                <ext:RecordField Name="CustomerCode"/>
                                <ext:RecordField Name="CustomerName"/>
                                <ext:RecordField Name="RegistrationDate"/>
                                <ext:RecordField Name="MarginGroup"/>
                                <ext:RecordField Name="InterestRate"/>
                                <ext:RecordField Name="PreferentialPackage"/>
                                <ext:RecordField Name="CustomerLevelName"/>
                                <ext:RecordField Name="LoanLimit"/>
                                <ext:RecordField Name="ApprovingLoanLimit"/>
                                <ext:RecordField Name="EffectiveDate"/>
                                <ext:RecordField Name="EndDate"/>
                                <ext:RecordField Name="AttachmentFileName"/>
                                <ext:RecordField Name="CustomerStatus"/>
                                <ext:RecordField Name="CreatedBy"/>
                                <ext:RecordField Name="CreatedDate"/>
                                <ext:RecordField Name="UpdatedBy"/>
                                <ext:RecordField Name="UpdatedDate"/>
                            </Fields>
                        </ext:JsonReader>
                    </Reader>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:RowNumbererColumn Width="20" Header="#"/>
                    <ext:Column DataIndex="CustomerId" Header="CUSTOMER ID" Width="50"/>
                    <ext:CommandColumn Width="70" DataIndex="command">
                        <Commands>
                            <ext:GridCommand Icon="Zoom" CommandName="View" Text="View"/>
                        </Commands>
                    </ext:CommandColumn>
                    <ext:CommandColumn Width="70" DataIndex="command">
                        <Commands>
                            <ext:GridCommand Icon="Delete" CommandName="Delete" Text="Delete"/>
                        </Commands>
                        <%--<PrepareToolbar Fn="preStatus"/>--%>
                    </ext:CommandColumn>
                    <ext:Column DataIndex="CustomerCode" Header="Số tài khoản" Width="60"/>
                    <ext:Column DataIndex="CustomerName" Header="Tên khách hàng" Width="100"/>
                    <ext:DateColumn DataIndex="RegistrationDate" Header="Ngày mở tài khoản" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="MarginGroup" Header="Gói SPDV" Width="100"/>
                    <ext:NumberColumn DataIndex="InterestRate" Header="Lãi suất" Width="100" Format="000,000.00"/>
                    <ext:Column DataIndex="PreferentialPackage" Header="Gói ưu đãi đang sử dụng" Width="100"/>
                    <ext:Column DataIndex="CustomerLevelName" Header="Hạng KH" Width="100"/>
                    <ext:NumberColumn DataIndex="LoanLimit" Header="Hạn mức cho vay theo hạng KH" Width="100" Format="000,000"/>
                    <ext:NumberColumn DataIndex="ApprovingLoanLimit" Header="Hạn mức phê duyệt cho vay vượt" Width="100" Format="000,000"/>
                    <ext:DateColumn DataIndex="EffectiveDate" Header="Ngày hiệu lực" Width="120" Format="dd/MM/yyyy"/>
                    <ext:DateColumn DataIndex="EndDate" Header="Ngày kết thúc" Width="120" Format="dd/MM/yyyy"/>
                    <ext:Column DataIndex="CustomerStatus" Header="Trạng thái" Width="80">
                        <Renderer Fn="renderCustomerStatus"></Renderer>
                    </ext:Column>
                    <ext:DateColumn DataIndex="CreatedDate" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="CreatedBy" Header="Người tạo" Width="120" Hidden="True"/>
                    <ext:DateColumn DataIndex="UpdatedDate" Header="Ngày cập nhật" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="UpdatedBy" Header="Người cập nhật" Width="120"/>
                </Columns>
            </ColumnModel>
            <Listeners>
                <Command Handler="grdRequestListCommand(command, record.data);">
            </Command>
            </Listeners>
            <LoadMask ShowMask="true"/>
            <BottomBar>
                <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:"/>
                        <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="20"/>
                                <ext:ListItem Text="40"/>
                                <ext:ListItem Text="60"/>
                                <ext:ListItem Text="80"/>
                            </Items>
                            <SelectedItem Value="40"/>
                            <Listeners>
                                <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"/>
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true"/>
            </SelectionModel>
        </ext:GridPanel>
    </Items>
</ext:Viewport>

<ext:Window runat="server" ID="winViewRequest" Title="Khách hàng vượt hạn mức" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="500" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
    <Items>
        <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmViewRequest">
            <Items>
                <ext:Hidden runat="server" ID="hidRequestID"></ext:Hidden>
                <ext:TextField runat="server" ID="frmViewRequest_txtCustomerCode" Width="200" FieldLabel="Số tài khoản" AllowBlank="False" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtCustomerName" Width="200" FieldLabel="Tên khách hàng" AllowBlank="False" ReadOnly="True"/>
                <ext:DateField runat="server" ID="frmViewRequest_dtxRegistrationDate" Width="150" FieldLabel="Ngày mở tài khoản" AllowBlank="False" Format="dd/MM/yyyy" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtMarginGroup" Width="200" FieldLabel="Gói SPDV" AllowBlank="False" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtInterestRate" Width="200" FieldLabel="Lãi suất" AllowBlank="False" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtPreferentialPackage" Width="200" FieldLabel="Gói ưu đãi đang sử dụng" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtCustomerLevelName" Width="150" FieldLabel="Hạng KH" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtLoanLimit" Width="150" FieldLabel="Hạn mức cho vay theo hạng KH" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmViewRequest_txtApprovingLoanLimit" Width="150" FieldLabel="Hạn mức phê duyệt cho vay vượt" ReadOnly="True"/>
                <ext:DateField runat="server" ID="frmViewRequest_dtxEffectiveDate" Width="150" FieldLabel="Ngày hiệu lực" Format="dd/MM/yyyy" ReadOnly="True"/>
                <ext:DateField runat="server" ID="frmViewRequest_dtxEndDate" Width="150" FieldLabel="Ngày kết thúc" Format="dd/MM/yyyy" ReadOnly="True"/>
                <ext:Button runat="server" Text="File đính kèm" Icon="BasketPut" ID="frmViewRequest_btnDownloadAttachment">
                    <DirectEvents>
                        <Click OnEvent="frmViewRequest_btnDownloadAttachment_Click" IsUpload="True"/>
                    </DirectEvents>
                </ext:Button>
                <ext:ComboBox runat="server" ID="frmViewRequest_cboCustomerStatus" Width="150" FieldLabel="Trạng thái" ReadOnly="True"/>
            </Items>
            <Buttons>
                <ext:Button runat="server" Text="Duyệt" Icon="Accept">
                    <DirectEvents>
                        <Click OnEvent="frmViewRequest_btnApprove_Click"/>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" Text="Đóng">
                    <DirectEvents>
                        <Click OnEvent="frmViewRequest_btnClose_Click"/>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </Items>
</ext:Window>

<ext:Window runat="server" ID="winDeleteRequest" Title="Xóa yêu cầu khách hàng vượt hạn mức"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
    <Items>
        <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDeleteRequest">
            <Buttons>
                <ext:Button runat="server" Text="Xóa yêu cầu" Icon="Delete">
                    <DirectEvents>
                        <Click OnEvent="frmDeleteRequest_btnDelete_Click">
                            <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý với việc xóa yêu cầu này?"/>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </Items>
</ext:Window>
<ext:Window runat="server" ID="winInsertRequest" Title="Thêm thông tin khách hàng vượt hạn mức"
            Hidden="True" Icon="Application" Height="500" Width="500" Layout="FitLayout" Modal="True">
    <Items>
        <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmInsertRequest">
            <Items>
                <ext:TextField runat="server" ID="frmInsertRequest_txtCustomerCode" Width="200" FieldLabel="Số tài khoản (6 số)" AllowBlank="False"/>
                <ext:Button runat="server" Height="10" Width="10" ID="frmInsertRequest_txtGetCustomerInformation" Text="Lấy thông tin" Icon="ApplicationGet">
                    <DirectEvents>
                        <Click OnEvent="frmInsertRequest_txtGetCustomerInformation_Click">
                        </Click>
                    </DirectEvents>
                </ext:Button>

                <ext:TextField runat="server" ID="frmInsertRequest_txtCustomerName" Width="200" FieldLabel="Tên khách hàng" AllowBlank="False" ReadOnly="True"/>
                <ext:DateField runat="server" ID="frmInsertRequest_dtxRegistrationDate" Width="150" FieldLabel="Ngày mở tài khoản" AllowBlank="False" Format="dd/MM/yyyy" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtMarginGroup" Width="200" FieldLabel="Gói SPDV" AllowBlank="False" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtInterestRate" Width="200" FieldLabel="Lãi suất" AllowBlank="False" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtPreferentialPackage" Width="200" FieldLabel="Gói ưu đãi đang sử dụng" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtCustomerLevelName" Width="150" FieldLabel="Hạng KH" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtLoanLimit" Width="150" FieldLabel="Hạn mức cho vay theo hạng KH" ReadOnly="True"/>
                <ext:TextField runat="server" ID="frmInsertRequest_txtApprovingLoanLimit" Width="150" FieldLabel="Hạn mức phê duyệt cho vay vượt" AllowBlank="False"/>
                <ext:DateField runat="server" ID="frmInsertRequest_dtxEffectiveDate" Width="150" FieldLabel="Ngày hiệu lực" Format="dd/MM/yyyy" AllowBlank="False"/>
                <ext:DateField runat="server" ID="frmInsertRequest_dtxEndDate" Width="150"  Format="dd/MM/yyyy" FieldLabel="Ngày kết thúc" AllowBlank="False"/>

                <ext:FileUploadField ID="frmInsertRequest_Attachment" runat="server" Width="300" Icon="Attach" FieldLabel="File đính kèm">
                    <Listeners>
                        <FileSelected Handler="if(UpdateUploadInfo(this.fileInput.dom)) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Dung lượng File tối đa: 4MB', buttons:Ext.Msg.OK}); this.reset(); return false;}">
                        </FileSelected>
                    </Listeners>
                </ext:FileUploadField>
            </Items>
            <Buttons>
                <ext:Button runat="server" ID="frmInsertRequest_btnInsert" Text="Thêm mới" Icon="ApplicationAdd">
                    <Listeners>
                        <Click Handler="if(#{frmInsertRequest}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}"/>
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="frmInsertRequest_btnInsert_Click">
                            <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý thêm mới?"/>
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </Items>
</ext:Window>
<ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchCustomerCode}">
    <ext:KeyBinding StopEvent="true">
        <Keys>
            <ext:Key Code="ENTER"/>
        </Keys>
        <Listeners>
            <Event Handler="#{storeRequestList}.reload()"/>
        </Listeners>
    </ext:KeyBinding>
</ext:KeyMap>
<ext:KeyMap ID="KeyMap2" runat="server" Target="#{txtSearchCustomerName}">
    <ext:KeyBinding StopEvent="true">
        <Keys>
            <ext:Key Code="ENTER"/>
        </Keys>
        <Listeners>
            <Event Handler="#{storeRequestList}.reload()"/>
        </Listeners>
    </ext:KeyBinding>
</ext:KeyMap>
</form>
</body>
</html>