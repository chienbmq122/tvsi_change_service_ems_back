﻿using Ext.Net;
using System;
using System.Globalization;
using System.IO;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;
using MessageBox = Ext.Net.MessageBox;
using TextField = Ext.Net.TextField;

namespace TVSI.Web.ChangeSP.WebUI.KHVHM
{
    public partial class ManageKHVHM : System.Web.UI.Page
    {
        private readonly KhachHangVuotHanMucService _myKhachHangVuotHanMucService;
        private readonly CommonService _myCommonService;

        protected ManageKHVHM()
        {
            _myKhachHangVuotHanMucService = new KhachHangVuotHanMucService();
            _myCommonService = new CommonService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var customerStatusDict = ConstParam.CustomerStatusDict;

                foreach (var item in customerStatusDict)
                {
                    cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                    frmViewRequest_cboCustomerStatus.Items.Add(new ListItem(item.Value, item.Key));
                }

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "-1"));
                cboSearchStatus.SelectedIndex = 0;
                GridDataBind();
            }
        }

        protected void storeRequestList_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var customerCode = txtSearchCustomerCode.Text.Trim();
            var customerName = txtSearchCustomerName.Text.Trim();
            var customerStatus = Convert.ToInt32(cboSearchStatus.SelectedItem.Value);
            if (customerStatus == 0)
            {
                customerStatus = 10;
            }

            var dt = _myKhachHangVuotHanMucService.Search(customerCode, customerName, customerStatus);
            if (dt != null)
            {
                grdRequestList.GetStore().DataSource = dt;
                grdRequestList.GetStore().DataBind();
            }
            else
            {
                grdRequestList.GetStore().DataSource = new object[] { };
                grdRequestList.GetStore().DataBind();
            }
        }

        public void frmViewRequest_btnApprove_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }

            var requestId = Convert.ToInt64(hidRequestID.Value);
            var result = _myKhachHangVuotHanMucService.UpdateStatus(requestId, 20, username);

            if (result > 0)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Message = "Bản ghi đã được duyệt"
                });

                frmViewRequest.Reset();
                winViewRequest.Hide();
                GridDataBind();
            }
        }

        public void frmDeleteRequest_btnDelete_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }

            var requestId = Convert.ToInt64(hidRequestID.Value);
            var result = _myKhachHangVuotHanMucService.UpdateStatus(requestId, 99, username);

            if (result > 0)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Message = "Bản ghi đã được xóa logic"
                });
                winDeleteRequest.Hide();
                GridDataBind();
            }
        }

        public void frmInsertRequest_txtGetCustomerInformation_Click(object sender, EventArgs e)
        {
            try
            {
                var customerCode = frmInsertRequest_txtCustomerCode.Text.Trim();
                if (customerCode.Length != 6)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Tài khoản không đủ 6 số hoặc thừa."
                    });
                    return;
                }

                if (string.IsNullOrEmpty(customerCode))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Bạn chưa nhập số tài khoản."
                    });
                    return;
                }

                var customerInformation = _myCommonService.TVSI_sREGSERVICE_GET_CUSTINFO(customerCode);
                if (customerInformation == null)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Không tìm thấy số tài khoản."
                    });
                    return;
                }

                frmInsertRequest_txtCustomerName.Text = customerInformation.ten_khach_hang.ToString();
                frmInsertRequest_dtxRegistrationDate.Value = customerInformation.ngay_mo_tai_khoan;
                frmInsertRequest_txtMarginGroup.Text = customerInformation.ten_nhom;
                frmInsertRequest_txtInterestRate.Text = customerInformation.lai_suat.ToString("N2");
                frmInsertRequest_txtPreferentialPackage.Text = customerInformation.goi_dich_vu_ud;
                frmInsertRequest_txtCustomerLevelName.Text = customerInformation.loai_vip;
                frmInsertRequest_txtLoanLimit.Text =
                    Functions.FormatDouble(customerInformation.han_muc_cho_vay_theo_hang_kh.ToString(), 0);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(ManageKHVHM), "frmInsertRequest_txtGetCustomerInformation_Click: " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
                return;
            }
        }

        public void frmInsertRequest_btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();

                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }

                var customerCode = frmInsertRequest_txtCustomerCode.Text.Trim();

                if (customerCode.Length != 6)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Tài khoản không đủ 6 số hoặc thừa."
                    });
                    return;
                }

                var attachmentFileName = string.Empty;
                if (frmInsertRequest_Attachment.HasFile)
                {
                    HttpPostedFile file = frmInsertRequest_Attachment.PostedFile;
                    string uploadPath = Server.MapPath("./attachment/");
                    string fileName = CommonUtils.ParaphraseFileName(frmInsertRequest_Attachment.PostedFile.FileName);
                    string fileFullName = Path.Combine(uploadPath, fileName);
                    FileInfo fileInfo = new FileInfo(fileFullName);
                    string fileExtension = fileInfo.Extension;
                    if (!string.IsNullOrWhiteSpace(fileExtension))
                    {
                        file.SaveAs(fileFullName);

                        if (File.Exists(fileFullName))
                        {
                            attachmentFileName = fileName;
                        }
                        else
                        {
                            ExtNet.Msg.Show(new MessageBoxConfig
                            {
                                Title = "Thông báo",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.ERROR,
                                Message = "Có lỗi xảy ra khi upload File"
                            });
                            return;
                        }
                    }
                }

                var newRequest = new KhachHangVuotHanMucModel
                {
                    CustomerCode = frmInsertRequest_txtCustomerCode.Text.Trim(),
                    CustomerName = frmInsertRequest_txtCustomerName.Text.Trim(),
                    RegistrationDate = frmInsertRequest_dtxRegistrationDate.SelectedDate,
                    MarginGroup = frmInsertRequest_txtMarginGroup.Text.Trim(),
                    InterestRate = Convert.ToDecimal(frmInsertRequest_txtInterestRate.Text),
                    PreferentialPackage = frmInsertRequest_txtPreferentialPackage.Text.Trim(),
                    CustomerLevelName = frmInsertRequest_txtCustomerLevelName.Text.Trim(),
                    LoanLimit = Convert.ToDecimal(frmInsertRequest_txtLoanLimit.Text),
                    ApprovingLoanLimit = Convert.ToDecimal(frmInsertRequest_txtApprovingLoanLimit.Text),
                    EffectiveDate = frmInsertRequest_dtxEffectiveDate.SelectedDate,
                    EndDate = frmInsertRequest_dtxEndDate.SelectedDate,
                    AttachmentFileName = attachmentFileName,
                    CustomerStatus = 10,
                    CreatedBy = username,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = username,
                    UpdatedDate = DateTime.Now
                };

                var result = _myKhachHangVuotHanMucService.Insert(newRequest);

                if (result > 0)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Thêm mới thành công"
                    });

                    frmInsertRequest.Reset();
                    winInsertRequest.Hide();
                    GridDataBind();
                }
                else
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Có lỗi xảy ra"
                    });
                }
            }
            catch (Exception exception)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra"
                });
            }
        }

        public void frmViewRequest_btnDownloadAttachment_Click(object sender, EventArgs e)
        {
            string attachmentName = string.Empty;
            long requestId = Convert.ToInt64(hidRequestID.Value.ToString());

            var requestDetail = _myKhachHangVuotHanMucService.SelectById(requestId);
            attachmentName = requestDetail.AttachmentFileName;

            string downloadPath = Server.MapPath("./attachment/");
            string fileFullName = Path.Combine(downloadPath, attachmentName);

            FileInfo file = new FileInfo(fileFullName);
            if (file.Exists)
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + attachmentName);
                Response.WriteFile(fileFullName);
                Response.Flush();
                Response.End();
            }
        }

        protected void btnInsertRequest_Click(object sender, EventArgs e)
        {
            frmInsertRequest.Reset();
            winInsertRequest.Show();
        }

        protected void frmViewRequest_btnClose_Click(object sender, EventArgs e)
        {
            frmViewRequest.Reset();
            winViewRequest.Hide();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewRequest(long requestId)
        {
            frmViewRequest.Reset();

            hidRequestID.Value = requestId;
            var requestDetail = _myKhachHangVuotHanMucService.SelectById(requestId);

            frmViewRequest_txtCustomerCode.Text = requestDetail.CustomerCode;
            frmViewRequest_txtCustomerName.Text = requestDetail.CustomerName;
            frmViewRequest_dtxRegistrationDate.Value = requestDetail.RegistrationDate;
            frmViewRequest_txtMarginGroup.Text = requestDetail.MarginGroup;
            frmViewRequest_txtInterestRate.Text = requestDetail.InterestRate.ToString("N2");
            frmViewRequest_txtPreferentialPackage.Text = requestDetail.PreferentialPackage;
            frmViewRequest_txtCustomerLevelName.Text = requestDetail.CustomerLevelName;
            frmViewRequest_txtLoanLimit.Text = Functions.FormatDouble(requestDetail.LoanLimit, 2);
            frmViewRequest_txtApprovingLoanLimit.Text = Functions.FormatDouble(requestDetail.ApprovingLoanLimit, 2);
            frmViewRequest_dtxEffectiveDate.Value = requestDetail.EffectiveDate;
            frmViewRequest_dtxEndDate.Value = requestDetail.EndDate;
            frmViewRequest_btnDownloadAttachment.Text = requestDetail.AttachmentFileName;
            frmViewRequest_cboCustomerStatus.SelectedItem.Value = requestDetail.CustomerStatus.ToString();

            winViewRequest.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeleteRequest(long requestId)
        {
            hidRequestID.Value = requestId;

            winDeleteRequest.Show();
        }
    }
}