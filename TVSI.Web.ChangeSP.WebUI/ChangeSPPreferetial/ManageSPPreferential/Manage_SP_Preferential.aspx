﻿<%@ Page Language="C#" CodeBehind="Manage_SP_Preferential.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.ChangeSP.ManageSPPreferential.Manage_SP_Preferential" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdSPList(command, data) {
                if (command == "Edit") {
                    USERMETHOD.ViewServicePackDetail(data.goi_dich_vu_uu_daiid);
                }
                if (command == "Delete") {
                    USERMETHOD.DeleteServicePack(data.goi_dich_vu_uu_daiid);
                }
            }

            var renderSPStatus = function (value) {
                if (value == "1")
                    return "Hoạt động"
                if (value == "99")
                    return "Xóa logic"
            };
        </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager runat="server" />
    <form id="form2" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="Trạng thái " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:ComboBox runat="server" ID="cboSearchStatus" Width="100">
                                    <Listeners>
                                        <Select Handler="#{storeListServicePack}.reload()"></Select>
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeListServicePack}.reload()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Thêm mới" ID="btnAdd" Icon="ApplicationAdd" CausesValidation="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddServicePack_Click" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdListServicePack" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeListServicePack" runat="server" OnRefreshData="storeListServicePack_Refresh">
                            <Reader>
                                <ext:JsonReader IDProperty="goi_dich_vu_uu_daiid">
                                    <Fields>
                                        <ext:RecordField Name="goi_dich_vu_uu_daiid" />
                                        <ext:RecordField Name="ten_goi_dich_vu_uu_dai" />
                                        <ext:RecordField Name="lai_suat_toi_thieu" />
                                        <ext:RecordField Name="ly_le_phi_toi_thieu" />
                                        <ext:RecordField Name="phi_giao_dich_toi_thieu" />
                                        <ext:RecordField Name="so_ngay_mien_lai" />
                                        <ext:RecordField Name="vong_quay_du_no_thang" />
                                        <ext:RecordField Name="du_no_toi_da" />
                                        <ext:RecordField Name="phi_dich_vu_goi" />
                                        <ext:RecordField Name="ty_le_vay_toi_da" />
                                        <ext:RecordField Name="so_luong_ma_cho_vay" />
                                        <ext:RecordField Name="doi_tuong" />
                                        <ext:RecordField Name="mo_ta" />
                                        <ext:RecordField Name="trang_thai" />
                                        <ext:RecordField Name="nguoi_tao" />
                                        <ext:RecordField Name="ngay_tao" />
                                        <ext:RecordField Name="nguoi_cap_nhat" />
                                        <ext:RecordField Name="ngay_cap_nhat" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="20" Header="#" />
                            <ext:Column DataIndex="goi_dich_vu_uu_daiid" Header="GDV ID" Width="50" />
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="ApplicationEdit" CommandName="Edit" Text="Edit" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Text="Delete" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:Column DataIndex="ten_goi_dich_vu_uu_dai" Header="Tên GDV ưu đãi" Width="60" />
                            <ext:Column DataIndex="lai_suat_toi_thieu" Header="Lãi suất tối thiểu" Width="80" />
                            <ext:Column DataIndex="ly_le_phi_toi_thieu" Header="lệ phí tối thiểu" Width="80" />
                            <ext:Column DataIndex="phi_giao_dich_toi_thieu" Header="Phí giao dịch tối thiểu" Width="80" />
                            <ext:Column DataIndex="so_ngay_mien_lai" Header="Số ngày miễn lãi" Width="80" />
                            <ext:Column DataIndex="vong_quay_du_no_thang" Header="Vòng quay dư nợ tháng" Width="80" />
                            <ext:Column DataIndex="du_no_toi_da" Header="Hạn mức dư nợ được ưu đãi" Width="80" />
                            <ext:Column DataIndex="phi_dich_vu_goi" Header="Phí gia hạn khoản vay" Width="80" />
                            <%--
                            <ext:Column DataIndex="ty_le_vay_toi_da" Header="Tỷ lệ vay toi đa" Width="80" />
                            --%>
                            <ext:Column DataIndex="so_luong_ma_cho_vay" Header="Số lượng mã cho vay" Width="80" />
                            <%--
                            <ext:Column DataIndex="doi_tuong" Header="Đối tượng" Width="80" />
                            --%>
                            <%--
                            <ext:Column DataIndex="mo_ta" Header="Mô tả" Width="80" />
                            --%>
                            <ext:Column DataIndex="trang_thai" Header="Trạng thái" Width="80">
                                <Renderer Fn="renderSPStatus"></Renderer>
                            </ext:Column>
                            <ext:DateColumn DataIndex="ngay_tao" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_tao" Header="Người tạo" Width="120" />
                            <ext:DateColumn DataIndex="ngay_cap_nhat" Header="Ngày cập nhật" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_cap_nhat" Header="Người cập nhật" Width="120" />
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdSPList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window runat="server" ID="winServicePackDetail" Title="Yêu cầu chỉnh sửa GDV" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditService">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditTen_goi_dich_vu_uu_dai" Width="200" FieldLabel="Tên GDV ưu đãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditlai_suat_toi_thieu" Width="200" FieldLabel="Lãi suất tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditly_le_phi_toi_thieu" Width="200" FieldLabel="Lệ phí tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditphi_giao_dich_toi_thieu" Width="200" FieldLabel="phí giao dịch tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditso_ngay_mien_lai" Width="200" FieldLabel="Số ngày miễn lãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditvong_quay_du_no_thang" Width="200" FieldLabel="Vòng quay dư nợ để đạt lãi suất tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditdu_no_toi_da" Width="200" FieldLabel="Hạn mức dư nợ được ưu đãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditphi_dich_vu_goi" Width="200" FieldLabel="Phí gia hạn khoản vay" AllowBlank="False" />
                        <%--
                        <ext:TextField runat="server" ID="txtEditty_le_vay_toi_da" Width="200" FieldLabel="Tỷ lệ vay tối đa" AllowBlank="False" />
                        --%>
                        <ext:TextField runat="server" ID="txtEditso_luong_ma_cho_vay" Width="200" FieldLabel="Số lượng mã cho vay" AllowBlank="False" />
                        <%--
                        <ext:TextField runat="server" ID="txtEditdoi_tuong" Width="200" FieldLabel="Đối tượng" AllowBlank="False" />
                        --%>
                        <%--
                        <ext:TextField runat="server" ID="txtEditmo_ta" Width="200" FieldLabel="Mô tả cho gói" AllowBlank="False" />
                        --%>
                        <ext:ComboBox runat="server" ID="cboEditTrang_thai" Width="200" FieldLabel="Trạng thái" AllowBlank="False" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Cập nhật" Icon="ApplicationEdit">
                            <Listeners>
                                <Click Handler="if(#{frmEditService}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnEditUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật GDV này không?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winAddNewServicePack" Title="Thêm GDV"
            Hidden="True" Icon="Application" Height="450" Width="400" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmAddService">
                    <Items>
                        <ext:TextField runat="server" ID="txtAddTen_goi_dich_vu_uu_dai" Width="200" FieldLabel="Tên GDV" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddlai_suat_toi_thieu" Width="200" FieldLabel="Lãi suất tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddly_le_phi_toi_thieu" Width="200" FieldLabel="Lệ phí tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddphi_giao_dich_toi_thieu" Width="200" FieldLabel="Phí giao dịch tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddso_ngay_mien_lai" Width="200" FieldLabel="Số ngày miễn lãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddvong_quay_du_no_thang" Width="200" FieldLabel="Vòng quay dư nợ để đạt lãi suất tối thiểu" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAdddu_no_toi_da" Width="200" FieldLabel="Hạn mức dư nợ được ưu đãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddphi_dich_vu_goi" Width="200" FieldLabel="Phí gia hạn khoản vay" AllowBlank="False" />
                        <%--
                        <ext:TextField runat="server" ID="txtAddty_le_vay_toi_da" Width="200" FieldLabel="Tỷ lệ vay tối đa" AllowBlank="False" />
                        --%>
                        <ext:TextField runat="server" ID="txtAddso_luong_ma_cho_vay" Width="200" FieldLabel="Số lượng mã cho vay" AllowBlank="False" />
                        <%--
                        <ext:TextField runat="server" ID="txtAdddoi_tuong" Width="200" FieldLabel="Đối tượng" AllowBlank="False" />
                        --%>
                        <%--
                        <ext:TextField runat="server" ID="txtAddmo_ta" Width="200" FieldLabel="Mô tả" AllowBlank="False" />
                        --%>
                        <ext:ComboBox runat="server" ID="cboAddTrang_thai" Width="200" FieldLabel="Trạng thái" ReadOnly="True" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" ID="btnConfAddServicePack" Text="Thêm mới" Icon="ApplicationAdd">
                            <Listeners>
                                <Click Handler="if(#{frmAddService}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnConfAddServicePack_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý thêm GDV này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeleteServicePack" Title="Xóa GDV"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecService">
                    <Buttons>
                        <ext:Button runat="server" Text="Xóa GDV" Icon="Delete">
                            <DirectEvents>
                                <Click OnEvent="btnDeleteServicePack_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý với việc xóa GDV này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>
    </form>
</body>
</html>