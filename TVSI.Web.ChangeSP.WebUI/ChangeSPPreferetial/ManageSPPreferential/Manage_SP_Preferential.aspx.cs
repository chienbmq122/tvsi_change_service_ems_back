﻿using System;
using System.Web.UI;
using Ext.Net;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.ManageSPPreferential
{
    public partial class Manage_SP_Preferential : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var statusDict = ConstParam.ServicePackStatusDict;

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                foreach (var item in statusDict)
                {
                    cboAddTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                }

                cboAddTrang_thai.SelectedIndex = 0;
                cboSearchStatus.SelectedIndex = 1;

                GridDataBind();
            }
        }

        protected void storeListServicePack_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var status = cboSearchStatus.SelectedItem.Value;

            var servicePackService = new ManageServicePackServicePreferential();
            var servicePackInfo = servicePackService.SearchServicePackList(status);

            grdListServicePack.GetStore().DataSource = servicePackInfo;
            grdListServicePack.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewServicePackDetail(long sp_id)
        {
            try
            {
                hidID.Value = sp_id;
                var servicePackService = new ManageServicePackServicePreferential();
                var servicePackInfo = servicePackService.GetServicePackByID(sp_id);
                txtEditTen_goi_dich_vu_uu_dai.Text = servicePackInfo.ten_goi_dich_vu_uu_dai;
                txtEditlai_suat_toi_thieu.Text = servicePackInfo.lai_suat_toi_thieu.ToString();
                txtEditly_le_phi_toi_thieu.Text = servicePackInfo.ly_le_phi_toi_thieu.ToString();
                txtEditphi_giao_dich_toi_thieu.Text = servicePackInfo.phi_giao_dich_toi_thieu.ToString();
                txtEditso_ngay_mien_lai.Text = servicePackInfo.so_ngay_mien_lai.ToString();
                txtEditvong_quay_du_no_thang.Text = servicePackInfo.vong_quay_du_no_thang.ToString();
                txtEditdu_no_toi_da.Text = servicePackInfo.du_no_toi_da.ToString();
                txtEditphi_dich_vu_goi.Text = servicePackInfo.phi_dich_vu_goi.ToString();
                /*
                txtEditty_le_vay_toi_da.Text = servicePackInfo.ty_le_vay_toi_da ?? string.Empty;
                */
                txtEditso_luong_ma_cho_vay.Text = servicePackInfo.so_luong_ma_cho_vay.ToString();
                /*
                txtEditdoi_tuong.Text = servicePackInfo.doi_tuong ?? string.Empty;
                */
                /*
                txtEditmo_ta.Text = servicePackInfo.mo_ta ?? string.Empty;
                */
                cboEditTrang_thai.SelectedItem.Value = servicePackInfo.trang_thai.ToString();
                winServicePackDetail.Show();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(Manage_SP_Preferential), ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }

        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeleteServicePack(long sp_id)
        {
            hidID.Value = sp_id;
            winDeleteServicePack.Show();
        }

        public void btnEditUser_Click(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var sp_id = int.Parse(hidID.Value + "");

            var spName = txtEditTen_goi_dich_vu_uu_dai.Text.Trim();
            var laisuattoithieu = txtEditlai_suat_toi_thieu.Text.Trim();
            var lephitoithieu = txtEditly_le_phi_toi_thieu.Text.Trim();
            var phigiaodichtoitieu = txtEditphi_giao_dich_toi_thieu.Text.Trim();
            var songaymienlai = txtEditso_ngay_mien_lai.Text.Trim();
            var vongquaydunothang = txtEditvong_quay_du_no_thang.Text.Trim();
            var dunotoida = txtEditdu_no_toi_da.Text.Trim();
            var phidichvugoi = txtEditphi_dich_vu_goi.Text.Trim();
            /*
            var tylevaytoida = txtEditty_le_vay_toi_da.Text.Trim();
            */
            var soluongmachovay = txtEditso_luong_ma_cho_vay.Text.Trim();
            /*
            var doituong = txtEditdoi_tuong.Text.Trim();
            */
            /*
            var mota = txtEditmo_ta.Text.Trim();
            */
            var status = int.Parse(cboEditTrang_thai.SelectedItem.Value);
            
            double dsongaymienlai = 0;


            if (!Double.TryParse(songaymienlai, out dsongaymienlai)
            )
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Vui lòng nhập đúng kiểu dữ liệu số vào đúng các " +
                              "trường : SÔ NGÀY MIỄN LÃI"
                });
                return;
            }

            var servicePackService = new ManageServicePackServicePreferential();

            var editedUser = servicePackService.UpdateServicePackByID(sp_id,spName,laisuattoithieu, lephitoithieu, phigiaodichtoitieu,
                dsongaymienlai,vongquaydunothang,dunotoida,phidichvugoi,null,
                soluongmachovay,null,null,status);
            if (editedUser)
            {
                winServicePackDetail.Hide();
                GridDataBind();
            }
        }

        public void btnAddServicePack_Click(object sender, EventArgs e)
        {
            /*txtAddTen_goi_dich_vu_uu_dai.Text = "";
            txtAddlai_suat_toi_thieu.Text = "";
            txtAddly_le_phi_toi_thieu.Text = "";*/
            cboAddTrang_thai.SelectedIndex = 0;
            winAddNewServicePack.Show();
            
        }

        public void btnConfAddServicePack_Click(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var servicePackService = new ManageServicePackServicePreferential();
            var spName = txtAddTen_goi_dich_vu_uu_dai.Text.Trim();
            var laisuattoithieu = txtAddlai_suat_toi_thieu.Text.Trim();
            var lephitoithieu = txtAddly_le_phi_toi_thieu.Text.Trim();
            var phigiaodichtoitieu = txtAddphi_giao_dich_toi_thieu.Text.Trim();
            var songaymienlai = txtAddso_ngay_mien_lai.Text.Trim();
            var vongquaydunothang = txtAddvong_quay_du_no_thang.Text.Trim();
            var dunotoida = txtAdddu_no_toi_da.Text.Trim();
            var phidichvugoi = txtAddphi_dich_vu_goi.Text.Trim();
            /*
            var tylevaytoida = txtAddty_le_vay_toi_da.Text.Trim();
            */
            var soluongmachovay = txtAddso_luong_ma_cho_vay.Text.Trim();
            /*
            var doituong = txtAdddoi_tuong.Text.Trim();
            */
            /*
            var mota = txtAddmo_ta.Text.Trim();
            */
            var servicePackExisted = servicePackService.CheckExistingSPByName(spName);
            if (servicePackExisted)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Một GDV với tên này đã tồn tại"
                });
                return;
            }
            double dInterestRate = 0;
            double dCallRate = 0;
            double dlaisuattoithieu = 0;
            double dlephitoithieu = 0;
            double dphigiaodichtoitieu = 0;
            double dsongaymienlai = 0;
            double dvongquaydunothang = 0;
            double ddunotoida = 0;
            double dsoluongmachovay = 0;

            if (!Double.TryParse(songaymienlai, out dsongaymienlai)
            )
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Vui lòng nhập đúng kiểu dữ liệu số vào đúng các " +
                              "trường : SÔ NGÀY MIỄN LÃI"
                });
                return;
            }

            var addedUser = servicePackService.AddServicePack(spName, laisuattoithieu, lephitoithieu,
                phigiaodichtoitieu,dsongaymienlai,
                vongquaydunothang,dunotoida, phidichvugoi,
                null,soluongmachovay,null,null  );

            if (addedUser)
            {
                winAddNewServicePack.Hide();
                GridDataBind();
            }
        }

        public void btnDeleteServicePack_Click(object sender, EventArgs e)
        {
            var servicePackService = new ManageServicePackServicePreferential();
            var sp_id = int.Parse(hidID.Value + "");
            var changed = servicePackService.ChangeSPStatusByID(sp_id, 99);

            if (changed)
            {
                winDeleteServicePack.Hide();
                GridDataBind();
            }
        }
    }
}