﻿<%@ Page Language="C#" CodeBehind="CancelUserSPPreferential.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.ChangeSPPreferetial.CancelUserSPPreferential.CancelUserSPPreferential" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdStockApiList(command, data) {
                if (command == "View") {
                    USERMETHOD.ViewUserSPDetail(data.goi_dich_vu_uu_daiid);
                }
                if (command == "Approve") {
                    USERMETHOD.ApproveUserSPChange(data.goi_dich_vu_uu_daiid);
                }
                if (command == "Decline") {
                    USERMETHOD.DeclineUserSPChange(data.goi_dich_vu_uu_daiid);
                }
            }

            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';

       var renderSPStatus = function (value) {
                     if (value == "-1")
                         return String.format(template, "#ffab43", "Từ chối");
                     if (value == "0")
                         return String.format(template, "#1ae5e5", "Tạo mới");
                     if (value == "1")
                         return String.format(template, "#1cff00", " Thành công");
                     if(value == "5")
                          return String.format(template, "#f108e5", "Hủy đăng ký gói"); 
                     if (value == "99")
                         return String.format(template, "#de081a", "Xóa");  
                     if (value == "-2")
                         return String.format(template, "#134bf5", "Đã hủy");   
                     if(value == "-3")
                         return String.format(template, "#721101", "Ngừng sử dụng");
                         
     					return value;
                 };
             var prepare = function (grid, toolbar, rowIndex, record) {
                        if (record.data.trang_thai ==5 ) {
                            toolbar.items.itemAt(0).show();
                        }
             };
             
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
        </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager runat="server" />
    <form id="form1" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="Số TK " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchAccountNo" Width="80" />
                                <ext:ToolbarSeparator />
                                <ext:Label Text="Họ tên KH " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchUserName" Width="120" />
                                <ext:ToolbarSeparator />
                                <ext:Label Text="Trạng thái " runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:ComboBox runat="server" ID="cboSearchStatus" Width="100">
                                    <Listeners>
                                        <Select Handler="#{storeListUserService}.reload()"></Select>
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ToolbarSeparator/>

                                <ext:Label Text="Từ ngày đăng ký" runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:DateField ID="dtxSearchFrom" Format="dd/MM/yyyy" runat="server" Width="70">
                                    <Listeners>
                                        <Select Handler="#{storeListUserService}.reload()"></Select>
                                    </Listeners>
                                </ext:DateField>
                                <ext:ToolbarSpacer/>
                                <ext:Label Text="Đến ngày đăng ký" runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:DateField ID="dtxSearchTo" Format="dd/MM/yyyy" runat="server" Width="70">
                                    <Listeners>
                                        <Select Handler="#{storeListUserService}.reload()"></Select>
                                    </Listeners>
                                </ext:DateField>
                                <ext:ToolbarSeparator/>
                                <ext:ToolbarSeparator />

                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeListUserService}.reload()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Export" Text="Export" runat="server" Icon="pagewhiteexcel">
                                    <DirectEvents>
                                        <Click OnEvent="btnExport_Click" IsUpload="true">
                                            <EventMask ShowMask="false"/>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button runat="server" Text="Duyệt all hủy gói TK đã chọn" Icon="StatusBeRightBack">
                                    <DirectEvents>
                                        <Click onEvent="btnApproveAll">
                                            <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý gửi danh sách đã chọn không?"/>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button runat="server" Text="Duyệt All Trở về trạng thái Duyệt goi" ID="BntTuChoiAll" Icon="StatusBusy" CausesValidation="true">
                                    <DirectEvents>
                                        <Click OnEvent="rejectAll"/>
                                    </DirectEvents>
                                </ext:Button>
                                
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdListUserService" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeListUserService" runat="server" OnRefreshData="storeListUserService_Refresh">
                            <Reader>
                                <ext:JsonReader IDProperty="goi_dich_vu_uu_daiid">
                                    <Fields>
                                        <ext:RecordField Name="goi_dich_vu_uu_daiid" />
                                        <ext:RecordField Name="so_tai_khoan" />
                                        <ext:RecordField Name="ho_ten_khach_hang" />
                                        <ext:RecordField Name="ngay_dang_ky" />
                                        <ext:RecordField Name="ngay_hieu_luc" />
                                        <ext:RecordField Name="ngay_ket_thuc" />
                                        <ext:RecordField Name="goi_dich_vu_hien_tai" />
                                        <ext:RecordField Name="goi_dich_vu_moi" />
                                        <ext:RecordField Name="ly_do_tu_choi" />
                                        <ext:RecordField Name="trang_thai" />
                                        <ext:RecordField Name="ngay_tao" />
                                        <ext:RecordField Name="nguoi_tao" />
                                        <ext:RecordField Name="ngay_phe_duyet" />
                                        <ext:RecordField Name="nguoi_phe_duyet" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="20" Header="STT" />
                            <ext:Column DataIndex="goi_dich_vu_uu_daiid" Header="GDV ID" Width="50" />
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Zoom" CommandName="View" Text="View" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Accept" CommandName="Approve" Text="Chấp nhận hủy" />
                                </Commands>
                                <PrepareToolbar Fn="prepare" />
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="PageBack" CommandName="Decline" Text="Trở về trạng thái Duyệt gói" />
                                </Commands>
                               <PrepareToolbar Fn="prepare" />
                            </ext:CommandColumn>
                            <ext:Column DataIndex="so_tai_khoan" Header="Số TK" Width="60" />
                            <ext:Column DataIndex="ho_ten_khach_hang" Header="Tên KH" Width="100" />
                            <ext:DateColumn DataIndex="ngay_dang_ky" Header="Ngày đăng ký" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:DateColumn DataIndex="ngay_hieu_luc" Header="Ngày hiệu lực" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:DateColumn DataIndex="ngay_ket_thuc" Header="Ngày kết thúc" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="goi_dich_vu_hien_tai" Header="GDV hiện tại" Width="100" />
                            <ext:Column DataIndex="goi_dich_vu_moi" Header="GDV mới" Width="100" />
                            <ext:Column DataIndex="ly_do_tu_choi" Header="Lý do từ chối" Width="200" Hidden="True" />
                            <ext:Column DataIndex="trang_thai" Header="Trạng thái" Width="80">
                                <Renderer Fn="renderSPStatus"></Renderer>
                            </ext:Column>
                            <ext:DateColumn DataIndex="ngay_tao" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_tao" Header="Người tạo" Width="120" />
                            <ext:DateColumn DataIndex="ngay_phe_duyet" Header="Ngày phê duyệt" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_phe_duyet" Header="Người phê duyệt" Width="120" />
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdStockApiList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                    <SelectionModel>
                        <%--
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    --%>
                                                <ext:CheckboxSelectionModel runat="server" Mode="Multi" ID="RowSelectionModel" />

                    </SelectionModel>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window runat="server" ID="winUserSPDetail" Title="Yêu cầu chuyển đổi GDV" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditUser">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditSo_tai_khoan" Width="200" FieldLabel="Số tài khoản" AllowBlank="False" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditTen_khach_hang" Width="200" FieldLabel="Họ và tên khách hàng" AllowBlank="False" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditTrang_thai" Width="150" FieldLabel="Trạng thái" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditGDV_hien_tai" Width="150" FieldLabel="GDV Hiện tại" AllowBlank="False" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditGDV_moi" Width="150" FieldLabel="GDV mới" AllowBlank="False" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_dang_ky" Width="150" FieldLabel="Ngày đăng ký" AllowBlank="False" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_hieu_luc" Width="150" FieldLabel="Ngày hiệu lực" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_ket_thuc" Width="150" FieldLabel="Ngày kết thúc" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditLy_do_tu_choi" Width="200" FieldLabel="Lý do từ chối" ReadOnly="True" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Đóng" Icon="Accept">
                            <Listeners>
                                <Click Handler="if(#{frmEditUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnViewUser_Click" />
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>
        <ext:Window runat="server" ID="winDeclineAllUser" Title="Trở về trạng thái (Tạo gói) all user"
                Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
        <Items>
            <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecallUser">
                <Items>
                    <ext:TextField runat="server" ID="txtDecLy_do_tu_choi_all" Width="200" FieldLabel="Lý do " AllowBlank="False"/>
                </Items>
                <Buttons>
                    <ext:Button runat="server" Text="Từ chối" Icon="Decline">
                        <Listeners>
                            <Click Handler="if(#{frmDecallUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}"/>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="BtnRejectAll">
                                <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có chắc chắn từ chối all user này?"/>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

        <ext:Window runat="server" ID="winApproveUser" Title="Duyệt hủy gói cho user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmApprvUser">
                    <Buttons>
                        <ext:Button runat="server" Text="Chấp thuận" Icon="Accept">
                            <DirectEvents>
                                <Click OnEvent="btnApproveUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có chấp thuận việc hủy gói cho user này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeclineUser" Title="trở về trạng thái tạo mới user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecUser">
                    <Items>
                        <ext:TextField runat="server" ID="txtDecLy_do_tu_choi" Width="200" FieldLabel="Vui lòng nhâp lý do" AllowBlank="False" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Trở về" Icon="PageBack">
                            <Listeners>
                                <Click Handler="if(#{frmDecUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnDeclineUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có chắc chắn làm điều này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchAccountNo}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
        <ext:KeyMap ID="KeyMap2" runat="server" Target="#{txtSearchUserName}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
    </form>
</body>
</html>