﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Ext.Net;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using TVSI.Web.ChangeSP.WebUI.ChangeSP.ApproveUserSPPreferential;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSPPreferetial.CancelUserSPPreferential
{
    public partial class CancelUserSPPreferential : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    dtxSearchFrom.SelectedDate = dtxSearchTo.SelectedDate = DateTime.MinValue;

                    var userSPService = new ChangeUserServicePackagePreferential();
                    Logger.Info(typeof(ChangeUserServicePackagePreferential), "Buoc1 Model " + userSPService);
                    var servicePackList = userSPService.GetSP();
                    Logger.Info(typeof(ChangeUserServicePackagePreferential), "Buoc2 Model " + servicePackList);
                    if (servicePackList != null)
                    {
                        foreach (var item in servicePackList)
                        {
                            cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                            cboEditGDV_moi.Items.Add(new ListItem(item, item));
                        }
                    }

                    var statusDict = ConstParam.StatusDict;
                    Logger.Info(typeof(ChangeUserServicePackagePreferential), "Buoc3 ConstParam" + statusDict);
                    cboSearchStatus.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                    cboEditTrang_thai.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                    if (statusDict.Count > 0)
                    {
                        Logger.Info(typeof(approve_user_service_package), "Buoc4 - forech statusDict khi len hon 0");
                        foreach (var item in statusDict)
                        {
                            cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                            cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                        }
                    }

                    Logger.Info(typeof(approve_user_service_package),
                        " Buoc 5 Add trang thai vo cboSearchStatus,cboEditTrang_thai");
                    cboSearchStatus.SelectedIndex = 0;
                    cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                    GridDataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(approve_user_service_package), ex.Message);
            }
        }

        protected void btnApproveAll(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var userSPService = new ChangeUserServicePackagePreferential();
            StringBuilder result = new StringBuilder();
            RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;
            var list = sm.SelectedRows;

            var messageBoxNotSelect = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
            };
            if (list.Count == 0)
            {
                ExtNet.Msg.Show(messageBoxNotSelect);
            }

            var changed = false;
            var messageBoxInsertError = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
            };
            foreach (SelectedRow row in list)
            {
                changed = userSPService.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), -2, "");
                if (changed == false)
                {
                    ExtNet.Msg.Show(messageBoxInsertError);
                }
            }

            GridDataBind();
        }

        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var curStatus = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "5";
            }

            var userSPService = new ChangeUserServicePackagePreferential();
            userSPService.ExportListUserPreCancel(accountNo, userName, "", curStatus, fromDate, toDate,
                "", "");
        }

        private void GridDataBind()
        {
            Logger.Info(typeof(approve_user_service_package), "GridDataBind ");
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var curStatus = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            var userSPService = new ChangeUserServicePackagePreferential();
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "5";
            }

            var userInfo = userSPService.SearchUserServicePackListCancel(accountNo, userName, "", 
                curStatus,fromDate,toDate,"","");
            Logger.Info(typeof(approve_user_service_package), "B4 GridDataBind - sql userInfo > " + userInfo);
            if (userInfo != null)
            {
                grdListUserService.GetStore().DataSource = userInfo;
                grdListUserService.GetStore().DataBind();
            }
            else
            {
                grdListUserService.GetStore().DataSource = new List<object>();
                grdListUserService.GetStore().DataBind();
            }
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePackagePreferential();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;

            winUserSPDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ApproveUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            winApproveUser.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeclineUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePackagePreferential();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtDecLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            winDeclineUser.Show();
        }

        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserSPDetail.Hide();
        }

        public void btnApproveUser_Click(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var userSPService = new ChangeUserServicePackagePreferential();

            var sp_id = Convert.ToInt32(hidID.Value);
            var declineReason = txtDecLy_do_tu_choi.Text.Trim();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            var changed = userSPService.ChangeUserSPStatusByIDCancel(sp_id, -2, declineReason);

            if (changed)
            {
                winApproveUser.Hide();
                GridDataBind();
                var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                accNumLengString += "1";
                var userName = userInfo.ho_ten_khach_hang;
                var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);
                if (!string.IsNullOrEmpty(userEmail))
                {
                    var curSP = userInfo.goi_dich_vu_hien_tai;
                    var newSP = userInfo.goi_dich_vu_moi;
                    var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                    // gửi mail khi được duyệt lệnh thành công
                    userSPService.SendConfirmEmailToUser(userName, userEmail, curSP, newSP, fromDate);
                }

                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Message = "Duyệt lệnh hủy thành công."
                });
            }
        }

        public void btnDeclineUser_Click(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var userSPService = new ChangeUserServicePackagePreferential();
            var sp_id = Convert.ToInt32(hidID.Value);
            var declineReason = txtDecLy_do_tu_choi.Text.Trim();
            var changed = userSPService.ChangeUserSPStatusByIDCancel(sp_id, 0, declineReason);
            if (changed)
            {
                winDeclineUser.Hide();
                GridDataBind();
                var userInfo = userSPService.GetUserSPByID(sp_id);
                var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                accNumLengString += "1";
                var userName = userInfo.ho_ten_khach_hang;
                var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);
                if (!string.IsNullOrEmpty(userEmail))
                {
                    var curSP = userInfo.goi_dich_vu_hien_tai;
                    var newSP = userInfo.goi_dich_vu_moi;
                    var lydotuchoi = userInfo.ly_do_tu_choi;
                    var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                    // gửi mail khi được duyệt lệnh bị từ chối
                    userSPService.SendDeclineEmailToUser(userName, userEmail, lydotuchoi);
                }

                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Message = "Từ chối hủy đăng ký dịch vụ thành công."
                });
            }
        }

        public void rejectAll(object sender, EventArgs e)
        {
            try
            {
                winDeclineAllUser.Show();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(CancelUserSPPreferential),
                    "rejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }

        public void BtnRejectAll(object sender, EventArgs e)
        {
            try
            {
                /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
                if (string.IsNullOrEmpty(userUpdate))
                {
                    Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                    X.Js.Call("logout");
                    return;
                }*/
                var userSPService = new ChangeUserServicePackagePreferential();
                StringBuilder result = new StringBuilder();
                RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;
                var list = sm.SelectedRows;
                var lyDoTuChoiAll = txtDecLy_do_tu_choi_all.Text;
                if (list.Count <= 0)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.CANCEL,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
                    });
                    return;
                }

                List<string> listID = new List<string>();
                var changed = false;
                foreach (SelectedRow row in list)
                {
                    changed = userSPService.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), 0, lyDoTuChoiAll);
                    if (changed == false)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.CANCEL,
                            Icon = MessageBox.Icon.ERROR,
                            Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
                        });
                    }
                }

                GridDataBind();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(CancelUserSPPreferential),
                    "BtnRejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }
    }
}