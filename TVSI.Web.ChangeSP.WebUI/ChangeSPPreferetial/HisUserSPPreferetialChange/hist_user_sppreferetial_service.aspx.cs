﻿using System;
using System.Web.UI;
using Ext.Net;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSPPreferetial.HisUserSPPreferetialChange
{
    public partial class hist_user_sppreferetial_service : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dtxSearchFrom.SelectedDate = DateTime.Now.AddMonths(-1);
                dtxSearchTo.SelectedDate  = DateTime.Now;
                
                var userSPService = new ChangeUserServicePackagePreferential();
                var servicePackList = userSPService.GetSP();

                foreach (var item in servicePackList)
                {
                    cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboEditGDV_moi.Items.Add(new ListItem(item, item));
                }

                var statusDict = ConstParam.StatusDict;

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                foreach (var item in statusDict)
                {
                    cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                }
                cboSearchStatus.SelectedIndex = 0;
                var namepackpre = userSPService.getNamePackPre();
                foreach (var namepre in namepackpre)
                {
                    txtSearchPackagecurent.Items.Add(new ListItem(namepre.ten_goi_dich_vu_uu_dai,namepre.ten_goi_dich_vu_uu_dai));
                    txtSearchPackageNew.Items.Add(new ListItem(namepre.ten_goi_dich_vu_uu_dai,namepre.ten_goi_dich_vu_uu_dai));
                }                   
                txtSearchPackagecurent.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                txtSearchPackageNew.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));

                GridDataBind();
            }
        }

        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var status = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime)dtxSearchTo.Value;
            var packnew = txtSearchPackageNew.Text.Trim();
            var packcurrent = txtSearchPackagecurent.Text.Trim();
            var userSPService = new ChangeUserServicePackagePreferential();
            
            userSPService.ExportListUserPreHis(accountNo, userName, "", status, fromDate, toDate,packcurrent,packnew);
            
        }

        private void GridDataBind()
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var status = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime)dtxSearchTo.Value;      
            var packnew = txtSearchPackageNew.Text.Trim();
            var packcurrent = txtSearchPackagecurent.Text.Trim();
            var userSPService = new ChangeUserServicePackagePreferential();
            var userInfo = userSPService.SearchUserServicePackListHist(accountNo, userName, null, status, fromDate, toDate,packcurrent,packnew);

            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePackagePreferential();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            txtEditNote.Text = userInfo.note;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;

            winUserSPDetail.Show();
        }

        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserSPDetail.Hide();
        }
    }
}