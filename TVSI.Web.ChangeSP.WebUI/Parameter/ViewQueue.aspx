﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewQueue.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.Parameter.ViewQueue" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css"> 
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }       
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdRequestList(command, data) {
                if (command == "View") {                
                    REQUESTMETHOD.ViewRequestDetail(data.margin_package_request_queue_id);                    
                }
            }
            
            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';

             var renderStatus = function (value) {
                if (value == "10")
                    return String.format(template, "#1ae5e5", "Tạo mới");
                if (value == "20")
                   return String.format(template, "#ffab43", "Chờ xử lý");
                if (value == "30")
                  return String.format(template, "#134bf5", "Đang xử lý");
                if (value == "40")
                   return String.format(template, "#1cff00", "Thành công");
                if (value == "50")
                   return String.format(template, "#de081a", "Thất bại");  
                
                return value;
            };
             
             var columnAutoResize = function (grid) {
                 var view = grid.getView(),
                     store = grid.getStore(),
                     colModel = grid.getColumnModel(),
                     columns = colModel.config,
                     maxAutoWidth = 250, //0 to disable
                     cell,
                     value,
                     width = 0;
             
                 Ext.each(columns, function (column, colIdx) {
                     // Data Width
                     var colWidth = width;
                     store.each(function (record, rowIdx) {
                         cell = view.getCell(rowIdx, colIdx);
                         value = record.get(column.dataIndex);
                         colWidth = Math.max(colWidth, Ext.util.TextMetrics.measure(cell, value).width);
                     });
             
                     if (!column.isRowNumberer) {
                         // Header Width
                         header = view.getHeaderCell(colIdx);
                         headerWidth = Ext.util.TextMetrics.measure(header, column.header).width;
                     }
                     else {
                         // Calc width using total rows
                         lengthWidth = store.getTotalCount().toString().length;
                         headerWidth = (lengthWidth * 10) + (lengthWidth > 1 ? 0 : 10);
                     }
             
                     // Choose the biggest width
                     if (colWidth < headerWidth || colWidth == 0) {
                         colWidth = headerWidth;
                     }
             
                     // Max Length
                     if (colWidth > maxAutoWidth && maxAutoWidth > 0) {
                         colWidth = maxAutoWidth;
                     }
             
                     //Add space to avoid ...
                     if (!column.isRowNumberer) {
                         colWidth += 20
                     }
             
                     colModel.setColumnWidth(colIdx, colWidth);
                 });
             };
        </script>
    </ext:XScript>
</head>
<body>
<ext:ResourceManager runat="server"/>
<form id="form1" runat="server">
<ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
    <Items>
        <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
            <TopBar>
                <ext:Toolbar runat="server">
                    <Items>
                        <ext:Label Text="Số TK " runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:TextField runat="server" ID="txtSearchCustomerAccount" Width="50"/>
                        <ext:ToolbarSeparator/>

                        <ext:ToolbarSpacer/>
                        <ext:Label Text="Từ ngày hiệu lực" runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:DateField ID="dtxSearchFrom" Format="dd/MM/yyyy" runat="server" Width="80">
                            <Listeners>
                                <Select Handler="#{storeListRequest}.reload()"></Select>
                            </Listeners>
                        </ext:DateField>
                        <ext:ToolbarSpacer/>
                        <ext:Label Text="Đến ngày hiệu lực" runat="server"/>
                        <ext:ToolbarSpacer/>
                        <ext:DateField ID="dtxSearchTo" Format="dd/MM/yyyy" runat="server" Width="80">
                            <Listeners>
                                <Select Handler="#{storeListRequest}.reload()"></Select>
                            </Listeners>
                        </ext:DateField>

                        <ext:ToolbarSeparator/>
                        <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                            <Listeners>
                                <Click Handler="#{storeListRequest}.reload()"></Click>
                            </Listeners>
                        </ext:Button>
                        <ext:Button ID="btnExport" Text="Kết xuất Excel" runat="server" Icon="pagewhiteexcel">
                            <DirectEvents>
                                <Click OnEvent="btnExport_Click" IsUpload="true">
                                    <EventMask ShowMask="false"></EventMask>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Items>
                </ext:Toolbar>
            </TopBar>
        </ext:Panel>
        <ext:GridPanel ID="grdListRequest" runat="server" Margins="0 0 0 5" Region="Center"
                       ColumnLines="True" StripeRows="True" ButtonAlign="Center">
            <Store>
                <ext:Store ID="storeListRequest" runat="server" OnRefreshData="storeListRequest_Refresh">
                    <Reader>
                        <ext:JsonReader IDProperty="margin_package_request_queue_id">
                            <Fields>
                                <ext:RecordField Name="margin_package_request_queue_id"/>
                                <ext:RecordField Name="margin_package_request_id"/>
                                <ext:RecordField Name="preferential_package_request_id"/>
                                <ext:RecordField Name="preferential_package_name"/>
                                <ext:RecordField Name="customer_account"/>
                                <ext:RecordField Name="current_service_package"/>
                                <ext:RecordField Name="new_service_package"/>
                                <ext:RecordField Name="interest_type"/>
                                <ext:RecordField Name="account_interest_rate"/>
                                <ext:RecordField Name="basic_interest_rate"/>
                                <ext:RecordField Name="adjust_initial_rate"/>
                                <ext:RecordField Name="adjust_call_rate"/>
                                <ext:RecordField Name="adjust_force_sell_rate"/>
                                <ext:RecordField Name="customer_level_name"/>
                                <ext:RecordField Name="staff_checker"/>
                                <ext:RecordField Name="remark_sba"/>
                                <ext:RecordField Name="effective_date"/>
                                <ext:RecordField Name="interest_rate_checker"/>
                                <ext:RecordField Name="margin_rate_checker"/>
                                <ext:RecordField Name="margin_package_request_queue_status"/>
                                <ext:RecordField Name="current_ee"/>
                                <ext:RecordField Name="new_ee"/>
                                <ext:RecordField Name="margin_package_api_reference_number"/>
                                <ext:RecordField Name="margin_package_api_error_msg"/>
                                <ext:RecordField Name="interest_rate_api_reference_number"/>
                                <ext:RecordField Name="interest_rate_api_status"/>
                                <ext:RecordField Name="interest_rate_api_error_msg"/>
                                <ext:RecordField Name="margin_rate_api_reference_number"/>
                                <ext:RecordField Name="margin_rate_api_status"/>
                                <ext:RecordField Name="margin_rate_api_error_msg"/>
                                <ext:RecordField Name="created_by"/>
                                <ext:RecordField Name="created_date"/>
                                <ext:RecordField Name="updated_by"/>
                                <ext:RecordField Name="updated_date"/>
                            </Fields>
                        </ext:JsonReader>
                    </Reader>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:RowNumbererColumn Width="20" Header="#"/>
                    <ext:CommandColumn Width="70" DataIndex="command">
                        <Commands>
                            <ext:GridCommand Icon="Zoom" CommandName="View" Text="View"/>
                        </Commands>
                    </ext:CommandColumn>
                    <ext:Column DataIndex="margin_package_request_queue_id" Header="Queue ID" Width="50"/>
                    <ext:Column DataIndex="margin_package_request_id" Header="margin_package_request_id" Width="50"/>
                    <ext:Column DataIndex="preferential_package_request_id" Header="ID Gói ưu đãi" Width="50"/>
                    <ext:Column DataIndex="preferential_package_name" Header="Tên Gói ưu đãi" Width="50"/>
                    <ext:Column DataIndex="customer_account" Header="Số TK" Width="60"/>
                    <ext:Column DataIndex="current_service_package" Header="GDV hiện tại" Width="100"/>
                    <ext:NumberColumn DataIndex="current_ee" Header="EE GDV hiện tại" Width="100" Format="00,000"/>
                    <ext:Column DataIndex="new_service_package" Header="GDV mới" Width="100"/>
                    <ext:NumberColumn DataIndex="new_ee" Header="EE GDV mới" Width="100" Format="00,000"/>
                    <ext:Column DataIndex="interest_type" Header="Interest Type" Width="100"/>
                    <ext:Column DataIndex="account_interest_rate" Header="Lãi suất TK" Width="100"/>
                    <ext:Column DataIndex="basic_interest_rate" Header="Lãi suất cơ bản" Width="100"/>
                    <ext:Column DataIndex="adjust_initial_rate" Header="Adjust Initial Rate" Width="100"/>
                    <ext:Column DataIndex="adjust_call_rate" Header="Adjust Call Rate" Width="100"/>
                    <ext:Column DataIndex="adjust_force_sell_rate" Header="Adjust Force Sell Rate" Width="100"/>
                    <ext:Column DataIndex="customer_level_name" Header="Hạng KH" Width="100"/>
                    <ext:BooleanColumn DataIndex="staff_checker" Header="Nhân viên" Width="100"/>
                    <ext:Column DataIndex="remark_sba" Header="Remark SBA" Width="100"/>
                    <ext:DateColumn DataIndex="effective_date" Header="Ngày hiệu lực" Width="100" Format="dd/MM/yyyy"/>
                    <ext:BooleanColumn DataIndex="interest_rate_checker" Header="interest_rate_checker" Width="100"/>
                    <ext:BooleanColumn DataIndex="margin_rate_checker" Header="margin_rate_checker" Width="100"/>
                    <ext:Column DataIndex="margin_package_request_queue_status" Header="margin_package_request_queue_status" Width="100">
                        <Renderer Fn="renderStatus"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="interest_rate_api_status" Header="interest_rate_api_status" Width="100">
                        <Renderer Fn="renderStatus"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="margin_rate_api_status" Header="margin_rate_api_status" Width="100">
                        <Renderer Fn="renderStatus"></Renderer>
                    </ext:Column>
                    <ext:DateColumn DataIndex="created_date" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="created_by" Header="Người tạo" Width="120"/>
                    <ext:DateColumn DataIndex="updated_date" Header="Ngày cập nhật" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="updated_by" Header="Người cập nhật" Width="120"/>
                </Columns>
            </ColumnModel>
            <Listeners>
                <Command Handler="grdRequestList(command, record.data);">
            </Command>
            <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
            </ViewReady>
            <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
            </Listeners>
            <LoadMask ShowMask="true"></LoadMask>
            <BottomBar>
                <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Page size:"/>
                        <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="20"/>
                                <ext:ListItem Text="40"/>
                                <ext:ListItem Text="60"/>
                                <ext:ListItem Text="80"/>
                            </Items>
                            <SelectedItem Value="40"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"/>
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </Items>
</ext:Viewport>

<ext:Window runat="server" ID="winRequestDetail" Title="Request" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="600" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
    <Items>
        <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmRequestDetail" AutoScroll="True">
            <Items>
                <ext:Hidden runat="server" ID="hidID"/>
                <ext:TextField runat="server" ID="txtMargin_package_request_id" Width="200" FieldLabel="margin_package_request_id" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtPreferential_package_request_id" Width="200" FieldLabel="ID Gói ưu đãi" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtPreferential_package_name" Width="200" FieldLabel="Tên Gói ưu đãi" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCustomer_account" Width="200" FieldLabel="Số tài khoản" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCurrent_service_package" Width="200" FieldLabel="GDV hiện tại" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCurrent_EE" Width="200" FieldLabel="EE GDV hiện tại" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtNew_service_package" Width="200" FieldLabel="GDV mới" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtNew_EE" Width="200" FieldLabel="EE GDV mới" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtInterest_type" Width="200" FieldLabel="Interest Type" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtAccount_interest_rate" Width="200" FieldLabel="Lãi suất TK" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtBasic_interest_rate" Width="200" FieldLabel="Lãi suất cơ bản" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtAdjust_initial_rate" Width="200" FieldLabel="Adjust Initial Rate" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtAdjust_call_rate" Width="200" FieldLabel="Adjust Call Rate" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtAdjust_force_sell_rate" Width="200" FieldLabel="Adjust Force Sell Rate" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCustomer_level_name" Width="200" FieldLabel="Hạng KH" ReadOnly="True"/>
                <ext:Checkbox runat="server" ID="chkStaff_checker" Width="200" FieldLabel="Nhân viên" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtRemark_sba" Width="200" FieldLabel="Remark SBA" ReadOnly="True"/>
                <ext:DateField runat="server" ID="dtxEffective_date" Width="150" FieldLabel="Ngày hiệu lực" Format="dd/MM/yyyy" ReadOnly="True"/>
                <ext:Checkbox runat="server" ID="chkInterest_rate_checker" Width="200" FieldLabel="interest_rate_checker" ReadOnly="True"/>
                <ext:Checkbox runat="server" ID="chkMargin_rate_checker" Width="200" FieldLabel="margin_rate_checker" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_package_api_reference_number" Width="200" FieldLabel="API Đổi gói - Refer No." ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_package_request_queue_status" Width="200" FieldLabel="API Đổi gói - Status" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_package_api_error_msg" Width="200" FieldLabel="API Đổi gói - Msg" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtInterest_rate_api_reference_number" Width="200" FieldLabel="API Lãi suất - Refer No." ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtInterest_rate_api_status" Width="200" FieldLabel="API Lãi suất - Status" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtInterest_rate_api_error_msg" Width="200" FieldLabel="API Lãi suất - Msg" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_rate_api_reference_number" Width="200" FieldLabel="API Tỷ lệ ký quỹ - Refer No." ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_rate_api_status" Width="200" FieldLabel="API Tỷ lệ ký quỹ - Status" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtMargin_rate_api_error_msg" Width="200" FieldLabel="API Tỷ lệ ký quỹ - Msg" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCreated_by" Width="200" FieldLabel="Người tạo" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtCreated_date" Width="200" FieldLabel="Ngày tạo" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtUpdated_by" Width="200" FieldLabel="Người cập nhật" ReadOnly="True"/>
                <ext:TextField runat="server" ID="txtUpdated_date" Width="200" FieldLabel="Ngày cập nhật" ReadOnly="True"/>
            </Items>
            <Buttons>
                <ext:Button runat="server" Text="Đóng" Icon="Accept">
                    <Listeners>
                        <Click Handler="#{winRequestDetail}.hide()"></Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </Items>
</ext:Window>

<ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchCustomerAccount}">
    <ext:KeyBinding StopEvent="true">
        <Keys>
            <ext:Key Code="ENTER"/>
        </Keys>
        <Listeners>
            <Event Handler="#{storeListRequest}.reload()"></Event>
        </Listeners>
    </ext:KeyBinding>
</ext:KeyMap>
</form>
</body>
</html>