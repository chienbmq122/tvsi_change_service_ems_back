﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TVSI.Web.ChangeSP.Lib.Service;

namespace TVSI.Web.ChangeSP.WebUI.Parameter
{
    public partial class ViewMarginRateParameter : System.Web.UI.Page
    {
        private readonly CommonService myCommonService;
        
        protected ViewMarginRateParameter()
        {
            myCommonService = new CommonService();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            GridDataBind();

        }
        protected void storeListParameter_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }
        
        private void GridDataBind()
        {
            grdListParameter.GetStore().DataSource = myCommonService.SelectMarginRateParameter();
            grdListParameter.GetStore().DataBind();
        }
    }
}