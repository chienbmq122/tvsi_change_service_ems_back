﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportStaff.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.Parameter.ImportStaff" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner { font-weight: bold !important; }   
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
            
            function grdRequestListCommand(command, data) {
                if (command === "Update") {
                    UpdateRequest(data);
                }
            }
            
            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';
            var renderRequestStatus = function (value) {
                if (value == "-1")
                    return "";

                if (value == "10")
                    return String.format(template, "#96cfb9", "Hoạt động");
                if (value == "99")
                    return String.format(template, "#a11d10", "Ngừng hoạt động");
                
                return value;
            };
            
            UpdateUploadInfo = function (el) {
                var ret = true;
        
                if (Ext.isIE) {
                    return;
                }
                
                var file = el.files[0];
                var size = file.size;
        
                if (size > 4194304) {
                    ret = false;
                }
                
                return ret;
            }
            
            var menuItemClick = function (item) {
                if (item.text == 'Download'){
                    DownloadExcelTemplate();
                }
            };
            
            var columnAutoResize = function (grid) {
                var view = grid.getView(),
                    store = grid.getStore(),
                    colModel = grid.getColumnModel(),
                    columns = colModel.config,
                    maxAutoWidth = 250, //0 to disable
                    cell,
                    value,
                    width = 0;
            
                Ext.each(columns, function (column, colIdx) {
                    // Data Width
                    var colWidth = width;
                    store.each(function (record, rowIdx) {
                        cell = view.getCell(rowIdx, colIdx);
                        value = record.get(column.dataIndex);
                        colWidth = Math.max(colWidth, Ext.util.TextMetrics.measure(cell, value).width);
                    });
            
                    if (!column.isRowNumberer) {
                        // Header Width
                        header = view.getHeaderCell(colIdx);
                        headerWidth = Ext.util.TextMetrics.measure(header, column.header).width;
                    }
                    else {
                        // Calc width using total rows
                        lengthWidth = store.getTotalCount().toString().length;
                        headerWidth = (lengthWidth * 10) + (lengthWidth > 1 ? 0 : 10);
                    }
            
                    // Choose the biggest width
                    if (colWidth < headerWidth || colWidth == 0) {
                        colWidth = headerWidth;
                    }
            
                    // Max Length
                    if (colWidth > maxAutoWidth && maxAutoWidth > 0) {
                        colWidth = maxAutoWidth;
                    }
            
                    //Add space to avoid ...
                    if (!column.isRowNumberer) {
                        colWidth += 20
                    }
            
                    colModel.setColumnWidth(colIdx, colWidth);
                });
            };
        </script>
    </ext:XScript>
    <script type="text/javascript">
        function IsNullOrWhiteSpace(str){
            return str === null || str.match(/^\s*$/) !== null;
        }
        
        var UpdateRequest = function (selectedRequest) {
            REQUESTLIST.UpdateRequest(selectedRequest);
        }
        
        var DownloadExcelTemplate = function () {
            REQUESTLIST.DownloadExcelTemplate();
        }
    </script>
</head>
<body>
<form id="form1" runat="server">
<ext:ResourceManager runat="server"/>
<ext:Viewport runat="server" layout="BorderLayout">
    <Items>
        <ext:FormPanel Margins="0 0 0 5" runat="server" Region="North" ButtonAlign="Center" ID="frmSearchRequest" Height="150">
            <Items>
                <ext:Hidden runat="server" ID="hidRequestID"/>
                <ext:Container runat="server" Layout="Column" Height="100">
                    <Items>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:TextField ID="txtCustomerCode" runat="server" FieldLabel="Số tài khoản" Width="200" LabelStyle="font-weight:bold;"
                                               MsgTarget="Side" AnchorHorizontal="50%"/>
                                <ext:TextField ID="txtDepartmentName" runat="server" FieldLabel="Phòng ban" Width="200" LabelStyle="font-weight:bold;"
                                               MsgTarget="Side" AnchorHorizontal="50%"/>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:TextField ID="txtCustomerName" runat="server" FieldLabel="Tên KH" Width="200" LabelStyle="font-weight:bold;"
                                               MsgTarget="Side" AnchorHorizontal="50%"/>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:ComboBox runat="server" ID="cboRequestStatus" FieldLabel="Trạng thái" Editable="false" SelectedIndex="0" Width="120" LabelStyle="font-weight:bold;">
                                    <Items>
                                        <ext:ListItem Text="--- Tất cả ---" Value="-1"/>
                                        <ext:ListItem Text="Hoạt động" Value="10"/>
                                        <ext:ListItem Text="Ngừng hoạt động" Value="99"/>
                                    </Items>
                                    <SelectedItem Value="-1"></SelectedItem>
                                    <Listeners>
                                        <Select Handler="#{storeRequestList}.reload()"></Select>
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:Container>
            </Items>
            <Buttons>
                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="Find" CausesValidation="true">
                    <DirectEvents>
                        <Click OnEvent="btnSearch_Click"></Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="#{storeRequestList}.reload()"></Click>
                    </Listeners>
                </ext:Button>
                <ext:Button runat="server" Text="Reset" ID="btnReset" Icon="Stop">
                    <Listeners>
                        <Click Handler="#{frmSearchRequest}.reset()"></Click>
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="btnReset_Click"></Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnInsertRequest" runat="server" Text="Thêm mới" Icon="Add">
                    <DirectEvents>
                        <Click OnEvent="btnInsertRequest_Click"></Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnShowWinImport" runat="server" Text="Import" Icon="PageAdd">
                    <Listeners>
                        <Click Handler="#{winImport}.show(); #{winImport}.maximize();"></Click>
                    </Listeners>
                    <DirectEvents>
                        <Click OnEvent="btnShowWinImport_Click"></Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" Text="Kết xuất Excel" ID="frmSearchRequest_btnExportExcel" AutoPostBack="true" Icon="PageExcel" OnClick="frmSearchRequest_btnExportExcel_Click">
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
        <ext:GridPanel ID="grdRequestList" runat="server" Margins="0 0 0 5" Region="Center"
                       ColumnLines="True" StripeRows="True" ButtonAlign="Center">
            <Store>
                <ext:Store ID="storeRequestList" runat="server" OnRefreshData="storeRequestList_Refresh">
                    <Reader>
                        <ext:JsonReader IDProperty="staff_id">
                            <Fields>
                                <ext:RecordField Name="staff_id"/>
                                <ext:RecordField Name="customer_code"/>
                                <ext:RecordField Name="customer_name"/>
                                <ext:RecordField Name="department_name"/>
                                <ext:RecordField Name="identity_number"/>
                                <ext:RecordField Name="effective_date"/>
                                <ext:RecordField Name="end_date"/>
                                <ext:RecordField Name="staff_status"/>
                                <ext:RecordField Name="created_date"/>
                                <ext:RecordField Name="created_by"/>
                                <ext:RecordField Name="updated_by"/>
                                <ext:RecordField Name="updated_date"/>
                            </Fields>
                        </ext:JsonReader>
                    </Reader>
                </ext:Store>
            </Store>
            <ColumnModel runat="server">
                <Columns>
                    <ext:CommandColumn Width="40">
                        <Commands>
                            <ext:GridCommand Icon="ApplicationEdit" CommandName="Update">
                                <ToolTip Text="Update"></ToolTip>
                            </ext:GridCommand>
                        </Commands>
                    </ext:CommandColumn>
                    <ext:Column Header="STT" Fixed="true" Sortable="false" ColumnID="stt" MenuDisabled="true" Width="40">
                        <Renderer Fn="function (v, p, record, rowIndex) {return rowIndex + 1}"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="staff_id" Header="Mã định danh" Width="100"/>
                    <ext:Column DataIndex="customer_code" Header="Số tài khoản" Width="100"/>
                    <ext:Column DataIndex="customer_name" Header="Tên khách hàng" Width="100"/>
                    <ext:Column DataIndex="department_name" Header="Phòng ban" Width="100"/>
                    <ext:Column DataIndex="identity_number" Header="CMTND/CCCD" Width="100"/>
                    <ext:DateColumn DataIndex="effective_date" Header="Ngày hiệu lực" Width="100" Format="dd/MM/yyyy"/>
                    <ext:DateColumn DataIndex="end_date" Header="Ngày hết hiệu lực" Width="100" Format="dd/MM/yyyy"/>
                    <ext:Column DataIndex="staff_status" Header="Trạng thái" Width="100">
                        <Renderer Fn="renderRequestStatus"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="created_by" Header="Người tạo" Width="100"/>
                    <ext:DateColumn DataIndex="created_date" Header="Ngày tạo" Width="100" Format="dd/MM/yyyy HH:mm:ss"/>
                    <ext:Column DataIndex="updated_by" Header="Người cập nhật" Width="100"/>
                    <ext:DateColumn DataIndex="updated_date" Header="Ngày cập nhật" Width="100" Format="dd/MM/yyyy HH:mm:ss"/>
                </Columns>
            </ColumnModel>
            <LoadMask ShowMask="true"></LoadMask>
            <BottomBar>
                <ext:PagingToolbar runat="server" PageSize="20" ID="pageTBar">
                    <Items>
                        <ext:Label runat="server" Text="Page size:"/>
                        <ext:ComboBox runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="10"/>
                                <ext:ListItem Text="20"/>
                                <ext:ListItem Text="30"/>
                                <ext:ListItem Text="40"/>
                            </Items>
                            <SelectedItem Value="20"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"></Select>
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
            <View>
                <ext:LockingGridView runat="server"/>
            </View>
            <Buttons>
            </Buttons>
            <Listeners>
                <Command Handler="grdRequestListCommand(command, record.data);">
            </Command>
            <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
            </ViewReady>
            <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
            </Listeners>
        </ext:GridPanel>
    </Items>
</ext:Viewport>
<ext:Window ID="winUpdateRequest" runat="server" Title="Update" Hidden="True"
            Icon="Application" Layout="FitLayout" ButtonAlign="Center" Closable="True" Height="480" Width="360"
            Padding="0" Modal="True">
    <Items>
        <ext:FormPanel ID="frmUpdateRequest" runat="server" Header="False" ButtonAlign="Center" AnchorVertical="100%"
                       AutoScroll="True" Padding="5" AnchorHorizontal="100%">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtCustomerCode" runat="server" LabelStyle="font-weight:bold;" FieldLabel="Số tài khoản" Width="200" MsgTarget="Side" AllowBlank="False" MinLength="6" MaxLength="6"/>
                <ext:TextField ID="frmUpdateRequest_txtCustomerName" runat="server" FieldLabel="Tên tài khoản" AllowBlank="False" Width="200"/>
                <ext:TextField ID="frmUpdateRequest_txtDepartmentName" runat="server" FieldLabel="Phòng ban" AllowBlank="False" Width="200"/>
                <ext:TextField ID="frmUpdateRequest_txtIdentityNumber" runat="server" FieldLabel="CMTND/CCCD" AllowBlank="False" Width="200"/>
                <ext:DateField ID="frmUpdateRequest_dtxEffectiveDate" runat="server" FieldLabel="Ngày hiệu lực" MaskRe="[0-9\/]" AllowBlank="False" Format="dd/MM/yyyy" MsgTarget="Side">
                    <DirectEvents>
                        <Change onEvent="frmUpdateRequest_dtxEffectiveDate_Change"></Change>
                    </DirectEvents>
                </ext:DateField>
                <ext:DateField ID="frmUpdateRequest_dtxEndDate" runat="server" FieldLabel="Ngày hết hiệu lực" MaskRe="[0-9\/]" AllowBlank="False" Format="dd/MM/yyyy" MsgTarget="Side">
                    <DirectEvents>
                        <Change onEvent="frmUpdateRequest_dtxEndDate_Change"></Change>
                    </DirectEvents>
                </ext:DateField>
                <ext:ComboBox runat="server" ID="frmUpdateRequest_cboRequestStatus" FieldLabel="Trạng thái" Editable="false" SelectedIndex="0" Width="120" LabelStyle="font-weight:bold;">
                    <Items>
                        <ext:ListItem Text="Hoạt động" Value="10"/>
                        <ext:ListItem Text="Ngừng hoạt động" Value="99"/>
                    </Items>
                    <SelectedItem Value="10"></SelectedItem>
                </ext:ComboBox>
                <ext:TextField ID="frmUpdateRequest_txtCreatedBy" runat="server" FieldLabel="Người tạo" Width="200" ReadOnly="True"/>
                <ext:DateField ID="frmUpdateRequest_dtxCreatedDate" runat="server" FieldLabel="Ngày tạo" MaskRe="[0-9\/]" AllowBlank="False" Format="dd/MM/yyyy" MsgTarget="Side" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtUpdatedBy" runat="server" FieldLabel="Người cập nhật" Width="200" ReadOnly="True"/>
                <ext:DateField ID="frmUpdateRequest_dtxUpdatedDate" runat="server" FieldLabel="Ngày cập nhật" MaskRe="[0-9\/]" AllowBlank="False" Format="dd/MM/yyyy" MsgTarget="Side" ReadOnly="True"/>
            </Items>
        </ext:FormPanel>
    </Items>
    <Buttons>
        <ext:Button ID="frmUpdateRequest_btnClose" runat="server" Text="Tắt cửa sổ">
            <Listeners>
                <Click Handler="#{winUpdateRequest}.hide(); #{storeRequestList}.reload();"></Click>
            </Listeners>
        </ext:Button>
        <ext:Button ID="frmUpdateRequest_btnUpdateRequest" runat="server" Text="Cập nhật" Icon="Accept">
            <DirectEvents>
                <Click OnEvent="frmUpdateRequest_btnUpdateRequest_Click">
                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật không?"></Confirmation>
                </Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="if(#{frmUpdateRequest}.getForm().isValid()) { return true; }else{ Ext.Msg.notify('Error', 'Bạn chưa nhập đầy đủ và đúng thông tin'); #{frmUpdateRequest_txtCustomerCode}.focus(); return false; }"></Click>
            </Listeners>
        </ext:Button>
    </Buttons>
</ext:Window>

<ext:Window ID="winImport" runat="server" Title="Import" Hidden="true"
            Icon="Application" Layout="BorderLayout" ButtonAlign="Center" Closable="False"
            Padding="0" Modal="true">
    <Items>
        <ext:FormPanel Margins="0 0 0 5" runat="server" Region="North" ButtonAlign="Center" ID="frmImportRequest" Height="50">
            <Items>
                <ext:Container runat="server" Layout="Column" Height="50">
                    <Items>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".3">
                            <Items>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:FileUploadField ID="fileUpload" runat="server" Width="200" Icon="Attach" FieldLabel="" LabelStyle="font-weight:bold;">
                                    <Listeners>
                                        <FileSelected Handler="if(UpdateUploadInfo(this.fileInput.dom)) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Dung lượng File tối đa: 4MB', buttons:Ext.Msg.OK}); this.reset(); return false;}">
                                        </FileSelected>
                                    </Listeners>
                                </ext:FileUploadField>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:Button ID="btnUpload" runat="server" Text="Import" Icon="PageAdd" FieldLabel=" " LabelSeparator=" ">
                                    <DirectEvents>
                                        <Click IsUpload="True"
                                               OnEvent="btnUpload_Click"
                                               Before="Ext.Msg.wait('Uploading File...', 'Uploading');"
                                               Failure="Ext.Msg.show({
                                                                        title   : 'Error',
                                                                        msg     : 'Error during uploading',
                                                                        minWidth: 200,
                                                                        modal   : true,
                                                                        icon    : Ext.Msg.ERROR,
                                                                        buttons : Ext.Msg.OK
                                                                    });">
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                            <Items>
                                <ext:LinkButton runat="server" ID="btnExcelTemplate" Text="Tải Excel Template" FieldLabel=" " LabelSeparator=" ">
                                    <Menu>
                                        <ext:Menu runat="server">
                                            <Items>
                                                <ext:MenuItem runat="server" Text="Download" Icon="BasketPut"/>
                                            </Items>
                                            <Listeners>
                                                <ItemClick Fn="menuItemClick"></ItemClick>
                                            </Listeners>
                                        </ext:Menu>
                                    </Menu>
                                </ext:LinkButton>
                            </Items>
                        </ext:Container>
                        <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".3">
                            <Items>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:Container>
            </Items>
            <Buttons>
            </Buttons>
        </ext:FormPanel>
        <ext:GridPanel ID="grdImportRequestList" runat="server" Margins="0 0 0 5" Region="Center"
                       ColumnLines="True" StripeRows="True" ButtonAlign="Center">
            <Store>
                <ext:Store ID="storeImportRequestList" runat="server" OnRefreshData="storeImportRequestList_Refresh">
                    <Reader>
                        <ext:JsonReader IDProperty="staff_id">
                            <Fields>
                                <ext:RecordField Name="staff_id"/>
                                <ext:RecordField Name="customer_code"/>
                                <ext:RecordField Name="customer_name"/>
                                <ext:RecordField Name="department_name"/>
                                <ext:RecordField Name="identity_number"/>
                                <ext:RecordField Name="effective_date"/>
                                <ext:RecordField Name="end_date"/>
                                <ext:RecordField Name="staff_status"/>
                                <ext:RecordField Name="created_date"/>
                                <ext:RecordField Name="created_by"/>
                                <ext:RecordField Name="updated_by"/>
                                <ext:RecordField Name="updated_date"/>
                            </Fields>
                        </ext:JsonReader>
                    </Reader>
                </ext:Store>
            </Store>
            <ColumnModel runat="server">
                <Columns>
                    <ext:Column Header="STT" Fixed="true" Sortable="false" ColumnID="stt" MenuDisabled="true" Width="40">
                        <Renderer Fn="function (v, p, record, rowIndex) {return rowIndex + 1}"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="customer_code" Header="Số tài khoản" Width="100"/>
                    <ext:Column DataIndex="customer_name" Header="Tên khách hàng" Width="100"/>
                    <ext:Column DataIndex="department_name" Header="Phòng ban" Width="100"/>
                    <ext:Column DataIndex="identity_number" Header="CMTND/CCCD" Width="100"/>
                    <ext:DateColumn DataIndex="effective_date" Header="Ngày hiệu lực" Width="100" Format="dd/MM/yyyy"/>
                    <ext:DateColumn DataIndex="end_date" Header="Ngày hết hiệu lực" Width="100" Format="dd/MM/yyyy"/>
                    <ext:Column DataIndex="created_by" Header="Người tạo" Width="100"/>
                </Columns>
            </ColumnModel>
            <LoadMask ShowMask="true"></LoadMask>
            <BottomBar>
                <ext:PagingToolbar runat="server" PageSize="20" ID="pageImportTBar">
                    <Items>
                        <ext:Label runat="server" Text="Page size:"/>
                        <ext:ComboBox runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="10"/>
                                <ext:ListItem Text="20"/>
                                <ext:ListItem Text="30"/>
                                <ext:ListItem Text="40"/>
                            </Items>
                            <SelectedItem Value="20"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{pageImportTBar}.pageSize = parseInt(this.getValue()); #{pageImportTBar}.doLoad();"></Select>
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
            <View>
                <ext:LockingGridView runat="server"/>
            </View>
            <Buttons>
                <ext:Button runat="server" Text="Thêm mới" Icon="ApplicationGo">
                    <DirectEvents>
                        <Click onEvent="btnImport_Click">
                            <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý import không?"></Confirmation>
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnWinImportClose" runat="server" Text="Đóng">
                    <Listeners>
                        <Click Handler="#{fileUpload}.reset(); #{winImport}.hide();"></Click>
                    </Listeners>
                    <DirectEvents>
                        <Click onEvent="btnWinImportClose_Click">
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
            <Listeners>
                <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
                </ViewReady>
                <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
            </Listeners>
        </ext:GridPanel>
    </Items>
</ext:Window>
</form>
</body>
</html>