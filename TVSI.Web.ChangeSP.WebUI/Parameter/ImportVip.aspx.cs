﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.ChangeSP.Lib.Constant;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;

namespace TVSI.Web.ChangeSP.WebUI.Parameter
{
    public partial class ImportVip : System.Web.UI.Page
    {
        private readonly CommonService _commonService;
        private DataTable dtImportRequestList = new DataTable();

        protected ImportVip()
        {
            _commonService = new CommonService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SessionHelper.RecreateSession();
            if (!IsPostBack)
            {
                dtImportRequestList = createRequestList();
                Session["IMPORT_REQUEST_LIST"] = dtImportRequestList;
                
                cboCustomerLevel.Items.Add(new ListItem("--Tất cả--", "-1"));
                var lstCustomerLevel = _commonService.TVSI_sSPM_CUSTOMER_LEVEL_SELECT_ALL();
                foreach (var item in lstCustomerLevel)
                {
                    cboCustomerLevel.Items.Add(new ListItem(item.customer_level_name, item.customer_level_code.ToString()));
                    frmUpdateRequest_cboCustomerLevel.Items.Add(new ListItem(item.customer_level_name, item.customer_level_code.ToString()));
                }
                cboCustomerLevel.SelectedIndex = 0;

                grdRequestList_DataBind();
            }
        }

        protected void storeRequestList_Refresh(object sender, StoreRefreshDataEventArgs e)
        {
            grdRequestList_DataBind();
        }

        private void grdRequestList_DataBind()
        {
            string customerCode = txtCustomerCode.Text.Trim();
            string customerName = txtCustomerName.Text.Trim();
            string branchName = txtBranchName.Text.Trim();
            int customerLevelCode = Convert.ToInt32(cboCustomerLevel.SelectedItem.Value);
            int requestStatus = Convert.ToInt32(cboRequestStatus.SelectedItem.Value);

            var dt = _commonService.TVSI_sSPM_KHACH_HANG_VIP_SEARCH(customerCode, customerName, branchName, customerLevelCode, requestStatus);
            if (dt != null)
            {
                grdRequestList.GetStore().DataSource = dt;
                grdRequestList.GetStore().DataBind();
            }
            else
            {
                grdRequestList.GetStore().DataSource = new object[] { };
                grdRequestList.GetStore().DataBind();
            }
        }

        protected void frmUpdateRequest_dtxEffectiveDate_Change(object sender, DirectEventArgs e)
        {
            frmUpdateRequest_dtxEndDate.MinDate = frmUpdateRequest_dtxEffectiveDate.SelectedDate;
        }

        protected void frmUpdateRequest_dtxEndDate_Change(object sender, DirectEventArgs e)
        {
            frmUpdateRequest_dtxEffectiveDate.MaxDate = frmUpdateRequest_dtxEndDate.SelectedDate;
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
        }

        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {
        }

        protected void btnInsertRequest_Click(object sender, DirectEventArgs e)
        {
            var user_id = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(user_id))
            {
                X.Js.Call("logout");
                return;
            }

            hidRequestID.Value = -1;
            frmUpdateRequest.Reset();

            frmUpdateRequest_dtxEffectiveDate.SelectedDate = DateTime.Now;
            frmUpdateRequest_dtxEndDate.SelectedDate = DateTime.MaxValue;
            frmUpdateRequest_dtxEndDate.MinDate = frmUpdateRequest_dtxEffectiveDate.SelectedDate;
            frmUpdateRequest_dtxEffectiveDate.MaxDate = frmUpdateRequest_dtxEndDate.SelectedDate;
            frmUpdateRequest_txtCreatedBy.Text = user_id;
            frmUpdateRequest_dtxCreatedDate.SelectedDate = DateTime.Now;
            frmUpdateRequest_txtUpdatedBy.Text = user_id;
            frmUpdateRequest_dtxUpdatedDate.SelectedDate = DateTime.Now;
            frmUpdateRequest_cboCustomerLevel.SelectedIndex = 0;

            frmUpdateRequest_cboRequestStatus.Disabled = true;
            winUpdateRequest.Show();
        }

        protected void frmUpdateRequest_btnUpdateRequest_Click(object sender, DirectEventArgs e)
        {
            var user_id = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(user_id))
            {
                X.Js.Call("logout");
                return;
            }

            var myRequest = new VipModel()
            {
                tai_khoan_vip_id = int.Parse(hidRequestID.Value.ToString()),
                ma_khach_hang = frmUpdateRequest_txtCustomerCode.Text.Trim(),
                loai_vip = int.Parse(frmUpdateRequest_cboCustomerLevel.SelectedItem.Value),
                mo_ta = frmUpdateRequest_cboCustomerLevel.SelectedItem.Text,
                customer_name = frmUpdateRequest_txtCustomerName.Text.Trim(),
                branch_name = frmUpdateRequest_txtBranchName.Text.Trim(),
                effective_date = frmUpdateRequest_dtxEffectiveDate.SelectedDate,
                end_date = frmUpdateRequest_dtxEndDate.SelectedDate,
                trang_thai = int.Parse(frmUpdateRequest_cboRequestStatus.SelectedItem.Value),
                nguoi_tao = user_id,
                updated_by = user_id
            };

            if (myRequest.tai_khoan_vip_id > 0)
            {
                var updateResult = _commonService.TVSI_sSPM_KHACH_HANG_VIP_UPDATE(myRequest);

                if (updateResult > 0)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Cập nhật thành công!"
                    });

                    winUpdateRequest.Hide();
                    grdRequestList_DataBind();
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Cập nhật thất bại!"
                    });
                }
            }
            else
            {
                var updateResult = _commonService.TVSI_sSPM_KHACH_HANG_VIP_INSERT(myRequest);

                if (updateResult > 0)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Thêm mới thành công!"
                    });

                    winUpdateRequest.Hide();
                    grdRequestList_DataBind();
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Thêm mới thất bại!"
                    });
                }
            }
        }

        protected void frmSearchRequest_btnExportExcel_Click(object sender, EventArgs e)
        {
            var user_id = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(user_id))
            {
                X.Js.Call("logout");
                return;
            }

            try
            {
                string customerCode = txtCustomerCode.Text.Trim();
                string customerName = txtCustomerName.Text.Trim();
                string branchName = txtBranchName.Text.Trim();
                int customerLevelCode = Convert.ToInt32(cboCustomerLevel.SelectedItem.Value);
                int requestStatus = Convert.ToInt32(cboRequestStatus.SelectedItem.Value);

                var lstRequest =
                    _commonService.TVSI_sSPM_KHACH_HANG_VIP_SEARCH(customerCode, customerName, branchName, customerLevelCode, requestStatus);

                if (lstRequest == null || lstRequest.Count == 0)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Danh sách rỗng"
                    });

                    return;
                }

                var templateFileName = "SPM_Vip_List_yyyyMMdd_HHmmss.xlsx";
                var templateFilepath =
                    Server.MapPath(ConfigurationManager.AppSettings["SPM_TEMPLATE_FOLDER"] + templateFileName);
                var desFileName = templateFileName.Replace("yyyyMMdd_HHmmss",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                var desFilePath =
                    Server.MapPath(ConfigurationManager.AppSettings["SPM_DOWNLOAD_FOLDER"] + desFileName);

                // Create a copy of the template file and open the copy
                File.Copy(templateFilepath, desFilePath, true);

                // Create Excel File
                var newFile = new FileInfo(desFilePath);

                using (ExcelPackage package = new ExcelPackage(newFile, new FileInfo(templateFilepath)))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                    var currentRow = 3;

                    for (int i = 0; i < lstRequest.Count; i++)
                    {
                        for (int j = 1; j <= 11; j++)
                        {
                            worksheet.Cells[currentRow, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }

                        worksheet.Cells[currentRow, 1].Value = lstRequest[i].ma_khach_hang;
                        worksheet.Cells[currentRow, 2].Value = lstRequest[i].customer_name;
                        worksheet.Cells[currentRow, 3].Value = lstRequest[i].branch_name;
                        worksheet.Cells[currentRow, 4].Value = lstRequest[i].mo_ta;
                        worksheet.Cells[currentRow, 5].Value =
                            lstRequest[i].effective_date?.ToString("dd/MM/yyyy");
                        worksheet.Cells[currentRow, 6].Value =
                            lstRequest[i].end_date?.ToString("dd/MM/yyyy");
                        worksheet.Cells[currentRow, 7].Value =
                            CommonUtils.ConvertVipStatusCodeToName(lstRequest[i].trang_thai);
                        worksheet.Cells[currentRow, 8].Value = lstRequest[i].nguoi_tao;
                        worksheet.Cells[currentRow, 9].Value =
                            lstRequest[i].ngay_tao?.ToString("dd/MM/yyyy - HH:mm:ss");
                        worksheet.Cells[currentRow, 10].Value = lstRequest[i].updated_by;
                        worksheet.Cells[currentRow, 11].Value =
                            lstRequest[i].updated_date?.ToString("dd/MM/yyyy - HH:mm:ss");

                        currentRow++;
                    }

                    package.Save();
                }

                // Download Excel File
                var file = new FileInfo(desFilePath);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(file.FullName);
                    Response.Flush();
                    System.IO.File.Delete(desFilePath);
                    Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = $"Lỗi xảy ra khi kết xuất Excel: {ex}"
                });
            }
        }

        protected void btnShowWinImport_Click(object sender, DirectEventArgs e)
        {
            frmImportRequest.Reset();
            Session["IMPORT_REQUEST_LIST"] = createRequestList();
            grdImportRequestList_DataBind();
        }

        protected void btnWinImportClose_Click(object sender, DirectEventArgs e)
        {
            frmImportRequest.Reset();
            Session["IMPORT_REQUEST_LIST"] = createRequestList();
            grdImportRequestList_DataBind();
        }

        protected void storeImportRequestList_Refresh(object sender, StoreRefreshDataEventArgs e)
        {
            grdImportRequestList_DataBind();
        }

        private void grdImportRequestList_DataBind()
        {
            dtImportRequestList = (DataTable) Session["IMPORT_REQUEST_LIST"];
            storeImportRequestList.DataSource = dtImportRequestList;
            storeImportRequestList.DataBind();
        }

        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var lstCustomerLevel = _commonService.TVSI_sSPM_CUSTOMER_LEVEL_SELECT_ALL();

            string tpl = "Trích xuất thông tin thành công từ File: {0}<br/>";

            if (fileUpload.HasFile)
            {
                string fileExtension = System.IO.Path.GetExtension(fileUpload.FileName);
                if (fileExtension.Equals(".xlsx"))
                {
                    using (MemoryStream stream = new MemoryStream(fileUpload.FileBytes))
                    using (ExcelPackage package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                        dtImportRequestList = createRequestList();

                        int currentRow = 2;
                        while (worksheet.Cells[currentRow, 1].Value != null &&
                               !String.IsNullOrWhiteSpace(worksheet.Cells[currentRow, 1].Value.ToString()))
                        {
                            DataRow drRequest = dtImportRequestList.NewRow();

                            drRequest["ma_khach_hang"] = worksheet.Cells[currentRow, 1].Value.ToString().Trim();


                            if (worksheet.Cells[currentRow, 2].Value != null &&
                                !String.IsNullOrWhiteSpace(worksheet.Cells[currentRow, 2].Value.ToString()))
                            {
                                drRequest["customer_name"] = worksheet.Cells[currentRow, 2].Value.ToString().Trim();
                            }
                            else
                            {
                                ExtNet.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.ERROR,
                                    Title = "Error",
                                    Message =
                                        $"Dòng {currentRow}: Định dạng tên khách hàng không hợp lệ"
                                });

                                fileUpload.Reset();
                                return;
                            }

                            if (worksheet.Cells[currentRow, 3].Value != null &&
                                !String.IsNullOrWhiteSpace(worksheet.Cells[currentRow, 3].Value.ToString()))
                            {
                                drRequest["branch_name"] = worksheet.Cells[currentRow, 3].Value.ToString().Trim();
                            }
                            else
                            {
                                ExtNet.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.ERROR,
                                    Title = "Error",
                                    Message =
                                        $"Dòng {currentRow}: Định dạng chi nhánh không hợp lệ"
                                });

                                fileUpload.Reset();
                                return;
                            }

                            if (worksheet.Cells[currentRow, 4].Value != null &&
                                !String.IsNullOrWhiteSpace(worksheet.Cells[currentRow, 4].Value.ToString()))
                            {
                                drRequest["mo_ta"] = worksheet.Cells[currentRow, 4].Value.ToString().Trim();
                                if (lstCustomerLevel.Any(i => i.customer_level_name == drRequest["mo_ta"].ToString()))
                                {
                                    var myCustomerLevel = lstCustomerLevel
                                        .FirstOrDefault(i => i.customer_level_name == drRequest["mo_ta"].ToString());
                                    if (myCustomerLevel != null){
                                        drRequest["loai_vip"] = myCustomerLevel.customer_level_code;
                                    }
                                    else
                                    {
                                        ExtNet.Msg.Show(new MessageBoxConfig
                                        {
                                            Buttons = MessageBox.Button.OK,
                                            Icon = MessageBox.Icon.ERROR,
                                            Title = "Error",
                                            Message =
                                                $"Dòng {currentRow}: Lỗi xảy ra khi lấy mã hạng KH tương ứng"
                                        });

                                        fileUpload.Reset();
                                        return;
                                    }
                                }
                                else
                                {
                                    ExtNet.Msg.Show(new MessageBoxConfig
                                    {
                                        Buttons = MessageBox.Button.OK,
                                        Icon = MessageBox.Icon.ERROR,
                                        Title = "Error",
                                        Message =
                                            $"Dòng {currentRow}: Tên hạng KH không nằm trong danh sách sẵn có"
                                    });

                                    fileUpload.Reset();
                                    return;
                                }
                            }
                            else
                            {
                                ExtNet.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.ERROR,
                                    Title = "Error",
                                    Message =
                                        $"Dòng {currentRow}: Định dạng Hạng KH không hợp lệ"
                                });

                                fileUpload.Reset();
                                return;
                            }

                            try
                            {
                                drRequest["effective_date"] = DateTime.ParseExact(
                                    worksheet.Cells[currentRow, 5].Value.ToString(), "dd/MM/yyyy",
                                    CultureInfo.InvariantCulture).ToString("O");
                                ;
                            }
                            catch (Exception ex)
                            {
                                ExtNet.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.ERROR,
                                    Title = "Error",
                                    Message =
                                        $"Dòng {currentRow}: Định dạng Ngày hiệu lực không hợp lệ " +
                                        "(Định dạng quy ước: dd/MM/yyyy)"
                                });

                                fileUpload.Reset();
                                return;
                            }

                            try
                            {
                                drRequest["end_date"] = DateTime.ParseExact(
                                    worksheet.Cells[currentRow, 6].Value.ToString(), "dd/MM/yyyy",
                                    CultureInfo.InvariantCulture).ToString("O");
                                ;
                            }
                            catch (Exception ex)
                            {
                                ExtNet.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.ERROR,
                                    Title = "Error",
                                    Message =
                                        $"Dòng {currentRow}: Ngày hết hiệu lực không hợp lệ " +
                                        "(Định dạng quy ước: dd/MM/yyyy)"
                                });

                                fileUpload.Reset();
                                return;
                            }

                            drRequest["trang_thai"] = VipStatusConst.HOAT_DONG;
                            drRequest["nguoi_tao"] = username;

                            dtImportRequestList.Rows.Add(drRequest);
                            currentRow++;
                        }

                        // Kiểm tra bản ghi bị trùng lặp --- Begin
                        var dtDuplicateChecker = dtImportRequestList.AsEnumerable().GroupBy(x =>
                                new
                                {
                                    customer_account = x["ma_khach_hang"]
                                })
                            .Where(x => x.Count() > 1).ToList();
                        if (dtDuplicateChecker.Count > 0)
                        {
                            var drDuplicateCheckerCustomerCode =
                                dtDuplicateChecker[0].ElementAtOrDefault(0)?["ma_khach_hang"];

                            ExtNet.Msg.Show(new MessageBoxConfig
                            {
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.ERROR,
                                Title = "Error",
                                Message = $"Số tài khoản {drDuplicateCheckerCustomerCode} " +
                                          "bị trùng lặp"
                            });

                            fileUpload.Reset();
                            return;
                        }
                        // Kiểm tra bản ghi bị trùng lặp --- End

                        Session["IMPORT_REQUEST_LIST"] = dtImportRequestList;
                        
                        var dt = dtImportRequestList.AsEnumerable().Take(50).CopyToDataTable();
                        if (dt != null)
                        {
                            grdImportRequestList.GetStore().DataSource = dt;
                            grdImportRequestList.GetStore().DataBind();
                        }
                        else
                        {
                            grdImportRequestList.GetStore().DataSource = new object[] { };
                            grdImportRequestList.GetStore().DataBind();
                        }
                    }

                    fileUpload.Reset();

                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Title = "Success",
                        Message = string.Format(tpl, fileUpload.PostedFile.FileName)
                    });
                }
                else
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Title = "Error",
                        Message = "Định dạng File phải là Excel (.xlsx)"
                    });
                    fileUpload.Reset();
                }
            }
            else
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Title = "Error",
                    Message = "No file uploaded"
                });
            }
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            var userCreate = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(userCreate))
            {
                X.Js.Call("logout");
                return;
            }

            if (Session["IMPORT_REQUEST_LIST"] != null)
            {
                dtImportRequestList = (DataTable) Session["IMPORT_REQUEST_LIST"];
                if (dtImportRequestList.Rows.Count == 0)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Title = "Error",
                        Message = "Danh sách rỗng"
                    });
                    return;
                }

                var lstRequest = new List<VipModel>();
                var dtRequest = (DataTable) Session["IMPORT_REQUEST_LIST"];
                lstRequest = (from DataRow dr in dtRequest.Rows
                    select new VipModel()
                    {
                        ma_khach_hang = dr["ma_khach_hang"].ToString(),
                        loai_vip = int.Parse(dr["loai_vip"].ToString()),
                        mo_ta = dr["mo_ta"].ToString(),
                        customer_name = dr["customer_name"].ToString(),
                        branch_name = dr["branch_name"].ToString(),
                        effective_date = DateTime.Parse(dr["effective_date"].ToString(), CultureInfo.InvariantCulture,
                            DateTimeStyles.RoundtripKind),
                        end_date = DateTime.Parse(dr["end_date"].ToString(), CultureInfo.InvariantCulture,
                            DateTimeStyles.RoundtripKind),
                        trang_thai = int.Parse(dr["trang_thai"].ToString()),
                        nguoi_tao = dr["nguoi_tao"].ToString()
                    }).ToList();

                var importResult = _commonService.ImportVip(lstRequest);
                if (importResult > 0)
                {
                    frmImportRequest.Reset();
                    Session["IMPORT_REQUEST_LIST"] = createRequestList();
                    grdImportRequestList_DataBind();
                    winImport.Hide();

                    grdRequestList_DataBind();
                }
                else
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Title = "Error",
                        Message = "Import thất bại"
                    });
                }
            }
        }

        private DataTable createRequestList()
        {
            var dt = new DataTable();

            dt.Columns.Add("tai_khoan_vip_id");
            dt.Columns.Add("ma_khach_hang");
            dt.Columns.Add("loai_vip");
            dt.Columns.Add("mo_ta");
            dt.Columns.Add("trang_thai");
            dt.Columns.Add("ngay_tao");
            dt.Columns.Add("nguoi_tao");
            dt.Columns.Add("customer_name");
            dt.Columns.Add("branch_name");
            dt.Columns.Add("effective_date");
            dt.Columns.Add("end_date");
            dt.Columns.Add("updated_by");
            dt.Columns.Add("updated_date");

            return dt;
        }

        [DirectMethod(Namespace = "REQUESTLIST")]
        public void UpdateRequest(VipModel selectedRequest)
        {
            var user_id = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(user_id))
            {
                X.Js.Call("logout");
                return;
            }

            hidRequestID.Value = selectedRequest.tai_khoan_vip_id;

            selectedRequest = _commonService.TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_ID(selectedRequest.tai_khoan_vip_id);

            frmUpdateRequest_txtCustomerCode.Text = selectedRequest.ma_khach_hang;
            frmUpdateRequest_txtCustomerName.Value = selectedRequest.customer_name;
            frmUpdateRequest_txtBranchName.Value = selectedRequest.branch_name;
            frmUpdateRequest_cboCustomerLevel.SelectedItem.Value = selectedRequest.loai_vip.ToString();
            frmUpdateRequest_dtxEffectiveDate.SelectedDate = selectedRequest.effective_date ?? DateTime.Now;
            frmUpdateRequest_dtxEndDate.SelectedDate = selectedRequest.end_date ?? DateTime.Now;
            frmUpdateRequest_cboRequestStatus.SelectedItem.Value = selectedRequest.trang_thai.ToString();
            frmUpdateRequest_txtCreatedBy.Value = selectedRequest.nguoi_tao;
            frmUpdateRequest_dtxCreatedDate.SelectedDate = selectedRequest.ngay_tao ?? DateTime.Now;
            frmUpdateRequest_txtUpdatedBy.Value = selectedRequest.updated_by;
            frmUpdateRequest_dtxUpdatedDate.SelectedDate = selectedRequest.updated_date ?? DateTime.Now;

            frmUpdateRequest_dtxEndDate.MinDate = frmUpdateRequest_dtxEffectiveDate.SelectedDate;
            frmUpdateRequest_dtxEffectiveDate.MaxDate = frmUpdateRequest_dtxEndDate.SelectedDate;
            frmUpdateRequest_cboRequestStatus.Disabled = false;
            winUpdateRequest.Show();
        }

        [DirectMethod(Namespace = "REQUESTLIST")]
        public void DownloadExcelTemplate()
        {
            string name = "Import_Vip_List.xlsx";
            string downloadPath =
                Server.MapPath(ConfigurationManager.AppSettings["SPM_TEMPLATE_FOLDER"]);
            string fileFullName = downloadPath + name;

            FileInfo file = new FileInfo(fileFullName);
            if (file.Exists)
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                Response.WriteFile(fileFullName);
                Response.Flush();
                Response.End();
            }
        }
    }
}