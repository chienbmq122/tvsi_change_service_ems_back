﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewInterestRateParameter.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.Parameter.ViewInterestRateParameter" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
        </script>
    </ext:XScript>
</head>
<body>
<ext:ResourceManager runat="server"/>
<form id="form1" runat="server">
    <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
        <Items>
            <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                <TopBar>
                    <ext:Toolbar runat="server">
                        <Items>
                            <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                <Listeners>
                                    <Click Handler="#{storeListParameter}.reload()"></Click>
                                </Listeners>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
            </ext:Panel>
            <ext:GridPanel ID="grdListParameter" runat="server" Margins="0 0 0 5" Region="Center"
                           ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                <Store>
                    <ext:Store ID="storeListParameter" runat="server" OnRefreshData="storeListParameter_Refresh">
                        <Reader>
                            <ext:JsonReader>
                                <Fields>
                                    <ext:RecordField Name="package_name"/>
                                    <ext:RecordField Name="normal_rate"/>
                                    <ext:RecordField Name="silver_rate"/>
                                    <ext:RecordField Name="gold_rate"/>
                                    <ext:RecordField Name="platinum_rate"/>
                                    <ext:RecordField Name="diamond_rate"/>
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                    </ext:Store>
                </Store>

                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn Width="30" Header="STT"/>
                        <ext:Column DataIndex="package_name" Header="Gói" Width="100"/>
                        <ext:Column DataIndex="normal_rate" Header="Normal" Width="80"/>
                        <ext:Column DataIndex="silver_rate" Header="Silver" Width="80"/>
                        <ext:Column DataIndex="gold_rate" Header="Gold" Width="80"/>
                        <ext:Column DataIndex="platinum_rate" Header="Platinum" Width="80"/>
                        <ext:Column DataIndex="diamond_rate" Header="Diamond" Width="80"/>
                    </Columns>
                </ColumnModel>
                <LoadMask ShowMask="true"></LoadMask>
                <BottomBar>
                    <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                        <Items>
                            <ext:Label ID="Label1" runat="server" Text="Page size:"/>
                            <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                <Items>
                                    <ext:ListItem Text="20"/>
                                    <ext:ListItem Text="40"/>
                                    <ext:ListItem Text="60"/>
                                    <ext:ListItem Text="80"/>
                                </Items>
                                <SelectedItem Value="40"></SelectedItem>
                                <Listeners>
                                    <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"/>
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Viewport>
</form>
</body>
</html>
