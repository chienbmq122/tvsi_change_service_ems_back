﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.WebUI.Parameter
{
    public partial class ViewQueue : System.Web.UI.Page
    {
        private readonly MarginPackageRequestQueueService myMarginPackageRequestQueueService;

        protected ViewQueue()
        {
            myMarginPackageRequestQueueService = new MarginPackageRequestQueueService();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dtxSearchFrom.SelectedDate = DateTime.Now.AddDays(-7);
                dtxSearchTo.SelectedDate  = DateTime.Now.AddDays(7);
                
                GridDataBind();
            }
        }

        protected void storeListRequest_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        /*protected void btnExport_Click(object sender, EventArgs e)
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var status = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime)dtxSearchTo.Value;
            var packnew = txtSearchPackageNew.Text.Trim();
            var packcurrent = txtSearchPackagecurent.Text.Trim();
            var userSPService = new ChangeUserServicePack();
            userSPService.ExportUserHistory(accountNo, userName, "", status, fromDate, toDate,packcurrent,packnew);
        }*/

        private void GridDataBind()
        {
            var customerAccount = txtSearchCustomerAccount.Text.Trim();
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime)dtxSearchTo.Value;
            
            var lstRequest = myMarginPackageRequestQueueService.Search(customerAccount, fromDate, toDate);

            grdListRequest.GetStore().DataSource = lstRequest;
            grdListRequest.GetStore().DataBind();
        }
        
        protected void btnExport_Click(object sender, DirectEventArgs e)
        {
            var username = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }

            try
            {
                var customerAccount = txtSearchCustomerAccount.Text.Trim();
                var fromDate = (DateTime)dtxSearchFrom.Value;
                var toDate = (DateTime)dtxSearchTo.Value;
            
                var lstRequest = myMarginPackageRequestQueueService.Search(customerAccount, fromDate, toDate);

                if (lstRequest == null || lstRequest.Count == 0)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Danh sách rỗng"
                    });

                    return;
                }

                var templateFileName = "SPM_Queue_List_yyyyMMdd_HHmmss.xlsx";
                var templateFilepath =
                    Server.MapPath(ConfigurationManager.AppSettings["SPM_TEMPLATE_FOLDER"] + templateFileName);
                var desFileName = templateFileName.Replace("yyyyMMdd_HHmmss",
                    DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                var desFilePath =
                    Server.MapPath(ConfigurationManager.AppSettings["SPM_DOWNLOAD_FOLDER"] + desFileName);

                // Create a copy of the template file and open the copy
                File.Copy(templateFilepath, desFilePath, true);

                // Create Excel File
                var newFile = new FileInfo(desFilePath);

                using (ExcelPackage package = new ExcelPackage(newFile, new FileInfo(templateFilepath)))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                    var currentRow = 3;

                    for (int i = 0; i < lstRequest.Count; i++)
                    {
                        for (int j = 1; j <= 28; j++)
                        {
                            worksheet.Cells[currentRow, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }

                        worksheet.Cells[currentRow, 1].Value = lstRequest[i].margin_package_request_queue_id;
                        worksheet.Cells[currentRow, 2].Value = lstRequest[i].margin_package_request_id;
                        worksheet.Cells[currentRow, 3].Value = lstRequest[i].preferential_package_request_id;
                        worksheet.Cells[currentRow, 4].Value = lstRequest[i].preferential_package_name;
                        worksheet.Cells[currentRow, 5].Value = lstRequest[i].customer_account;
                        worksheet.Cells[currentRow, 6].Value = lstRequest[i].current_service_package;
                        worksheet.Cells[currentRow, 7].Value = lstRequest[i].current_ee;
                        worksheet.Cells[currentRow, 8].Value = lstRequest[i].new_service_package;
                        worksheet.Cells[currentRow, 9].Value = lstRequest[i].new_ee;
                        worksheet.Cells[currentRow, 10].Value = lstRequest[i].interest_type;
                        worksheet.Cells[currentRow, 11].Value = lstRequest[i].account_interest_rate;
                        worksheet.Cells[currentRow, 12].Value = lstRequest[i].basic_interest_rate;
                        worksheet.Cells[currentRow, 13].Value = lstRequest[i].adjust_initial_rate;
                        worksheet.Cells[currentRow, 14].Value = lstRequest[i].adjust_call_rate;
                        worksheet.Cells[currentRow, 15].Value = lstRequest[i].adjust_force_sell_rate;
                        worksheet.Cells[currentRow, 16].Value = lstRequest[i].customer_level_name;
                        worksheet.Cells[currentRow, 17].Value = lstRequest[i].staff_checker ? "True" : "False";
                        worksheet.Cells[currentRow, 18].Value = lstRequest[i].remark_sba;
                        worksheet.Cells[currentRow, 19].Value =
                            lstRequest[i].effective_date?.ToString("dd/MM/yyyy");
                        worksheet.Cells[currentRow, 20].Value = lstRequest[i].interest_rate_checker ? "True" : "False";
                        worksheet.Cells[currentRow, 21].Value = lstRequest[i].margin_rate_checker ? "True" : "False";
                        worksheet.Cells[currentRow, 22].Value =
                            CommonUtils.ConvertRequestApiStatusCodeToName(lstRequest[i].margin_package_request_queue_status);
                        worksheet.Cells[currentRow, 23].Value =
                            CommonUtils.ConvertRequestApiStatusCodeToName(lstRequest[i].interest_rate_api_status);
                        worksheet.Cells[currentRow, 24].Value =
                            CommonUtils.ConvertRequestApiStatusCodeToName(lstRequest[i].margin_rate_api_status);
                        worksheet.Cells[currentRow, 25].Value =
                            lstRequest[i].created_date?.ToString("dd/MM/yyyy - HH:mm:ss");
                        worksheet.Cells[currentRow, 26].Value = lstRequest[i].created_by;
                        worksheet.Cells[currentRow, 27].Value =
                            lstRequest[i].updated_date?.ToString("dd/MM/yyyy - HH:mm:ss");
                        worksheet.Cells[currentRow, 28].Value = lstRequest[i].updated_by;
                        
                        currentRow++;
                    }

                    package.Save();
                }

                // Download Excel File
                var file = new FileInfo(desFilePath);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(file.FullName);
                    Response.Flush();
                    System.IO.File.Delete(desFilePath);
                    Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = $"Lỗi xảy ra khi kết xuất Excel: {ex}"
                });
            }
        }
        
        [DirectMethod(Namespace = "REQUESTMETHOD")]
        public void ViewRequestDetail(long requestId)
        {
            hidID.Value = requestId;
            var myRequest = myMarginPackageRequestQueueService.TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT_BY_ID(requestId);

            frmRequestDetail.Reset();
            
            txtMargin_package_request_id.Text = myRequest.margin_package_request_id.ToString();
            txtPreferential_package_request_id.Text = myRequest.preferential_package_request_id.ToString();
            txtPreferential_package_name.Text = myRequest.preferential_package_name;
            txtCustomer_account.Text = myRequest.customer_account;
            txtCurrent_service_package.Text = myRequest.current_service_package;
            txtCurrent_EE.Text = Functions.FormatDouble(myRequest.current_ee, 3);
            txtNew_service_package.Text = myRequest.new_service_package;
            txtNew_EE.Text = Functions.FormatDouble(myRequest.new_ee, 3);
            txtInterest_type.Text = myRequest.interest_type;
            txtAccount_interest_rate.Text = myRequest.account_interest_rate.ToString("N3");
            txtBasic_interest_rate.Text = myRequest.basic_interest_rate.ToString("N3");
            txtAdjust_initial_rate.Text = myRequest.adjust_initial_rate.ToString("N3");
            txtAdjust_call_rate.Text = myRequest.adjust_call_rate.ToString("N3");
            txtAdjust_force_sell_rate.Text = myRequest.adjust_force_sell_rate.ToString("N3");
            txtCustomer_level_name.Text = myRequest.customer_level_name;
            chkStaff_checker.Checked = myRequest.staff_checker;
            txtRemark_sba.Text = myRequest.remark_sba;
            dtxEffective_date.SelectedDate = myRequest.effective_date ?? DateTime.MinValue;
            chkInterest_rate_checker.Checked = myRequest.interest_rate_checker;
            chkMargin_rate_checker.Checked = myRequest.margin_rate_checker;
            txtMargin_package_api_reference_number.Text = myRequest.margin_package_api_reference_number;
            txtMargin_package_request_queue_status.Text = CommonUtils.ConvertRequestApiStatusCodeToName(myRequest.margin_package_request_queue_status);
            txtMargin_package_api_error_msg.Text = myRequest.margin_package_api_error_msg;
            txtInterest_rate_api_reference_number.Text = myRequest.interest_rate_api_reference_number;
            txtInterest_rate_api_status.Text = CommonUtils.ConvertRequestApiStatusCodeToName(myRequest.interest_rate_api_status);
            txtInterest_rate_api_error_msg.Text = myRequest.interest_rate_api_error_msg;
            txtMargin_rate_api_reference_number.Text = myRequest.margin_rate_api_reference_number;
            txtMargin_rate_api_status.Text = CommonUtils.ConvertRequestApiStatusCodeToName(myRequest.margin_rate_api_status);
            txtMargin_rate_api_error_msg.Text = myRequest.margin_rate_api_error_msg;
            txtCreated_by.Text = myRequest.created_by;
            txtCreated_date.Text = myRequest.created_date?.ToString("dd/MM/yyyy HH:mm:ss") ?? string.Empty;
            txtUpdated_by.Text = myRequest.updated_by;
            txtUpdated_date.Text = myRequest.updated_date?.ToString("dd/MM/yyyy HH:mm:ss") ?? string.Empty;
            
            winRequestDetail.Show();
        }
    }
}