﻿using Ext.Net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Constant;
using TVSI.Web.ChangeSP.Lib.Model;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Service.WebService;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.ApproveUserSPChange
{
    public partial class approve_user_service : System.Web.UI.Page
    {
        private readonly ChangeUserServicePack _userService;
        private readonly GetEEWebService myEEWebService;
        private readonly MarginPackageRequestQueueService myMarginPackageRequestQueueService;
        private readonly CommonService myCommonService;
        private readonly ManageServicePack myMarginSPM;

        protected approve_user_service()
        {
            myEEWebService = new GetEEWebService();
            myMarginPackageRequestQueueService = new MarginPackageRequestQueueService();
            myCommonService = new CommonService();
            myMarginSPM = new ManageServicePack();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dtxSearchFrom.SelectedDate = dtxSearchTo.SelectedDate = DateTime.MinValue;

                var userSPService = new ChangeUserServicePack();
                Logger.Info(typeof(approve_user_service), "b1 -- Model");
                var servicePackList = userSPService.GetSP();

                foreach (var item in servicePackList)
                {
                    cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboEditGDV_moi.Items.Add(new ListItem(item, item));
                    
                    winApproveUser_cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    winApproveUser_cboEditGDV_moi.Items.Add(new ListItem(item, item));
                }

                var statusDict = ConstParam.StatusDict;

                if (statusDict.Count > 0)
                {
                    foreach (var item in statusDict)
                    {
                        cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                        cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                        
                        winApproveUser_cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    }

                    cboSearchStatus.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                    cboEditTrang_thai.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                    
                    winApproveUser_cboEditTrang_thai.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                }

                cboSearchStatus.SelectedIndex = 0;
                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                GridDataBind();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var fromDate = (DateTime) dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            var curStatus = cboSearchStatus.SelectedItem.Value;

            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "0";
            }

            var userSPService = new ChangeUserServicePack();
            userSPService.ExportUserServicePackListApprove(accountNo, userName, "", curStatus, fromDate, toDate, "",
                "");
        }

        protected void btnApproveAll(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var changeUserServicePack = new ChangeUserServicePack();
            StringBuilder result = new StringBuilder();
            RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;
            var list = sm.SelectedRows;
            var messageBoxNotSelect = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
            };
            if (list.Count == 0)
            {
                ExtNet.Msg.Show(messageBoxNotSelect);
            }

            List<string> listID = new List<string>();
            var changed = false;
            var messageBoxInsertError = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
            };
            foreach (SelectedRow row in list)
            {
                changed = changeUserServicePack.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), 1, "");
                if (changed == false)
                {
                    ExtNet.Msg.Show(messageBoxInsertError);
                }
            }

            sm.SelectedRows.Clear();
            sm.UpdateSelection();
            GridDataBind();
        }


        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var webService = new GetEEWebService();
            winDeclineAllUser.Hide();
            var userName = txtSearchUserName.Text.Trim();
            var curStatus = cboSearchStatus.SelectedItem.Value;
            var fromDate = (DateTime) dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            var userSPService = new ChangeUserServicePack();
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "0";
            }

            var userInfo = userSPService.SearchUserServicePackList(accountNo, userName, "", curStatus, fromDate,
                toDate, "", "");

            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            txtEditNote.Text = userInfo.note;

            winUserSPDetail_txtCurrentEE.Text =
                Functions.FormatDouble(
                    myEEWebService.GetEEModelCurrent(userInfo.so_tai_khoan, userInfo.goi_dich_vu_hien_tai),
                    3);
            winUserSPDetail_txtNewEE.Text =
                Functions.FormatDouble(
                    myEEWebService.GetEEModelNew(userInfo.so_tai_khoan, userInfo.goi_dich_vu_moi),
                    3);

            dynamic vipCustomer =
                myCommonService.TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_MA_KHACH_HANG(userInfo.so_tai_khoan.Substring(0, 6));
            if (vipCustomer == null)
            {
                winUserSPDetail_txtCustomerLevelName.Text =
                    CommonUtils.ConvertCustomerLevelCodeToName(CustomerLevelCodeConst.NORMAL);
            }
            else
            {
                winUserSPDetail_txtCustomerLevelName.Text =
                    CommonUtils.ConvertCustomerLevelCodeToName(int.Parse(vipCustomer.loai_vip.ToString()));
            }

            dynamic defaultMarginPackage = myMarginSPM.TVSI_sSPM_SELECT_DEFAULT_MARGIN_PACKAGE();
            if (defaultMarginPackage == null)
            {
                winUserSPDetail_txtBasicInterestType.Text = "Không tìm thấy thông tin";
            }
            else
            {
                winUserSPDetail_txtBasicInterestType.Text = defaultMarginPackage.lai_suat_van_hanh.ToString("N3");
            }

            string customerLevelName = winUserSPDetail_txtCustomerLevelName.Text;
            dynamic myStaff = myCommonService.TVSI_sSPM_STAFF_SELECT_BY_CUSTOMER_CODE(userInfo.so_tai_khoan.Substring(0, 6));
            if (myStaff != null)
            {
                customerLevelName = CommonUtils.ConvertCustomerLevelCodeToName(CustomerLevelCodeConst.PLATINUM);
                winUserSPDetail_chkStaffChecker.Checked = true;
            }
            else
            {
                winUserSPDetail_chkStaffChecker.Checked = false;
            }

            dynamic myInterestRate =
                myMarginSPM.TVSI_sSPM_CUSTOMER_LEVEL_AND_SERVICE_PACKAGE_SELECT(
                    customerLevelName, userInfo.goi_dich_vu_moi);
            if (myInterestRate == null)
            {
                winUserSPDetail_txtAccountInterestType.Text = "Không tìm thấy thông tin";
            }
            else
            {
                winUserSPDetail_txtAccountInterestType.Text = myInterestRate.interest_rate.ToString("N3");
            }

            dynamic newMarginPackage = myMarginSPM.TVSI_sSPM_SELECT_MARGIN_PACKAGE_BY_NAME(userInfo.goi_dich_vu_moi);
            if (newMarginPackage == null)
            {
                winUserSPDetail_txtAdjustInitialRate.Text = "Không tìm thấy thông tin";
                winUserSPDetail_txtAdjustCallRate.Text = "Không tìm thấy thông tin";
                winUserSPDetail_txtAdjustForceSellRate.Text = "Không tìm thấy thông tin";
            }
            else
            {
                decimal defaultAdjustInitialRate = 0m;
                decimal defaultAdjustCallRate = 0m;
                decimal defaultAdjustForceSellRate = 0m;
                decimal newAdjustInitialRate = 0m;
                decimal newAdjustCallRate = 0m;
                decimal newAdjustForceSellRate = 0m;

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_initial_rate.ToString(),
                        out defaultAdjustInitialRate) &&
                    decimal.TryParse(newMarginPackage.adjust_initial_rate.ToString(), out newAdjustInitialRate))
                {
                    winUserSPDetail_txtAdjustInitialRate.Text =
                        (newAdjustInitialRate - defaultAdjustInitialRate).ToString("N3");
                }
                else
                {
                    winUserSPDetail_txtAdjustInitialRate.Text = "Tính toán không thành công";
                }

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_call_rate.ToString(), out defaultAdjustCallRate) &&
                    decimal.TryParse(newMarginPackage.adjust_call_rate.ToString(), out newAdjustCallRate))
                {
                    winUserSPDetail_txtAdjustCallRate.Text = (newAdjustCallRate - defaultAdjustCallRate).ToString("N3");
                }
                else
                {
                    winUserSPDetail_txtAdjustCallRate.Text = "Tính toán không thành công";
                }

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_force_sell_rate.ToString(),
                        out defaultAdjustForceSellRate) &&
                    decimal.TryParse(newMarginPackage.adjust_force_sell_rate.ToString(), out newAdjustForceSellRate))
                {
                    winUserSPDetail_txtAdjustForceSellRate.Text =
                        (newAdjustForceSellRate - defaultAdjustForceSellRate).ToString("N3");
                }
                else
                {
                    winUserSPDetail_txtAdjustForceSellRate.Text = "Tính toán không thành công";
                }
            }

            DateTime nextTradingDay = myCommonService.GetNextTradingDay(DateTime.Now, 1);
            winUserSPDetail_dtxEffectiveDate.SelectedDate = nextTradingDay;

            dynamic preferentialPackage =
                myCommonService.TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_ACTIVE_RECORD(
                    userInfo.so_tai_khoan, nextTradingDay);
            if (preferentialPackage == null)
            {
                hidPreferentialID.Value = "-1";
                winUserSPDetail_txtPreferentialPackageName.Text = "Không tìm thấy thông tin";
            }
            else
            {
                hidPreferentialID.Value = preferentialPackage.dang_ky_su_dung_dich_vu_id;
                winUserSPDetail_txtPreferentialPackageName.Text = preferentialPackage.ma_dich_vu.ToString();
                winUserSPDetail_txtAccountInterestType.Text = "0";
            }
            
            dynamic currentEffectivePreferentialPackage =
                myCommonService.TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_CURRENT_EFFECTIVE_RECORD(
                    userInfo.so_tai_khoan);
            if (currentEffectivePreferentialPackage == null)
            {
                winUserSPDetail_txtCurrentPreferentialPackageName.Text = "Không tìm thấy thông tin";
                winUserSPDetail_chkInterestRateChecker.Checked = true;
            }
            else
            {
                winUserSPDetail_txtCurrentPreferentialPackageName.Text = currentEffectivePreferentialPackage.ma_dich_vu.ToString();
                winUserSPDetail_chkInterestRateChecker.Checked = false;
            }

            decimal basicInterestType = 0m;
            decimal accountInterestType = 0m;
            if (decimal.TryParse(winUserSPDetail_txtBasicInterestType.Text, out basicInterestType) &&
                decimal.TryParse(winUserSPDetail_txtAccountInterestType.Text, out accountInterestType))
            {
                winUserSPDetail_txtSBAInterestType.Text = (accountInterestType - basicInterestType).ToString("N3");
            }
            else
            {
                winUserSPDetail_txtSBAInterestType.Text = "Tính toán không thành công";
            }

            var myMarginPackageRequestQueue =
                myMarginPackageRequestQueueService.TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT(sp_id);
            if (myMarginPackageRequestQueue != null)
            {
                winUserSPDetail_txtInterestType.Text = myMarginPackageRequestQueue.interest_type;
                winUserSPDetail_chkInterestRateChecker.Checked = myMarginPackageRequestQueue.interest_rate_checker;
                winUserSPDetail_chkMarginRateChecker.Checked = myMarginPackageRequestQueue.margin_rate_checker;
            }
            else
            {
                winUserSPDetail_txtInterestType.Text = "5";
                if (currentEffectivePreferentialPackage == null)
                {
                    winUserSPDetail_chkInterestRateChecker.Checked = true;
                }
                winUserSPDetail_chkMarginRateChecker.Checked = true;
            }

            if (preferentialPackage != null)
            {
                winUserSPDetail_txtRemarkSBA.Text = $"TK dang ky {preferentialPackage.ma_dich_vu}";
            }
            else if (myStaff != null)
            {
                winUserSPDetail_txtRemarkSBA.Text = $"Nhan vien chuyen goi {userInfo.goi_dich_vu_moi}";
            }
            else
            {
                winUserSPDetail_txtRemarkSBA.Text =
                    $"Hang {winUserSPDetail_txtCustomerLevelName.Text} chuyen goi {userInfo.goi_dich_vu_moi}";
            }

            winUserSPDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ApproveUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            winApproveUser_txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            winApproveUser_txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            winApproveUser_cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            winApproveUser_cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            winApproveUser_cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            winApproveUser_dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            winApproveUser_dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            winApproveUser_dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            winApproveUser_txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            winApproveUser_txtEditNote.Text = userInfo.note;

            winApproveUser_txtCurrentEE.Text =
                Functions.FormatDouble(
                    myEEWebService.GetEEModelCurrent(userInfo.so_tai_khoan, userInfo.goi_dich_vu_hien_tai),
                    3);
            winApproveUser_txtNewEE.Text =
                Functions.FormatDouble(
                    myEEWebService.GetEEModelNew(userInfo.so_tai_khoan, userInfo.goi_dich_vu_moi),
                    3);

            dynamic vipCustomer =
                myCommonService.TVSI_sSPM_KHACH_HANG_VIP_SELECT_BY_MA_KHACH_HANG(userInfo.so_tai_khoan.Substring(0, 6));
            if (vipCustomer == null)
            {
                winApproveUser_txtCustomerLevelName.Text =
                    CommonUtils.ConvertCustomerLevelCodeToName(CustomerLevelCodeConst.NORMAL);
            }
            else
            {
                winApproveUser_txtCustomerLevelName.Text =
                    CommonUtils.ConvertCustomerLevelCodeToName(int.Parse(vipCustomer.loai_vip.ToString()));
            }

            dynamic defaultMarginPackage = myMarginSPM.TVSI_sSPM_SELECT_DEFAULT_MARGIN_PACKAGE();
            if (defaultMarginPackage == null)
            {
                winApproveUser_txtBasicInterestType.Text = "Không tìm thấy thông tin";
            }
            else
            {
                winApproveUser_txtBasicInterestType.Text = defaultMarginPackage.lai_suat_van_hanh.ToString("N3");
            }

            string customerLevelName = winApproveUser_txtCustomerLevelName.Text;
            dynamic myStaff = myCommonService.TVSI_sSPM_STAFF_SELECT_BY_CUSTOMER_CODE(userInfo.so_tai_khoan.Substring(0, 6));
            if (myStaff != null)
            {
                customerLevelName = CommonUtils.ConvertCustomerLevelCodeToName(CustomerLevelCodeConst.PLATINUM);
                winApproveUser_chkStaffChecker.Checked = true;
            }
            else
            {
                winApproveUser_chkStaffChecker.Checked = false;
            }

            dynamic myInterestRate =
                myMarginSPM.TVSI_sSPM_CUSTOMER_LEVEL_AND_SERVICE_PACKAGE_SELECT(
                    customerLevelName, userInfo.goi_dich_vu_moi);
            if (myInterestRate == null)
            {
                winApproveUser_txtAccountInterestType.Text = "Không tìm thấy thông tin";
            }
            else
            {
                winApproveUser_txtAccountInterestType.Text = myInterestRate.interest_rate.ToString("N3");
            }

            dynamic newMarginPackage = myMarginSPM.TVSI_sSPM_SELECT_MARGIN_PACKAGE_BY_NAME(userInfo.goi_dich_vu_moi);
            if (newMarginPackage == null)
            {
                winApproveUser_txtAdjustInitialRate.Text = "Không tìm thấy thông tin";
                winApproveUser_txtAdjustCallRate.Text = "Không tìm thấy thông tin";
                winApproveUser_txtAdjustForceSellRate.Text = "Không tìm thấy thông tin";
            }
            else
            {
                decimal defaultAdjustInitialRate = 0m;
                decimal defaultAdjustCallRate = 0m;
                decimal defaultAdjustForceSellRate = 0m;
                decimal newAdjustInitialRate = 0m;
                decimal newAdjustCallRate = 0m;
                decimal newAdjustForceSellRate = 0m;

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_initial_rate.ToString(),
                        out defaultAdjustInitialRate) &&
                    decimal.TryParse(newMarginPackage.adjust_initial_rate.ToString(), out newAdjustInitialRate))
                {
                    winApproveUser_txtAdjustInitialRate.Text =
                        (newAdjustInitialRate - defaultAdjustInitialRate).ToString("N3");
                }
                else
                {
                    winApproveUser_txtAdjustInitialRate.Text = "Tính toán không thành công";
                }

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_call_rate.ToString(), out defaultAdjustCallRate) &&
                    decimal.TryParse(newMarginPackage.adjust_call_rate.ToString(), out newAdjustCallRate))
                {
                    winApproveUser_txtAdjustCallRate.Text = (newAdjustCallRate - defaultAdjustCallRate).ToString("N3");
                }
                else
                {
                    winApproveUser_txtAdjustCallRate.Text = "Tính toán không thành công";
                }

                if (defaultMarginPackage != null &&
                    decimal.TryParse(defaultMarginPackage.adjust_force_sell_rate.ToString(),
                        out defaultAdjustForceSellRate) &&
                    decimal.TryParse(newMarginPackage.adjust_force_sell_rate.ToString(), out newAdjustForceSellRate))
                {
                    winApproveUser_txtAdjustForceSellRate.Text =
                        (newAdjustForceSellRate - defaultAdjustForceSellRate).ToString("N3");
                }
                else
                {
                    winApproveUser_txtAdjustForceSellRate.Text = "Tính toán không thành công";
                }
            }

            DateTime nextTradingDay = myCommonService.GetNextTradingDay(DateTime.Now, 1);
            winApproveUser_dtxEffectiveDate.SelectedDate = nextTradingDay;

            dynamic preferentialPackage =
                myCommonService.TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_ACTIVE_RECORD(
                    userInfo.so_tai_khoan, nextTradingDay);
            if (preferentialPackage == null)
            {
                hidPreferentialID.Value = "-1";
                winApproveUser_txtPreferentialPackageName.Text = "Không tìm thấy thông tin";
            }
            else
            {
                hidPreferentialID.Value = preferentialPackage.dang_ky_su_dung_dich_vu_id;
                winApproveUser_txtPreferentialPackageName.Text = preferentialPackage.ma_dich_vu.ToString();
                winApproveUser_txtAccountInterestType.Text = "0";
            }
            
            dynamic currentEffectivePreferentialPackage =
                myCommonService.TVSI_sSPM_MARGIN_ACTIVE_DANG_KY_SU_DUNG_DICH_VU_SELECT_CURRENT_EFFECTIVE_RECORD(
                    userInfo.so_tai_khoan);
            if (currentEffectivePreferentialPackage == null)
            {
                winApproveUser_txtCurrentPreferentialPackageName.Text = "Không tìm thấy thông tin";
                winApproveUser_chkInterestRateChecker.Checked = true;
            }
            else
            {
                winApproveUser_txtCurrentPreferentialPackageName.Text = currentEffectivePreferentialPackage.ma_dich_vu.ToString();
                winApproveUser_chkInterestRateChecker.Checked = false;
            }

            decimal basicInterestType = 0m;
            decimal accountInterestType = 0m;
            if (decimal.TryParse(winApproveUser_txtBasicInterestType.Text, out basicInterestType) &&
                decimal.TryParse(winApproveUser_txtAccountInterestType.Text, out accountInterestType))
            {
                winApproveUser_txtSBAInterestType.Text = (accountInterestType - basicInterestType).ToString("N3");
            }
            else
            {
                winApproveUser_txtSBAInterestType.Text = "Tính toán không thành công";
            }

            var myMarginPackageRequestQueue =
                myMarginPackageRequestQueueService.TVSI_sSPM_MARGIN_PACKAGE_REQUEST_QUEUE_SELECT(sp_id);
            if (myMarginPackageRequestQueue != null)
            {
                winApproveUser_txtInterestType.Text = myMarginPackageRequestQueue.interest_type;
                winApproveUser_chkInterestRateChecker.Checked = myMarginPackageRequestQueue.interest_rate_checker;
                winApproveUser_chkMarginRateChecker.Checked = myMarginPackageRequestQueue.margin_rate_checker;
            }
            else
            {
                winApproveUser_txtInterestType.Text = "5";
                if (currentEffectivePreferentialPackage == null)
                {
                    winApproveUser_chkInterestRateChecker.Checked = true;
                }
                winApproveUser_chkMarginRateChecker.Checked = true;
            }

            if (preferentialPackage != null)
            {
                winApproveUser_txtRemarkSBA.Text = $"TK dang ky {preferentialPackage.ma_dich_vu}";
            }
            else if (myStaff != null)
            {
                winApproveUser_txtRemarkSBA.Text = $"Nhan vien chuyen goi {userInfo.goi_dich_vu_moi}";
            }
            else
            {
                winApproveUser_txtRemarkSBA.Text =
                    $"Hang {winApproveUser_txtCustomerLevelName.Text} chuyen goi {userInfo.goi_dich_vu_moi}";
            }

            winApproveUser.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeclineUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtDecLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            winDeclineUser.Show();
        }

        protected void btnUpdateMarginPackageRequest_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }

            decimal accountInterestType = 0m;
            decimal basicInterestType = 0m;
            decimal adjustInitialRate = 0m;
            decimal adjustCallRate = 0m;
            decimal adjustForceSellRate = 0m;
            decimal.TryParse(winUserSPDetail_txtAccountInterestType.Text, out accountInterestType);
            decimal.TryParse(winUserSPDetail_txtBasicInterestType.Text, out basicInterestType);
            decimal.TryParse(winUserSPDetail_txtAdjustInitialRate.Text, out adjustInitialRate);
            decimal.TryParse(winUserSPDetail_txtAdjustCallRate.Text, out adjustCallRate);
            decimal.TryParse(winUserSPDetail_txtAdjustForceSellRate.Text, out adjustForceSellRate);

            MarginPackageRequestQueueModel myRequest = new MarginPackageRequestQueueModel()
            {
                margin_package_request_id = long.Parse(hidID.Value.ToString()),
                preferential_package_request_id = int.Parse(hidPreferentialID.Value.ToString()),
                preferential_package_name = winUserSPDetail_txtPreferentialPackageName.Text,
                customer_account = txtEditSo_tai_khoan.Text,
                current_service_package = cboEditGDV_hien_tai.SelectedItem.Value,
                new_service_package = cboEditGDV_moi.SelectedItem.Value,
                interest_type = winUserSPDetail_txtInterestType.Text,
                account_interest_rate = accountInterestType,
                basic_interest_rate = basicInterestType,
                adjust_initial_rate = adjustInitialRate,
                adjust_call_rate = adjustCallRate,
                adjust_force_sell_rate = adjustForceSellRate,
                customer_level_name = winUserSPDetail_txtCustomerLevelName.Text,
                staff_checker = winUserSPDetail_chkStaffChecker.Checked,
                remark_sba = winUserSPDetail_txtRemarkSBA.Text,
                effective_date = winUserSPDetail_dtxEffectiveDate.SelectedDate,
                interest_rate_checker = winUserSPDetail_chkInterestRateChecker.Checked,
                margin_rate_checker = winUserSPDetail_chkMarginRateChecker.Checked,
                margin_package_request_queue_status = MarginPackageRequestQueueStatusConst.KHOI_TAO,
                current_ee = decimal.Parse(winUserSPDetail_txtCurrentEE.Text),
                new_ee = decimal.Parse(winUserSPDetail_txtNewEE.Text),
                updated_by = username
            };

            var updateResult = myMarginPackageRequestQueueService.UpdateBasicInfo(myRequest);
            if (updateResult > 0)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Message = "Cập nhật thành công"
                });

                frmEditUser.Reset();
                winUserSPDetail.Hide();
                GridDataBind();
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Cập nhật thất bại"
                });
            }
        }

        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserSPDetail.Hide();
        }

        public void btnApproveUser_Click(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }

                var sp_id = Convert.ToInt32(hidID.Value);
                var userSPService = new ChangeUserServicePack();
                var userInfo = userSPService.GetUserSPByID(sp_id);

                if (userInfo.so_tai_khoan != null)
                {
                    var checkCancelLenh = userSPService.GetCancelLenhGoiDichVu(userInfo.so_tai_khoan);
                    if (checkCancelLenh != null)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Message = "Số tài khoản này đang có lệnh hủy!!"
                        });
                        return;
                    }
                }

                /* Lưu thông tin ở thời điểm hiện tại vào Hàng đợi --- BEGIN */
                decimal accountInterestType = 0m;
                decimal basicInterestType = 0m;
                decimal adjustInitialRate = 0m;
                decimal adjustCallRate = 0m;
                decimal adjustForceSellRate = 0m;
                decimal.TryParse(winApproveUser_txtAccountInterestType.Text, out accountInterestType);
                decimal.TryParse(winApproveUser_txtBasicInterestType.Text, out basicInterestType);
                decimal.TryParse(winApproveUser_txtAdjustInitialRate.Text, out adjustInitialRate);
                decimal.TryParse(winApproveUser_txtAdjustCallRate.Text, out adjustCallRate);
                decimal.TryParse(winApproveUser_txtAdjustForceSellRate.Text, out adjustForceSellRate);

                MarginPackageRequestQueueModel myRequest = new MarginPackageRequestQueueModel()
                {
                    margin_package_request_id = long.Parse(hidID.Value.ToString()),
                    preferential_package_request_id = int.Parse(hidPreferentialID.Value.ToString()),
                    preferential_package_name = winApproveUser_txtPreferentialPackageName.Text,
                    customer_account = winApproveUser_txtEditSo_tai_khoan.Text,
                    current_service_package = winApproveUser_cboEditGDV_hien_tai.SelectedItem.Value,
                    new_service_package = winApproveUser_cboEditGDV_moi.SelectedItem.Value,
                    interest_type = winApproveUser_txtInterestType.Text,
                    account_interest_rate = accountInterestType,
                    basic_interest_rate = basicInterestType,
                    adjust_initial_rate = adjustInitialRate,
                    adjust_call_rate = adjustCallRate,
                    adjust_force_sell_rate = adjustForceSellRate,
                    customer_level_name = winApproveUser_txtCustomerLevelName.Text,
                    staff_checker = winApproveUser_chkStaffChecker.Checked,
                    remark_sba = winApproveUser_txtRemarkSBA.Text,
                    effective_date = winApproveUser_dtxEffectiveDate.SelectedDate,
                    interest_rate_checker = winApproveUser_chkInterestRateChecker.Checked,
                    margin_rate_checker = winApproveUser_chkMarginRateChecker.Checked,
                    margin_package_request_queue_status = MarginPackageRequestQueueStatusConst.CHO_XU_LY,
                    current_ee = decimal.Parse(winApproveUser_txtCurrentEE.Text),
                    new_ee = decimal.Parse(winApproveUser_txtNewEE.Text),
                    updated_by = username
                };

                var updateResult = myMarginPackageRequestQueueService.UpdateBasicInfo(myRequest);
                if (updateResult <= 0)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Cập nhật hàng đợi thất bại"
                    });
                    return;
                }
                /* Lưu thông tin ở thời điểm hiện tại vào Hàng đợi --- END */

                var changed = userSPService.ChangeUserSPStatusByID(sp_id, 1, "");

                if (changed)
                {
                    winApproveUser.Hide();
                    GridDataBind();
                    var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                    accNumLengString += "1";

                    var userName = userInfo.ho_ten_khach_hang;
                    var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);


                    if (!string.IsNullOrEmpty(userEmail.dia_chi_email))
                    {
                        var curSP = userInfo.goi_dich_vu_hien_tai;
                        var newSP = userInfo.goi_dich_vu_moi;
                        var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                        /*Gửi Mail thông báo cho khách hàng đã được duyệt lệnh thành công*/
                        userSPService.SendConfirmEmailToUser(userName, userEmail.dia_chi_email, curSP, newSP, fromDate);
                    }
                    else
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Message = "Không tìm thấy Email cho tài khoản này."
                        });
                    }

                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Đổi gói đăng ký dịch vụ thành công."
                    });
                }

                GridDataBind();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Approve lệnh duyệt thất bại " + ex.Message);
            }
        }

        public void btnDeclineUser_Click(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }
                
                var userSPService = new ChangeUserServicePack();

                var sp_id = Convert.ToInt32(hidID.Value);
                var declineReason = txtDecLy_do_tu_choi.Text.Trim();
                var changed = userSPService.ChangeUserSPStatusByID(sp_id, -1, declineReason);
                var userInfo = userSPService.GetUserSPByID(sp_id);
                if (changed)
                {
                    var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                    accNumLengString += "1";
                    var userName = userInfo.ho_ten_khach_hang;
                    var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);
                    if (!string.IsNullOrEmpty(userEmail.dia_chi_email))
                    {
                        var curSP = userInfo.goi_dich_vu_hien_tai;
                        var newSP = userInfo.goi_dich_vu_moi;
                        var lydotuchoi = userInfo.ly_do_tu_choi;
                        var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                        // gửi mail khi được duyệt lệnh bị từ chối
                        userSPService.SendDeclineEmailToUser(userName, userEmail.dia_chi_email, lydotuchoi);
                    }

                    winDeclineUser.Hide();
                    if (changed)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Message = "Từ chối đăng ký dịch vụ thành công."
                        });
                    }

                    GridDataBind();
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Reject lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }

        public void rejectAll(object sender, EventArgs e)
        {
            try
            {
                winDeclineAllUser.Show();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(approve_user_service), "rejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }

        public void BtnRejectAll(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }
                
                var changeUserServicePack = new ChangeUserServicePack();
                StringBuilder result = new StringBuilder();
                RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;

                var list = sm.SelectedRows;
                var lyDoTuChoiAll = txtDecLy_do_tu_choi_all.Text;
                if (list.Count <= 0)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.CANCEL,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
                    });
                    return;
                }

                List<string> listID = new List<string>();
                var changed = false;
                foreach (SelectedRow row in list)
                {
                    changed = changeUserServicePack.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), -1,
                        lyDoTuChoiAll);
                    if (changed == false)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.CANCEL,
                            Icon = MessageBox.Icon.ERROR,
                            Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
                        });
                    }
                }

                sm.SelectedRows.Clear();
                sm.UpdateSelection();
                GridDataBind();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(approve_user_service),
                    "BtnRejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }
    }
}