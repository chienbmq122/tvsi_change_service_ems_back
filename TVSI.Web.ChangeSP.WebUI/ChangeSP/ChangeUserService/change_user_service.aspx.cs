﻿using Ext.Net;
using System;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.ChangeUserService
{
    public partial class change_user_service : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var servicePackList = ConstParam.ServicePackList;

                foreach (var item in servicePackList)
                {
                    cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboEditGDV_moi.Items.Add(new ListItem(item, item));
                    cboAddGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboAddGDV_moi.Items.Add(new ListItem(item, item));
                }

                cboAddGDV_hien_tai.SelectedIndex = 0;
                cboAddGDV_moi.SelectedIndex = 0;

                GridDataBind();
            }
        }

        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();

            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.SearchUserServicePackList(accountNo, userName, "", "", DateTime.MinValue, DateTime.MinValue);

            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;

            winUserSPDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ApproveUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            winApproveUser.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeclineUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtDecLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            winDeclineUser.Show();
        }

        public void btnEditUser_Click(object sender, EventArgs e)
        {
            var id = int.Parse(hidID.Value + "");
            var accountNo = txtEditSo_tai_khoan.Text.Trim();
            var userName = txtEditTen_khach_hang.Text.Trim();
            var curStatus = int.Parse(cboEditTrang_thai.SelectedItem.Value);
            var curServicePack = cboEditGDV_hien_tai.SelectedItem.Value;
            var newServicePack = cboEditGDV_moi.SelectedItem.Value;
            var signUpDate = (DateTime)dtxEditNgay_dang_ky.Value;
            var fromDate = (DateTime)dtxEditNgay_hieu_luc.Value;
            var toDate = (DateTime)dtxEditNgay_ket_thuc.Value;
            var declineReason = txtEditLy_do_tu_choi.Text.Trim();

            if (curStatus == -1 & declineReason == "")
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Lý do từ chối không được để trống"
                });
                return;
            }

            var userSPService = new ChangeUserServicePack();
            var editedUser = userSPService.UpdateUserSPByID(id, accountNo, userName, curStatus, curServicePack, newServicePack, signUpDate, fromDate, toDate, declineReason);
            if (editedUser)
            {
                winUserSPDetail.Hide();
                if (!string.IsNullOrEmpty(txtEditSo_tai_khoan.Text.Trim()))
                    GridDataBind();
            }
        }

        public void btnApproveUser_Click(object sender, EventArgs e)
        {
            var userSPService = new ChangeUserServicePack();

            var sp_id = Convert.ToInt32(hidID.Value);
            var changed = userSPService.ChangeUserSPStatusByID(sp_id, 1, "");

            if (changed)
            {
                winApproveUser.Hide();
                GridDataBind();
            }
        }

        public void btnDeclineUser_Click(object sender, EventArgs e)
        {
            var userSPService = new ChangeUserServicePack();

            var sp_id = Convert.ToInt32(hidID.Value);
            var declineReason = txtDecLy_do_tu_choi.Text.Trim();
            var changed = userSPService.ChangeUserSPStatusByID(sp_id, -1, declineReason);

            if (changed)
            {
                winDeclineUser.Hide();
                GridDataBind();
            }
        }

        public void btnAddUser_Click(object sender, EventArgs e)
        {
            txtAddSo_tai_khoan.Text = "";
            txtAddTen_khach_hang.Text = "";
            cboAddGDV_hien_tai.SelectedIndex = 0;
            cboAddGDV_moi.SelectedIndex = 0;
            dtxAddNgay_dang_ky.Value = DateTime.Now;
            dtxAddNgay_ket_thuc.Value = DateTime.MinValue;
            winAddNewUser.Show();
        }

        public void btnConfAddUser_Click(object sender, EventArgs e)
        {
            var userSPService = new ChangeUserServicePack();

            var accountNo = txtAddSo_tai_khoan.Text.Trim();

            var accountExisted = userSPService.CheckExistingUserByAccountNo(accountNo);

            if (accountExisted)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Tài khoản này đã tồn tại"
                });
                return;
            }
            var userName = txtAddTen_khach_hang.Text.Trim();
            var curServicePack = cboAddGDV_hien_tai.SelectedItem.Value;
            var newServicePack = cboAddGDV_moi.SelectedItem.Value;
            var signUpDate = (DateTime)dtxAddNgay_dang_ky.Value;
            var toDate = (DateTime)dtxAddNgay_ket_thuc.Value;

            var addedUser = userSPService.AddUserSP(accountNo, userName, curServicePack, newServicePack, signUpDate, toDate);

            if (addedUser)
            {
                winAddNewUser.Hide();
                GridDataBind();
            }
        }
    }
}