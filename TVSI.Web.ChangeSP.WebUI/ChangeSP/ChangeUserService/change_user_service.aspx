﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="change_user_service.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.ChangeSP.ChangeUserService.change_user_service" %>

<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdStockApiList(command, data) {
                if (command == "View") {
                    USERMETHOD.ViewUserSPDetail(data.goi_dich_vuid);
                }
                if (command == "Approve") {
                    USERMETHOD.ApproveUserSPChange(data.goi_dich_vuid);
                }
                if (command == "Decline") {
                    USERMETHOD.DeclineUserSPChange(data.goi_dich_vuid);
                }
            }

            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';

            var renderSPStatus = function (value) {
                if (value == "-1")
                    return "Từ chối"
                if (value == "0")
                    return "Tạo mới"
                if (value == "1")
                    return "Thàng công"
                if (value == "99")
                    return "Xóa logic"
                return "Bảo thằng Đức nhập thêm " + value
            };

            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
        </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager runat="server" />
    <form id="form1" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="Số TK " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchAccountNo" Width="80" />
                                <ext:ToolbarSeparator />
                                <ext:Label Text="Họ tên KH " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchUserName" Width="120" />
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeListUserService}.reload()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Thêm mới" ID="btnAdd" Icon="ApplicationAdd" CausesValidation="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddUser_Click" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdListUserService" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeListUserService" runat="server" OnRefreshData="storeListUserService_Refresh">
                            <Reader>
                                <ext:JsonReader IDProperty="goi_dich_vuid">
                                    <Fields>
                                        <ext:RecordField Name="goi_dich_vuid" />
                                        <ext:RecordField Name="so_tai_khoan" />
                                        <ext:RecordField Name="ho_ten_khach_hang" />
                                        <ext:RecordField Name="ngay_dang_ky" />
                                        <ext:RecordField Name="ngay_hieu_luc" />
                                        <ext:RecordField Name="ngay_ket_thuc" />
                                        <ext:RecordField Name="goi_dich_vu_hien_tai" />
                                        <ext:RecordField Name="goi_dich_vu_moi" />
                                        <ext:RecordField Name="ly_do_tu_choi" />
                                        <ext:RecordField Name="trang_thai" />
                                        <ext:RecordField Name="ngay_tao" />
                                        <ext:RecordField Name="nguoi_tao" />
                                        <ext:RecordField Name="ngay_phe_duyet" />
                                        <ext:RecordField Name="nguoi_phe_duyet" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="20" Header="#" />
                            <ext:Column DataIndex="goi_dich_vuid" Header="GDV ID" Width="50" />
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Zoom" CommandName="View" Text="View" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Accept" CommandName="Approve" Text="Approve" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Decline" CommandName="Decline" Text="Reject" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:Column DataIndex="so_tai_khoan" Header="Số TK" Width="60" />
                            <ext:Column DataIndex="ho_ten_khach_hang" Header="Tên KH" Width="100" />
                            <ext:DateColumn DataIndex="ngay_dang_ky" Header="Ngày đăng ký" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:DateColumn DataIndex="ngay_hieu_luc" Header="Ngày hiệu lực" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:DateColumn DataIndex="ngay_ket_thuc" Header="Ngày kết thúc" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="goi_dich_vu_hien_tai" Header="GDV hiện tại" Width="100" />
                            <ext:Column DataIndex="goi_dich_vu_moi" Header="GDV mới" Width="100" />
                            <ext:Column DataIndex="ly_do_tu_choi" Header="Lý do từ chối" Width="200" Hidden="True" />
                            <ext:Column DataIndex="trang_thai" Header="Trạng thái" Width="80">
                                <Renderer Fn="renderSPStatus"></Renderer>
                            </ext:Column>
                            <ext:DateColumn DataIndex="ngay_tao" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss" Hidden="True" />
                            <ext:Column DataIndex="nguoi_tao" Header="Người tạo" Width="120" Hidden="True" />
                            <ext:DateColumn DataIndex="ngay_phe_duyet" Header="Ngày phê duyệt" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_phe_duyet" Header="Người phê duyệt" Width="120" />
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdStockApiList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window runat="server" ID="winUserSPDetail" Title="Yêu cầu chuyển đổi GDV" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditUser">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditSo_tai_khoan" Width="200" FieldLabel="Số tài khoản" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditTen_khach_hang" Width="200" FieldLabel="Họ và tên khách hàng" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="cboEditTrang_thai" Width="150" FieldLabel="Trạng thái">
                            <Items>
                                <ext:ListItem Value="0" Text="Tạo mới" />
                                <ext:ListItem Value="1" Text="Thành công" />
                                <ext:ListItem Value="-1" Text="Từ chối" />
                                <ext:ListItem Value="99" Text="Xóa logic" />
                            </Items>
                        </ext:ComboBox>
                        <ext:ComboBox runat="server" ID="cboEditGDV_hien_tai" Width="150" FieldLabel="GDV Hiện tại" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="cboEditGDV_moi" Width="150" FieldLabel="GDV mới" AllowBlank="False" />
                        <ext:DateField runat="server" ID="dtxEditNgay_dang_ky" Width="150" FieldLabel="Ngày đăng ký" AllowBlank="False" />
                        <ext:DateField runat="server" ID="dtxEditNgay_hieu_luc" Width="150" FieldLabel="Ngày hiệu lực" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_ket_thuc" Width="150" FieldLabel="Ngày kết thúc" />
                        <ext:TextField runat="server" ID="txtEditLy_do_tu_choi" Width="200" FieldLabel="Lý do từ chối" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Cập nhật" Icon="ApplicationAdd">
                            <Listeners>
                                <Click Handler="if(#{frmEditUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnEditUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật User này không?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winApproveUser" Title="Chấp thuận cho user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmApprvUser">
                    <Buttons>
                        <ext:Button runat="server" Text="Chấp thuận" Icon="Accept">
                            <DirectEvents>
                                <Click OnEvent="btnApproveUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có chấp thuận việc đổi gói SP cho user này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeclineUser" Title="Từ chối user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecUser">
                    <Items>
                        <ext:TextField runat="server" ID="txtDecLy_do_tu_choi" Width="200" FieldLabel="Lý do từ chối" AllowBlank="False" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Từ chối" Icon="Decline">
                            <Listeners>
                                <Click Handler="if(#{frmDecUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnDeclineUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có chắc chắn từ chối việc đổi gói SP cho user này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winAddNewUser" Title="Thêm thông tin thay đổi gói dịch vụ của khách hàng"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmAddUser">
                    <Items>
                        <ext:TextField runat="server" ID="txtAddSo_tai_khoan" Width="200" FieldLabel="Số tài khoản" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddTen_khach_hang" Width="200" FieldLabel="Họ và tên khách hàng" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="txtAddTrang_thai" Width="150" FieldLabel="Trạng thái" ReadOnly="True">
                            <Items>
                                <ext:ListItem Value="0" Text="Tạo mới" />
                                <ext:ListItem Value="1" Text="Thành công" />
                                <ext:ListItem Value="-1" Text="Từ chối" />
                                <ext:ListItem Value="99" Text="Xóa logic" />
                            </Items>
                            <SelectedItem Value="0" />
                        </ext:ComboBox>
                        <ext:ComboBox runat="server" ID="cboAddGDV_hien_tai" Width="150" FieldLabel="GDV Hiện tại" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="cboAddGDV_moi" Width="150" FieldLabel="GDV mới" AllowBlank="False" />
                        <ext:DateField runat="server" ID="dtxAddNgay_dang_ky" Width="150" FieldLabel="Ngày đăng ký" AllowBlank="False" />
                        <ext:DateField runat="server" ID="dtxAddNgay_ket_thuc" Width="150" FieldLabel="Ngày kết thúc" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" ID="btnConfAddUser" Text="Thêm mới" Icon="ApplicationAdd">
                            <Listeners>
                                <Click Handler="if(#{frmAddUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnConfAddUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý thêm thông tin thay đổi GDV cho khách hàng này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchAccountNo}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
        <ext:KeyMap ID="KeyMap2" runat="server" Target="#{txtSearchUserName}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
    </form>
</body>
</html>