﻿<%@ Page Language="C#" CodeBehind="hits_current_sp.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.ChangeSP.HisCurrentSP.hits_current_sp" %>

<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdStockApiList(command, data) {
                if (command == "View") {
                    USERMETHOD.ViewUserSPDetail(data.goi_dich_vuid);
                }
                if (command == "Approve") {
                    USERMETHOD.ApproveUserSPChange(data.goi_dich_vuid);
                }
                if (command == "Decline") {
                    USERMETHOD.DeclineUserSPChange(data.goi_dich_vuid);
                }
            }

            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';
            var template1 = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>'

       				         var renderSPStatus = function (value) {
                       if (value == "-1")
                           return String.format(template, "#ffab43", "Từ chối");
                       if (value == "0")
                           return String.format(template, "#1ae5e5", "Tạo mới");
                       if (value == "1")
                           return String.format(template, "#1cff00", " Thành công");
                       if(value == "5")
                            return String.format(template, "#f108e5", "Hủy đăng ký gói"); 
                       if (value == "99")
                           return String.format(template, "#de081a", "Xóa");  
                       if (value == "-2")
                           return String.format(template, "#134bf5", "Đã hủy");   
                       if(value == "-3")
                           return String.format(template, "#721101", "Ngừng sử dụng");
                       
                          return  value;
                   };      
            var renderSPPStatus = function (value) {
   
                if (value == "0")
                    return String.format(template, "#69e94c", "Tạo mới");
                if (value == "2")
                    return String.format(template, "#3168fa", "Chỉnh sửa");
                if (value == "99")
                    return String.format(template, "#f81313", "Xóa gói");
            };
            
                        var renderStatusPack = function (value) {
                            if (value == "1")
                    return  "Có";
                            if (value == "2")
                    return "Không";
                        };

            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
        </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager runat="server" />
    <form id="form1" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="Tên gói " runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:TextField runat="server" ID="txtSearchAccountNo" Width="80"/>
                                <ext:ToolbarSeparator/>
                                <ext:Label Text="Người cập nhật" runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:TextField runat="server" ID="txtSearchUserName" Width="120"/>
                                <ext:ToolbarSeparator/>
                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeListUserService}.reload()"/>
                                    </Listeners>
                                </ext:Button>
                                
                                <ext:Button ID="Export" Text="Export" runat="server" Icon="pagewhiteexcel">
                                    <DirectEvents>
                                        <Click OnEvent="btnExport_Click" IsUpload="true">
                                            <EventMask ShowMask="false"/>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdListUserService" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeListUserService" runat="server" OnRefreshData="storeListUserService_Refresh">
                            <Reader>
                                <ext:JsonReader IDProperty="log_goi_dich_vu_margin_id">
                                    <Fields>
                                        <ext:RecordField Name="log_goi_dich_vu_margin_id" />
                                        <ext:RecordField Name="ten_goi_dich_vu" />
                                        <ext:RecordField Name="trang_thai" />
                                        <ext:RecordField Name="trang_thai_log" />
                                        <ext:RecordField Name="ngay_cap_nhat_cuoi" />
                                        <ext:RecordField Name="nguoi_cap_nhat_cuoi" />
                                        <ext:RecordField Name="lai_suat" />
                                        <ext:RecordField Name="lai_suat_van_hanh" />
                                        <ext:RecordField Name="lai_suat_bang_chu" />
                                        <ext:RecordField Name="adjust_initial_rate" />
                                        <ext:RecordField Name="adjust_call_rate" />
                                        <ext:RecordField Name="adjust_force_sell_rate" />
                                        <ext:RecordField Name="cap_nhat_suc_mua" />
                                        <ext:RecordField Name="so_ngay_mien_lai" />
                                        <ext:RecordField Name="ty_le_ky_quy" />
                                        <ext:RecordField Name="ty_le_duy_tri" />
                                        <ext:RecordField Name="ty_le_call" />
                                        <ext:RecordField Name="ty_le_canh_bao" />
                                        <ext:RecordField Name="ty_le_vay" />
                                        <ext:RecordField Name="du_no_toi_da" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                  
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="30" Header="STT"/>
                            <ext:Column DataIndex="log_goi_dich_vu_margin_id" Header="ID LOG" Width="50"/>
                            <ext:Column DataIndex="trang_thai" Header="Trạng thái gói" Width="100">
                            <Renderer Fn="renderSPStatus"></Renderer>
                            </ext:Column>    
                            <ext:Column DataIndex="trang_thai_log" Header="Trạng thái (Log)" Width="100">
                            <Renderer Fn="renderSPPStatus"></Renderer>
                            </ext:Column>
                            <ext:Column DataIndex="ten_goi_dich_vu" Header="Tên gói dịch vụ" Width="60"/>
                            <ext:Column DataIndex="lai_suat" Header="Lãi suất mặc định" Width="60"/>
                            <ext:Column DataIndex="lai_suat_van_hanh" Header="Lãi suất vận hành" Width="60" />
                            <ext:Column DataIndex="lai_suat_bang_chu" Header="Lãi suất bằng chữ" Width="60" />
                            <ext:Column DataIndex="cap_nhat_suc_mua" Header="Cấp sức mua trong ngày" Width="60">
                                <Renderer Fn="renderStatusPack"></Renderer>
                            </ext:Column>
                            <ext:Column DataIndex="so_ngay_mien_lai" Header="Số ngày miễn lãi" Width="60"/>
                             <ext:Column DataIndex="ty_le_duy_tri" Header="Tỷ lệ ký quỹ cảnh báo" Width="60"/>
                            <ext:Column DataIndex="ty_le_ky_quy" Header="Tỷ lệ ký quỹ cơ bản" Width="60"/>
                            <ext:Column DataIndex="ty_le_call" Header="Tỷ lệ ký quỹ xử lý" Width="60"/>
                            <ext:Column DataIndex="adjust_initial_rate" Header="Adjust Initial Rate" Width="60"/>
                            <ext:Column DataIndex="adjust_call_rate" Header="Adjust Call Rate" Width="60"/>
                            <ext:Column DataIndex="adjust_force_sell_rate" Header="Adjust Force Sell Rate" Width="60"/>
                            <%--
                            <ext:Column DataIndex="ty_le_canh_bao" Header="Tỷ lệ ký quỹ cảnh báo" Width="60"/>
                            --%>
                            <ext:Column DataIndex="ty_le_vay" Header="Tỷ lệ vay tối đa" Width="60"/>
                            <ext:Column DataIndex="du_no_toi_da" Header="Dư nợ tối đa" Width="60"/>
                            <ext:DateColumn DataIndex="ngay_cap_nhat_cuoi" Header="Ngày cập nhật cuối cùng" Width="120" Format="dd/MM/yyyy HH:mm:ss"/>
                            <ext:Column DataIndex="nguoi_cap_nhat_cuoi" Header="Người cập nhật cuối cùng" Width="120"/>
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdStockApiList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>

                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window runat="server" ID="winUserSPDetail" Title="Yêu cầu chuyển đổi GDV" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditUser">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditSo_tai_khoan" Width="200" FieldLabel="Số tài khoản" AllowBlank="False" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditTen_khach_hang" Width="200" FieldLabel="Họ và tên khách hàng" AllowBlank="False" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditTrang_thai" Width="150" FieldLabel="Trạng thái" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditGDV_hien_tai" Width="150" FieldLabel="GDV Hiện tại" AllowBlank="False" ReadOnly="True" />
                        <ext:ComboBox runat="server" ID="cboEditGDV_moi" Width="150" FieldLabel="GDV mới" AllowBlank="False" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_dang_ky" Width="150" FieldLabel="Ngày đăng ký" AllowBlank="False" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_hieu_luc" Width="150" FieldLabel="Ngày hiệu lực" ReadOnly="True" />
                        <ext:DateField runat="server" ID="dtxEditNgay_ket_thuc" Width="150" FieldLabel="Ngày kết thúc" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditLy_do_tu_choi" Width="200" FieldLabel="Lý do từ chối" ReadOnly="True" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Đóng" Icon="Accept">
                            <Listeners>
                                <Click Handler="if(#{frmEditUser}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winApproveUser" Title="Chấp thuận cho user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmApprvUser">
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeclineUser" Title="Từ chối user"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecUser">
                    <Items>
                        <ext:TextField runat="server" ID="txtDecLy_do_tu_choi" Width="200" FieldLabel="Lý do từ chối" AllowBlank="False" />
                    </Items>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchAccountNo}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
        <ext:KeyMap ID="KeyMap2" runat="server" Target="#{txtSearchUserName}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
    </form>
</body>
</html>