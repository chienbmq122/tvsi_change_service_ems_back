﻿using System;
using System.Web.UI;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.HisCurrentSP
{
    public partial class hits_current_sp : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridDataBind();

        }
        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }
        
        private void GridDataBind()
        {
            var spName = txtSearchAccountNo.Text.Trim();
            var perSon = txtSearchUserName.Text.Trim();
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.SearchHistCurrentSpModels(spName,perSon);
            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var spName = txtSearchAccountNo.Text.Trim();
            var perSon = txtSearchUserName.Text.Trim();
            var userSPService = new ChangeUserServicePack();
            userSPService.ExportHistCurrentSp(spName,perSon);

        }
    }
}