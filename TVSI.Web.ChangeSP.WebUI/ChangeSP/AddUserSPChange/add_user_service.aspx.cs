﻿using Ext.Net;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Service.WebService;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;
using MessageBox = Ext.Net.MessageBox;
using Task = System.Threading.Tasks.Task;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.AddUserSPChange
{
    public partial class add_user_service : System.Web.UI.Page
    {
        private readonly GetEEWebService myEEWebService;

        protected add_user_service()
        {
            myEEWebService = new GetEEWebService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userSPService = new ChangeUserServicePack();

                var servicePackList = userSPService.GetSP();

                foreach (var item in servicePackList)
                {
                    cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboEditGDV_moi.Items.Add(new ListItem(item, item));
                    cboAddGDV_hien_tai.Items.Add(new ListItem(item, item));

                    cboAddGDV_moi.Items.Add(new ListItem(item, item));
                    /*
                    cboAddGDV_hien_tai7number.Items.Add(new ListItem(item, item));
                    */
                    cboAddGDV_moi7number.Items.Add(new ListItem(item, item));
                }

                cboAddGDV_moi.SelectedIndex = 0;
                cboAddGDV_hien_tai.SelectedIndex = 0;

                var goimoi = cboAddGDV_moi7number.SelectedIndex = 1;
                var goihientai = cboAddGDV_hien_tai7number;
                var statusDict = ConstParam.StatusDict;

                foreach (var item in statusDict)
                {
                    cboAddTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                }

                cboAddTrang_thai.SelectedIndex = 0;
                foreach (var item in statusDict)
                {
                    cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                }

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                cboSearchStatus.SelectedIndex = 0;
                GridDataBind();
            }
        }

        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var curUser = SessionHelper.GetUserName();
            var packcurrent = txtSearchPackagecurent.Text.Trim();
            var packnew = txtSearchPackageNew.Text.Trim();
            var curStatus = cboSearchStatus.SelectedItem.Value;
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "0";
            }

            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.SearchUserServicePackList(accountNo, userName, curUser, curStatus,
                DateTime.MinValue, DateTime.MinValue, packcurrent, packnew);
            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            txtEditNote.Text = userInfo.note;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            
            frmEditUser_txtCurrentEE.Text =
                Functions.FormatDouble(myEEWebService.GetEEModelCurrent(userInfo.so_tai_khoan, cboEditGDV_hien_tai.Text),
                    3);
            frmEditUser_txtNewEE.Text =
                Functions.FormatDouble(
                    myEEWebService.GetEEModelNew(userInfo.so_tai_khoan, cboEditGDV_moi.SelectedItem.Text),
                    3);
            
            winUserSPDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeleteUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);
            winDeleteUser.Show();
        }

        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserSPDetail.Hide();
        }

        public void btnDeleteUser_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var userSPService = new ChangeUserServicePack();

            var sp_id = Convert.ToInt32(hidID.Value);
            var changed = userSPService.ChangeUserSPStatusByID(sp_id, 99, "");

            if (changed)
            {
                winDeleteUser.Hide();
                GridDataBind();
            }
        }

        public void btnAddUser_Click(object sender, EventArgs e)
        {
            txtAddSo_tai_khoan.Text = "";
            txtAddTen_khach_hang.Text = "";
            cboAddGDV_hien_tai.SelectedIndex = 0;
            cboAddGDV_moi.SelectedIndex = 0;
            dtxAddNgay_dang_ky.Value = DateTime.Now;
            dtxAddNgay_ket_thuc.Value = DateTime.MinValue;
            winAddNewUser.Show();
        }

        public void TextBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var packagewebservice = new PackAgeWebService();
                var numberAcc = txtAddSo_tai_khoan7number.Text.Trim();
                if (numberAcc.Length != 7)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Tài khoản không đủ 7 số hoặc thừa."
                    });
                    return;
                }

                if (!numberAcc.EndsWith("6"))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Tài khoản không có đuôi 6."
                    });
                    return;
                }

                if (string.IsNullOrEmpty(numberAcc))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Bạn chưa nhập số tài khoản."
                    });
                    return;
                }

                var accNumLengString = numberAcc.Remove(numberAcc.Length - 1);
                var userSPService = new ChangeUserServicePack();
                if (userSPService.CheckSourceAccount(accNumLengString))
                {
                    if (!userSPService.CheckEKYCProfile(accNumLengString))
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR,
                            Message = "Tài khoản eKYC chưa hồ sơ, vui lòng hoàn thiện hồ sơ trước khi đăng ký"
                        });
                        return;
                    }
                }

                var accNum = userSPService.SearchNameByAccNumber(accNumLengString);
                txtAddTen_khach_hang7number.Text = accNum;
                if (string.IsNullOrEmpty(accNum))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = "Không tìm thấy số tài khoản."
                    });
                    return;
                }


                var currentpack = packagewebservice.GetPackageCurrentModel(numberAcc);
                if (!string.IsNullOrEmpty(currentpack.GOI_DICH_VU))
                {
                    cboAddGDV_hien_tai7number.Value = currentpack.GOI_DICH_VU;
                }
                else
                {
                    cboAddGDV_hien_tai7number.Value = "BASIC";
                }

                if (string.IsNullOrEmpty(currentpack.GOI_DICH_VU))
                    currentpack.GOI_DICH_VU = "BASIC";

                txtCurrentEE.Text =
                    Functions.FormatDouble(myEEWebService.GetEEModelCurrent(numberAcc, cboAddGDV_hien_tai7number.Text),
                        3);
                txtNewEE.Text =
                    Functions.FormatDouble(
                        myEEWebService.GetEEModelNew(numberAcc, cboAddGDV_moi7number.SelectedItem.Text),
                        3);

                /*var custcode = numberAcc.Remove(numberAcc.Length - 1);
                var valueChange = userSPService.GetChangeMargin(custcode, currentpack.GOI_DICH_VU, 1);
                if (!valueChange.Status.Equals("000"))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO,
                        Message = valueChange.Message
                    });
                    txtAddNote7number.Value = valueChange.Note;
                }*/
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(add_user_service), "Lấy thông tin" + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
                return;
            }
        }

        protected void SetTimePackage(object sender, EventArgs e)
        {
            var packnew = cboAddGDV_moi7number.SelectedItem.Text;
            var customerAccount = txtAddSo_tai_khoan7number.Text;
            
            try
            {
                if (packnew.Trim().Equals("BASIC"))
                {
                    dtxAddNgay_ket_thuc7number.Value = DateTime.MaxValue;
                    dtxAddNgay_ket_thuc7number.ReadOnly = true;
                }
                else
                {
                    dtxAddNgay_ket_thuc7number.Value = DateTime.MaxValue;
                    dtxAddNgay_ket_thuc7number.ReadOnly = true;
                }
                
                txtNewEE.Text =
                    Functions.FormatDouble(
                        myEEWebService.GetEEModelNew(customerAccount, packnew),
                        3);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(add_user_service), "SetTimePackage" + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }

        public void btnAddUser7number_Click(object sender, EventArgs e)
        {
            dtxAddNgay_ket_thuc7number.ReadOnly = true;
            winAddNewUser7number.Show();
            txtAddSo_tai_khoan7number.Text = "";
            txtAddTen_khach_hang7number.Text = "";
            cboAddGDV_hien_tai7number.Text = "";
            txtCurrentEE.Text = "";
            txtNewEE.Text = "";
            /*var goihientai = cboAddGDV_hien_tai7number;
            var goimoi = cboAddGDV_moi7number.SelectedIndex = 1;*/
            dtxAddNgay_dang_ky7number.Value = DateTime.Now;
            dtxAddNgay_ket_thuc7number.Value = DateTime.MaxValue;
        }

        public void btnConfAddUser_Click(object sender, EventArgs e)
        {
            /*var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }*/
            var userSPService = new ChangeUserServicePack();

            var accountNo = txtAddSo_tai_khoan.Text.Trim();

            var accountExisted = userSPService.CheckExistingUserByAccountNo(accountNo);

            if (accountExisted)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Tài khoản này đã tồn tại"
                });
                return;
            }

            var userName = txtAddTen_khach_hang.Text.Trim();
            var curServicePack = cboAddGDV_hien_tai.SelectedItem.Value;
            var newServicePack = cboAddGDV_moi.SelectedItem.Value;
            var signUpDate = (DateTime) dtxAddNgay_dang_ky.Value;
            var toDate = (DateTime) dtxAddNgay_ket_thuc.Value;
            var note = txtAddNote7number.Text.Trim();

            var addedUser = userSPService.AddUserSP(accountNo, userName, curServicePack, newServicePack, signUpDate,
                toDate, note);

            if (addedUser)
            {
                winAddNewUser.Hide();
                winAddNewUser7number.Hide();
                GridDataBind();
            }
        }

        public void btnConfAddUser7Number_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();

            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var userSPService = new ChangeUserServicePack();

            var accountNo = txtAddSo_tai_khoan7number.Text.Trim();

            if (accountNo.Length != 7)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Tài khoản không đủ 7 số hoặc thừa."
                });
                return;
            }

            if (!accountNo.EndsWith("6"))
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Tài khoản không có đuôi 6."
                });
                return;
            }

            var accountExisted = userSPService.CheckExistingUserByAccountNo(accountNo);
            if (accountExisted)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Tài khoản này đã tồn tại"
                });
                return;
            }

            var userName = txtAddTen_khach_hang7number.Text.Trim();
            var curServicePack = cboAddGDV_hien_tai7number.Text.Trim();
            var newServicePack = cboAddGDV_moi7number.SelectedItem.Text.Trim();

            if (curServicePack.Equals(newServicePack))
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Gói hiện tại và Gói dịch vụ mới giống nhau. Vui lòng thao tác lại."
                });
                return;
            }

            var signUpDate = DateTime.Now;
            var toDate = (DateTime) dtxAddNgay_ket_thuc7number.Value;
            if (newServicePack.ToUpper().Equals("BASIC"))
            {
                toDate = DateTime.MaxValue;
            }
            else
            {
                if (toDate < signUpDate)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig()
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.YESNO,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Ngày kết thúc không được nhỏ hơn ngày đăng ký."
                    });
                    return;
                }
            }

            var custcode = accountNo.Remove(accountNo.Length - 1);
            var valueChange = userSPService.GetChangeMargin(custcode, curServicePack, newServicePack, "", 1);
            if (!valueChange.Status.Equals("000"))
            {
                var dFromDate = signUpDate.ToString("dd/MM/yyyy");
                var dTodate = toDate.ToString("dd/MM/yyyy");
                txtAddNote7number.Value = valueChange.Note;

                if (valueChange.IsConfirm == 1)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo!!!",
                        Message = "" + valueChange.Message + "",
                        Width = 500,
                        Buttons = MessageBox.Button.YESNO,
                        MessageBoxButtonsConfig = new MessageBoxButtonsConfig
                        {
                            Yes = new MessageBoxButtonConfig
                            {
                                Text = "Yes",
                                Handler = "USERMETHOD.ConfrimPack('" + accountNo + "','" + userName + "','" +
                                          curServicePack + "','" + newServicePack + "','" + valueChange.Note + "','" +
                                          dFromDate + "','" + dTodate + "')"
                            },
                            No = new MessageBoxButtonConfig
                            {
                                Text = "No"
                            }
                        },
                        Icon = MessageBox.Icon.QUESTION
                    });
                    return;
                }

                ExtNet.Msg.Show(new MessageBoxConfig()
                {
                    Title = "Thông báo!!!",
                    Message = "" + valueChange.Message + "",
                    Width = 500,
                    Buttons = MessageBox.Button.OK,
                });
                return;
            }

            var note = txtAddNote7number.Text.Trim();
            var addedUser = userSPService.AddUserSP(accountNo, userName, curServicePack, newServicePack, signUpDate,
                toDate, note);
            if (addedUser)
            {
                winAddNewUser.Hide();
                winAddNewUser7number.Hide();
                GridDataBind();
            }
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ConfrimPack(string accountNo, string username, string packCurrent, string packNew, string note,
            string fromDate, string toDate)
        {
            var userSPService = new ChangeUserServicePack();
            DateTime fFromDate = default;
            if (!string.IsNullOrEmpty(fromDate))
                fFromDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);

            DateTime tToDate = default;
            if (!string.IsNullOrEmpty(toDate))
                tToDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);

            var addedUser =
                userSPService.AddUserSP(accountNo, username, packCurrent, packNew, fFromDate, tToDate, note);
            if (addedUser)
            {
                winAddNewUser.Hide();
                winAddNewUser7number.Hide();
                GridDataBind();
            }
        }
    }
}