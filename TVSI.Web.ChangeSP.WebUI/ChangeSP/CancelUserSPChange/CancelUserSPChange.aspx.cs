﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Ext.Net;
using TVSI.Utility;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using TVSI.Web.ChangeSP.WebUI.ChangeSP.ApproveUserSPChange;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.CancelUserSPChange
{
    public partial class CancelUserSPChange : Page
    {
  
        private readonly ChangeUserServicePack _userService;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                dtxSearchFrom.SelectedDate = dtxSearchTo.SelectedDate = DateTime.MinValue;

                var userSPService = new ChangeUserServicePack();
                Logger.Info(typeof(approve_user_service),"b1 -- Model");
                var servicePackList = userSPService.GetSP();

                foreach (var item in servicePackList)
                {
                    cboEditGDV_hien_tai.Items.Add(new ListItem(item, item));
                    cboEditGDV_moi.Items.Add(new ListItem(item, item));
                }
                
                var statusDict = ConstParam.StatusDict;

                cboSearchStatus.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                cboEditTrang_thai.Items.Add(new ListItem("Hủy đăng ký gói", "5"));
                if (statusDict.Count > 0)
                {
                    foreach (var item in statusDict)
                    {
                        cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                        cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    }

                }
                cboSearchStatus.SelectedIndex = 0;
                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                GridDataBind();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            var curStatus = cboSearchStatus.SelectedItem.Value;
            
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "5";
            }
            var userSPService = new ChangeUserServicePack();
            userSPService.ExportUserServicePackListCancel(accountNo, userName, "", curStatus, fromDate, toDate,"","");

        }

        protected void btnApproveAll(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var changeUserServicePack = new ChangeUserServicePack();
            StringBuilder result = new StringBuilder();
            RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;
            var list = sm.SelectedRows;
            var messageBoxNotSelect = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
            };
            if (list.Count == 0)
            {
                ExtNet.Msg.Show(messageBoxNotSelect);
            }

            List<string> listID = new List<string>();
            var changed = false;
            var messageBoxInsertError = new MessageBoxConfig
            {
                Title = "Thông báo",
                Buttons = MessageBox.Button.CANCEL,
                Icon = MessageBox.Icon.ERROR,
                Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
            };
            foreach (SelectedRow row in list)
            {
                changed = changeUserServicePack.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), -2, "");
                if (changed == false)
                {
                    ExtNet.Msg.Show(messageBoxInsertError);
                }
            }

            GridDataBind();
        }


        protected void storeListUserService_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var accountNo = txtSearchAccountNo.Text.Trim();
            var userName = txtSearchUserName.Text.Trim();
            var fromDate = (DateTime)dtxSearchFrom.Value;
            var toDate = (DateTime) dtxSearchTo.Value;
            var curStatus = cboSearchStatus.SelectedItem.Value;
            winDeclineAllUser.Hide();
            var userSPService = new ChangeUserServicePack();
            if (string.IsNullOrEmpty(curStatus))
            {
                curStatus = "5";
            }
            var userInfo = userSPService.SearchUserServicePackListCancel(accountNo, userName, "", curStatus, fromDate,
                toDate,"","");
            grdListUserService.GetStore().DataSource = userInfo;
            grdListUserService.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserSPDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtEditSo_tai_khoan.Text = userInfo.so_tai_khoan;
            txtEditTen_khach_hang.Text = userInfo.ho_ten_khach_hang;
            cboEditTrang_thai.SelectedItem.Value = userInfo.trang_thai.ToString();
            cboEditGDV_hien_tai.SelectedItem.Value = userInfo.goi_dich_vu_hien_tai;
            cboEditGDV_moi.SelectedItem.Value = userInfo.goi_dich_vu_moi;
            dtxEditNgay_dang_ky.Value = userInfo.ngay_dang_ky;
            dtxEditNgay_hieu_luc.Value = userInfo.ngay_hieu_luc;
            dtxEditNgay_ket_thuc.Value = userInfo.ngay_ket_thuc;
            txtEditLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;

            winUserSPDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ApproveUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            winApproveUser.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeclineUserSPChange(long sp_id)
        {
            hidID.Value = sp_id;
            var userSPService = new ChangeUserServicePack();
            var userInfo = userSPService.GetUserSPByID(sp_id);

            txtDecLy_do_tu_choi.Text = userInfo.ly_do_tu_choi;
            winDeclineUser.Show();
        }

        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserSPDetail.Hide();
        }

        public void btnApproveUser_Click(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }
                
                var sp_id = Convert.ToInt32(hidID.Value);
                var userSPService = new ChangeUserServicePack();
                var userInfo = userSPService.GetUserSPByID(sp_id);
                var declineReason = txtDecLy_do_tu_choi.Text.Trim();
                var changed = userSPService.ChangeUserSPStatusByIDCancel(sp_id, -2, declineReason);

                if (changed)
                {
                    winApproveUser.Hide();
                    GridDataBind();
                    var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                    accNumLengString += "1";
          
                    var userName = userInfo.ho_ten_khach_hang;
                    var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);
                    if (!string.IsNullOrEmpty(userEmail.dia_chi_email))
                    {
                        var curSP = userInfo.goi_dich_vu_hien_tai;
                        var newSP = userInfo.goi_dich_vu_moi;
                        var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                        /*Gửi Mail thông báo cho khách hàng đã được duyệt lệnh thành công*/
                        userSPService.SendConfirmEmailToUser(userName, userEmail.dia_chi_email, curSP, newSP, fromDate);
                    }
                    else
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Message = "Không tìm thấy Email cho tài khoản này."
                        });
                    }
                    winDeclineUser.Hide();
                    if (changed)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Message = "Lệnh hủy đăng ký gói thành công."
                        });
                    }
                    GridDataBind();
                }
                else
                {
                 
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Có lỗi xảy ra."
                    });   
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Approve lệnh duyệt thất bại " + ex.Message);
            }
        }

        public void btnDeclineUser_Click(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }
                
                var userSPService = new ChangeUserServicePack();

                var sp_id = Convert.ToInt32(hidID.Value);
                var declineReason = txtDecLy_do_tu_choi.Text.Trim();
                var changed = userSPService.ChangeUserSPStatusByIDCancel(sp_id, 0, declineReason);
                var userInfo = userSPService.GetUserSPByID(sp_id);
                if (changed)
                {
                    var accNumLengString = userInfo.so_tai_khoan.Remove(userInfo.so_tai_khoan.Length - 1);
                    accNumLengString += "1";
                    var userName = userInfo.ho_ten_khach_hang;
                    var userEmail = userSPService.GetUserEmailByAccNo(accNumLengString);
                    if (!string.IsNullOrEmpty(userEmail.dia_chi_email))
                    {
                        var curSP = userInfo.goi_dich_vu_hien_tai;
                        var newSP = userInfo.goi_dich_vu_moi;
                        var lydotuchoi = userInfo.ly_do_tu_choi;
                        var fromDate = ConstParam.AddBusinessDays(DateTime.Now);
                        // gửi mail khi được duyệt lệnh bị từ chối
                        userSPService.SendDeclineEmailToUser(userName, userEmail.dia_chi_email, lydotuchoi);
                    }

                    winDeclineUser.Hide();
                    if (changed)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Message = "Từ chối hủy đăng ký dịch vụ thành công."
                        });
                    }
                    GridDataBind();
                }
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(ChangeUserServicePack), "Reject lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }
        
        public void rejectAll(object sender, EventArgs e)
        {
            try
            {
                
                winDeclineAllUser.Show();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(approve_user_service), "rejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }
        
           public void BtnRejectAll(object sender, EventArgs e)
        {
            try
            {
                var username = SessionHelper.GetUserName();
                if (string.IsNullOrEmpty(username))
                {
                    X.Js.Call("logout");
                    return;
                }
                
                var userSPService = new ChangeUserServicePack();
                StringBuilder result = new StringBuilder();
                RowSelectionModel sm = this.grdListUserService.GetSelectionModel() as RowSelectionModel;
                var list = sm.SelectedRows;
                var lyDoTuChoiAll = txtDecLy_do_tu_choi_all.Text;
                if (list.Count <= 0)
                {
                
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.CANCEL,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Bạn chưa chọn bất kì lệnh nào để duyệt!"
                    });
                    return;
                }
                List<string> listID = new List<string>();
                var changed = false;
                foreach (SelectedRow row in list)
                {
                    changed = userSPService.ChangeUserSPStatusByID(Convert.ToInt64(row.RecordID), 0, lyDoTuChoiAll);
                    if (changed == false)
                    {
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.CANCEL,
                            Icon = MessageBox.Icon.ERROR,
                            Message = "Một lệnh chưa được duyệt đã xảy ra lỗi!"
                        });
                    }
                }
                GridDataBind();
            }
            catch (Exception ex)
            {
                TVSI.Utility.Logger.Error(typeof(approve_user_service), "BtnRejectAll lệnh duyệt thất bại " + ex.Message);
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Có lỗi xảy ra."
                });
            }
        }
        }
    }