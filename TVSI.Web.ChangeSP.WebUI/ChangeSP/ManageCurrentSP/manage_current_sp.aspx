﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="manage_current_sp.aspx.cs" Inherits="TVSI.Web.ChangeSP.WebUI.ChangeSP.ManageCurrentSP.manage_current_sp" %>

<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdSPList(command, data) {
                if (command == "Edit") {
                    USERMETHOD.ViewServicePackDetail(data.goi_dich_vuid);
                }
                if (command == "Delete") {
                    USERMETHOD.DeleteServicePack(data.goi_dich_vuid);
                }
            }

            var renderSPStatus = function (value) {
                if (value == "1")
                    return "Hoạt động"
                if (value == "99")
                    return "Xóa logic"
            };
            var renderStatusPack = function (value) {
                if (value == "1")
                    return "Có"
                if (value == "2")
                    return "Không"
            };

            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
        </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager runat="server" />
    <form id="form2" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="Trạng thái " runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:ComboBox runat="server" ID="cboSearchStatus" Width="100">
                                    <Listeners>
                                        <Select Handler="#{storeListServicePack}.reload()"></Select>
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeListServicePack}.reload()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Thêm mới" ID="btnAdd" Icon="ApplicationAdd" CausesValidation="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddServicePack_Click" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdListServicePack" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeListServicePack" runat="server" OnRefreshData="storeListServicePack_Refresh">
                            <Reader>
                                <ext:JsonReader IDProperty="goi_dich_vuid">
                                    <Fields>
                                        <ext:RecordField Name="goi_dich_vuid" />
                                        <ext:RecordField Name="ten_goi_dich_vu" />
                                        <ext:RecordField Name="lai_suat" />
                                        <ext:RecordField Name="lai_suat_van_hanh" />
                                        <ext:RecordField Name="lai_suat_bang_chu" />
                                        <ext:RecordField Name="adjust_initial_rate" />
                                        <ext:RecordField Name="adjust_call_rate" />
                                        <ext:RecordField Name="adjust_force_sell_rate" />
                                        <ext:RecordField Name="cap_nhat_suc_mua" />
                                        <ext:RecordField Name="so_ngay_mien_lai" />
                                        <ext:RecordField Name="ty_le_ky_quy" />
                                        <ext:RecordField Name="ty_le_duy_tri" />
                                        <ext:RecordField Name="ty_le_call" />
                                        <%--
                                        <ext:RecordField Name="ty_le_canh_bao" />
                                        --%>
                                        <ext:RecordField Name="ty_le_vay" />
                                        <ext:RecordField Name="du_no_toi_da" />
                                        <ext:RecordField Name="trang_thai" />
                                        <ext:RecordField Name="nguoi_tao" />
                                        <ext:RecordField Name="ngay_tao" />
                                        <ext:RecordField Name="nguoi_cap_nhat" />
                                        <ext:RecordField Name="ngay_cap_nhat" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="20" Header="#" />
                            <ext:Column DataIndex="goi_dich_vuid" Header="GDV ID" Width="50" />
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="ApplicationEdit" CommandName="Edit" Text="Edit" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="70" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Text="Delete" />
                                </Commands>
                            </ext:CommandColumn>
                            <ext:Column DataIndex="ten_goi_dich_vu" Header="Tên GDV" Width="60" />
                            <ext:Column DataIndex="lai_suat" Header="Lãi suất mặc định" Width="80" />
                            <ext:Column DataIndex="lai_suat_van_hanh" Header="Lãi suất vận hành" Width="80" />
                            <ext:Column DataIndex="lai_suat_bang_chu" Header="Lãi suất bằng chữ" Width="80" />
                            <ext:Column DataIndex="cap_nhat_suc_mua" Header="Cấp sức mua trong ngày" Width="80" >
                                 <Renderer Fn="renderStatusPack"></Renderer>
                            </ext:Column>
                            <ext:Column DataIndex="so_ngay_mien_lai" Header="Số ngày miễn lãi" Width="80"/>
                            <ext:Column DataIndex="ty_le_duy_tri" Header="TLKQ cảnh báo" Width="80"/>
                            <ext:Column DataIndex="ty_le_ky_quy" Header="TLKQ cơ bản" Width="80"/>
                            <ext:Column DataIndex="ty_le_call" Header="TLKQ xử lý" Width="80"/>
                            <ext:Column DataIndex="adjust_initial_rate" Header="Adjust Initial Rate" Width="80"/>
                            <ext:Column DataIndex="adjust_call_rate" Header="Adjust Call Rate" Width="80"/>
                            <ext:Column DataIndex="adjust_force_sell_rate" Header="Adjust Force Sell Rate" Width="80"/>
                            <%--
                            <ext:Column DataIndex="ty_le_canh_bao" Header="Tỷ lệ ký quỹ cảnh báo" Width="80" />
                            --%>
                            <ext:Column DataIndex="ty_le_vay" Header="Tỷ lệ vay tối đa" Width="80" />
                            <ext:Column DataIndex="du_no_toi_da" Header="Dư nợ tối đa 1 tài khoản" Width="80" />
                            <ext:Column DataIndex="trang_thai" Header="Trạng thái" Width="80">
                                <Renderer Fn="renderSPStatus"></Renderer>
                            </ext:Column>
                            <ext:DateColumn DataIndex="ngay_tao" Header="Ngày tạo" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_tao" Header="Người tạo" Width="120" />
                            <ext:DateColumn DataIndex="ngay_cap_nhat" Header="Ngày cập nhật" Width="120" Format="dd/MM/yyyy HH:mm:ss" />
                            <ext:Column DataIndex="nguoi_cap_nhat" Header="Người cập nhật" Width="120" />
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdSPList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window runat="server" ID="winServicePackDetail" Title="Yêu cầu chỉnh sửa gói dịch vụ" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="500" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditService">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditTen_goi_dich_vu" Width="200" FieldLabel="Tên gói dịch vụ" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditLai_suat" Width="200" FieldLabel="Lãi suất mặc định" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditLai_suat_van_hanh" Width="200" FieldLabel="Lãi suất vận hành" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditLai_suat_bang_chu" Width="200" FieldLabel="Lãi suất bằng chữ" AllowBlank="False"/>
                        <ext:ComboBox runat="server" ID="txtEditCap_nhat_suc_mua" Width="200" FieldLabel="Cấp sức mua trong ngày" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditSo_ngay_mien_lai" Width="200" FieldLabel="Số ngày miễn lãi" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditTy_le_duy_tri" Width="200" FieldLabel="Tỷ lệ ký quỹ cảnh báo" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditTy_le_ky_quy" Width="200" FieldLabel="Tỷ lệ ký quỹ cơ bản" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditTy_le_call" Width="200" FieldLabel="Tỷ lệ ký quỹ xử lý" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditAdjust_initial_rate" Width="200" FieldLabel="Adjust Initial Rate" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditAdjust_call_rate" Width="200" FieldLabel="Adjust Call Rate" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtEditAdjust_force_sell_rate" Width="200" FieldLabel="Adjust Force Sell Rate" AllowBlank="False"/>
                        <%--
                        <ext:TextField runat="server" ID="txtEditTy_le_canh_bao" Width="200" FieldLabel="Tỷ lệ ký quỹ cảnh báo" AllowBlank="False" />
                        --%>
                        <ext:TextField runat="server" ID="txtEditTy_le_vay" Width="200" FieldLabel="Tỷ lệ vay tối đa" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtEditDu_no_toi_da" Width="200" FieldLabel="Dư nợ tối đa 1 Tài khoản" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="cboEditTrang_thai" Width="200" FieldLabel="Trạng thái" AllowBlank="False" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Cập nhật" Icon="ApplicationEdit">
                            <Listeners>
                                <Click Handler="if(#{frmEditService}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnEditUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật GDV này không?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winAddNewServicePack" Title="Thêm gói dịch vụ"
            Hidden="True" Icon="Application" Height="500" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmAddService">
                    <Items>
                        <ext:TextField runat="server" ID="txtAddTen_goi_dich_vu" Width="200" FieldLabel="Tên gói dịch vụ" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddLai_suat" Width="200" FieldLabel="Lãi suất mặc định" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddLai_suat_van_hanh" Width="200" FieldLabel="Lãi suất vận hành" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtAddLai_suat_bang_chu" Width="200" FieldLabel="Lãi suất bằng chữ" AllowBlank="False"/>
                        <ext:ComboBox runat="server"  ID="txtAddCap_nhat_suc_mua" Width="200" FieldLabel="Cấp sức mua trong ngày" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddSo_ngay_mien_lai" Width="200" FieldLabel="Số ngày miễn lãi" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddTy_le_duy_tri" Width="200" FieldLabel="Tỷ lệ ký quỹ cảnh báo" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddTy_le_ky_quy" Width="200" FieldLabel="Tỷ lệ ký quỹ cơ bản" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddTy_le_call" Width="200" FieldLabel="Tỷ lệ ký quỹ xử lý" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddAdjust_initial_rate" Width="200" FieldLabel="Adjust Initial Rate" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtAddAdjust_call_rate" Width="200" FieldLabel="Adjust Call Rate" AllowBlank="False"/>
                        <ext:TextField runat="server" ID="txtAddAdjust_force_sell_rate" Width="200" FieldLabel="Adjust Force Sell Rate" AllowBlank="False"/>
                        <%--
                        <ext:TextField runat="server" ID="txtAddTy_le_canh_bao" Width="200" FieldLabel="Tỷ lệ ký quỹ cảnh báo" AllowBlank="False" />
                        --%>
                        <ext:TextField runat="server" ID="txtAddTy_le_vay" Width="200" FieldLabel="Tỷ lệ vay tối đa" AllowBlank="False" />
                        <ext:TextField runat="server" ID="txtAddDu_no_toi_da" Width="200" FieldLabel="Dư nợ tối đa 1 tài khoản" AllowBlank="False" />
                        <ext:ComboBox runat="server" ID="cboAddTrang_thai" Width="200" FieldLabel="Trạng thái" ReadOnly="True" />
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" ID="btnConfAddServicePack" Text="Thêm mới" Icon="ApplicationAdd">
                            <Listeners>
                                <Click Handler="if(#{frmAddService}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ thông tin', buttons:Ext.Msg.OK}); return false;}" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="btnConfAddServicePack_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý thêm GDV này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeleteServicePack" Title="Xóa GDV"
            Hidden="True" Icon="Application" Height="300" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmDecService">
                    <Buttons>
                        <ext:Button runat="server" Text="Xóa GDV" Icon="Delete">
                            <DirectEvents>
                                <Click OnEvent="btnDeleteServicePack_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý với việc xóa GDV này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>
    </form>
</body>
</html>