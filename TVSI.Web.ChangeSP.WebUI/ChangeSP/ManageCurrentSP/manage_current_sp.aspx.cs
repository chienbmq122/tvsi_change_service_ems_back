﻿using Ext.Net;
using System;
using TVSI.Web.ChangeSP.Lib.Service;
using TVSI.Web.ChangeSP.Lib.Utility;
using ListItem = Ext.Net.ListItem;

namespace TVSI.Web.ChangeSP.WebUI.ChangeSP.ManageCurrentSP
{
    public partial class manage_current_sp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var statusDict = ConstParam.ServicePackStatusDict;

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                foreach (var item in statusDict)
                {
                    cboAddTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboEditTrang_thai.Items.Add(new ListItem(item.Value, item.Key));
                    cboSearchStatus.Items.Add(new ListItem(item.Value, item.Key));
                }        
                var statusDictDay = ConstParam.PurChasingPowerOfTheDay;

                cboSearchStatus.Items.Add(new ListItem("--Tất cả--", "--Tất cả--"));
                foreach (var item in statusDictDay)
                {
                    txtAddCap_nhat_suc_mua.Items.Add(new ListItem(item.Value, item.Key));
                    txtEditCap_nhat_suc_mua.Items.Add(new ListItem(item.Value, item.Key));
                }

                cboAddTrang_thai.SelectedIndex = 0;
                cboSearchStatus.SelectedIndex = 1;

                GridDataBind();
            }
        }

        protected void storeListServicePack_Refresh(object sender, EventArgs e)
        {
            GridDataBind();
        }

        private void GridDataBind()
        {
            var status = cboSearchStatus.SelectedItem.Value;

            var servicePackService = new ManageServicePack();
            var servicePackInfo = servicePackService.SearchServicePackList(status);

            grdListServicePack.GetStore().DataSource = servicePackInfo;
            grdListServicePack.GetStore().DataBind();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewServicePackDetail(long sp_id)
        {
            hidID.Value = sp_id;
            var servicePackService = new ManageServicePack();
            var servicePackInfo = servicePackService.GetServicePackByID(sp_id);

            txtEditTen_goi_dich_vu.Text = servicePackInfo.ten_goi_dich_vu;
            txtEditLai_suat.Text = servicePackInfo.lai_suat.ToString();
            txtEditLai_suat_van_hanh.Text = servicePackInfo.lai_suat_van_hanh.ToString();
            txtEditLai_suat_bang_chu.Text = servicePackInfo.lai_suat_bang_chu;
            txtEditCap_nhat_suc_mua.Text = servicePackInfo.cap_nhat_suc_mua.ToString();
            txtEditSo_ngay_mien_lai.Text = servicePackInfo.so_ngay_mien_lai.ToString();
            txtEditTy_le_ky_quy.Text = servicePackInfo.ty_le_ky_quy.ToString();
            txtEditTy_le_duy_tri.Text = servicePackInfo.ty_le_duy_tri.ToString();
            txtEditTy_le_call.Text = servicePackInfo.ty_le_call.ToString();
            txtEditAdjust_initial_rate.Text = servicePackInfo.adjust_initial_rate.ToString();
            txtEditAdjust_call_rate.Text = servicePackInfo.adjust_call_rate.ToString();
            txtEditAdjust_force_sell_rate.Text = servicePackInfo.adjust_force_sell_rate.ToString();
            txtEditTy_le_vay.Text = servicePackInfo.ty_le_vay.ToString();
            txtEditDu_no_toi_da.Text = servicePackInfo.du_no_toi_da;
            cboEditTrang_thai.SelectedItem.Value = servicePackInfo.trang_thai.ToString();

            winServicePackDetail.Show();
        }

        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeleteServicePack(long sp_id)
        {
            hidID.Value = sp_id;
            winDeleteServicePack.Show();
        }

        public void btnEditUser_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var sp_id = int.Parse(hidID.Value + "");

            var spName = txtEditTen_goi_dich_vu.Text.Trim();
            
            var laisuat  = txtEditLai_suat.Text.Trim();
            var capnhatsucmua = txtEditCap_nhat_suc_mua.Text.Trim();
            var songaymienlai = txtEditSo_ngay_mien_lai.Text.Trim();
            var tylekyquy = txtEditTy_le_ky_quy.Text.Trim();
            var tyleduytri = txtEditTy_le_duy_tri.Text.Trim();
            var tylevay = txtEditTy_le_vay.Text.Trim();
            var callRate = txtEditTy_le_call.Text.Trim();
            var Dunotoida = txtEditDu_no_toi_da.Text.Trim();

            var status = int.Parse(cboEditTrang_thai.SelectedItem.Value);
            
            var lai_suat_van_hanh = txtEditLai_suat_van_hanh.Text.Trim();
            var lai_suat_bang_chu = txtEditLai_suat_bang_chu.Text.Trim();
            var adjust_initial_rate = txtEditAdjust_initial_rate.Text.Trim();
            var adjust_call_rate = txtEditAdjust_call_rate.Text.Trim();
            var adjust_force_sell_rate = txtEditAdjust_force_sell_rate.Text.Trim();

            double laisuatd = 0;
            double capnhatsucmuad = 0;
            double songaymienlaid = 0;
            double tylekyquyd = 0;
            double tyleduytrid = 0;
            double tylecanhbaod = 0;
            double tylevayd = 0;
            double dCallRate = 0;
            double d_lai_suat_van_hanh = 0;
            double d_adjust_initial_rate = 0;
            double d_adjust_call_rate = 0;
            double d_adjust_force_sell_rate = 0;

            if (!Double.TryParse(laisuat, out laisuatd) 
                || !Double.TryParse(callRate, out dCallRate) 
                || !Double.TryParse(capnhatsucmua, out capnhatsucmuad)
                || !Double.TryParse(songaymienlai, out songaymienlaid) 
                || !Double.TryParse(tylekyquy, out tylekyquyd)
                || !Double.TryParse(tyleduytri, out tyleduytrid)
                || !Double.TryParse(tylevay, out tylevayd)
                || !Double.TryParse(lai_suat_van_hanh, out d_lai_suat_van_hanh)
                || !Double.TryParse(adjust_initial_rate, out d_adjust_initial_rate)
                || !Double.TryParse(adjust_call_rate, out d_adjust_call_rate)
                || !Double.TryParse(adjust_force_sell_rate, out d_adjust_force_sell_rate))
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "vui lòng nhập kiểu số, ngoại trừ tên Gói dịch vụ."
                });
                return;
            }

            var servicePackService = new ManageServicePack();

            var editedUser = servicePackService.UpdateServicePackByID(sp_id, spName, laisuatd, capnhatsucmuad, songaymienlaid,
                tylekyquyd,tyleduytrid,dCallRate, tylecanhbaod, tylevayd, Dunotoida, status,
                lai_suat_bang_chu, d_lai_suat_van_hanh, d_adjust_initial_rate, d_adjust_call_rate, d_adjust_force_sell_rate);
            if (editedUser)
            {
                var addlog = servicePackService.AddLogSP(spName, status,laisuatd,capnhatsucmuad
                ,songaymienlaid,tylekyquyd,tyleduytrid,dCallRate,tylecanhbaod,tylevayd,Dunotoida,2,
                lai_suat_bang_chu, d_lai_suat_van_hanh, d_adjust_initial_rate, d_adjust_call_rate, d_adjust_force_sell_rate);
                winServicePackDetail.Hide();
                GridDataBind();
            }
        }

        public void btnAddServicePack_Click(object sender, EventArgs e)
        {
            txtAddTen_goi_dich_vu.Text = "";
            txtAddLai_suat.Text = "";
            txtAddCap_nhat_suc_mua.Text = "";
            txtAddSo_ngay_mien_lai.Text = "";
            txtAddTy_le_ky_quy.Text = "";
            txtAddTy_le_duy_tri.Text = "";
            txtAddTy_le_ky_quy.Text = "";
            txtAddTy_le_call.Text = "";
            txtAddTy_le_vay.Text = "";
            cboAddTrang_thai.SelectedIndex = 0;
            winAddNewServicePack.Show();
        }

        public void btnConfAddServicePack_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var servicePackService = new ManageServicePack();
            var spName = txtAddTen_goi_dich_vu.Text.Trim();

            var servicePackExisted = servicePackService.CheckExistingSPByName(spName);

            if (servicePackExisted)
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "Một GDV với tên này đã tồn tại"
                });
                return;
            }

            var laisuat  = txtAddLai_suat.Text.Trim();
            var capnhatsucmua = txtAddCap_nhat_suc_mua.Text.Trim();
            var songaymienlai = txtAddSo_ngay_mien_lai.Text.Trim();
            var tylekyquy = txtAddTy_le_ky_quy.Text.Trim();
            var tyleduytri = txtAddTy_le_duy_tri.Text.Trim();
            var tylevay = txtAddTy_le_vay.Text.Trim();
            var callRate = txtAddTy_le_call.Text.Trim();
            var Dunotoida = txtAddDu_no_toi_da.Text.Trim();
            
            var lai_suat_van_hanh = txtAddLai_suat_van_hanh.Text.Trim();
            var lai_suat_bang_chu = txtAddLai_suat_bang_chu.Text.Trim();
            var adjust_initial_rate = txtAddAdjust_initial_rate.Text.Trim();
            var adjust_call_rate = txtAddAdjust_call_rate.Text.Trim();
            var adjust_force_sell_rate = txtAddAdjust_force_sell_rate.Text.Trim();

            double laisuatd = 0;
            double capnhatsucmuad = 0;
            double songaymienlaid = 0;
            double tylekyquyd = 0;
            double tyleduytrid = 0;
            double tylecanhbaod = 0;
            double tylevayd = 0;
            double dCallRate = 0;
            double d_lai_suat_van_hanh = 0;
            double d_adjust_initial_rate = 0;
            double d_adjust_call_rate = 0;
            double d_adjust_force_sell_rate = 0;

            if (!Double.TryParse(laisuat, out laisuatd) 
                || !Double.TryParse(callRate, out dCallRate) || !Double.TryParse(capnhatsucmua, out capnhatsucmuad)
                || !Double.TryParse(songaymienlai, out songaymienlaid) 
                || !Double.TryParse(tylekyquy, out tylekyquyd)
                || !Double.TryParse(tyleduytri, out tyleduytrid)
                || !Double.TryParse(tylevay, out tylevayd) 
                || !Double.TryParse(lai_suat_van_hanh, out d_lai_suat_van_hanh)
                || !Double.TryParse(adjust_initial_rate, out d_adjust_initial_rate)
                || !Double.TryParse(adjust_call_rate, out d_adjust_call_rate)
                || !Double.TryParse(adjust_force_sell_rate, out d_adjust_force_sell_rate))
            {
                ExtNet.Msg.Show(new MessageBoxConfig
                {
                    Title = "Thông báo",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Message = "vui lòng nhập kiểu số, ngoại trừ tên Gói dịch vụ."
                });
                return;
            }

            var addedUser = servicePackService.AddServicePack(spName, laisuatd, capnhatsucmuad,
                songaymienlaid,tylekyquyd,tyleduytrid, dCallRate,
                tylecanhbaod,tylevayd,Dunotoida,
                lai_suat_bang_chu, d_lai_suat_van_hanh, d_adjust_initial_rate, d_adjust_call_rate, d_adjust_force_sell_rate);

            if (addedUser)
            {
                winAddNewServicePack.Hide();
                GridDataBind();
            }
        }

        public void btnDeleteServicePack_Click(object sender, EventArgs e)
        {
            var username = SessionHelper.GetUserName();
            if (string.IsNullOrEmpty(username))
            {
                X.Js.Call("logout");
                return;
            }
            
            var servicePackService = new ManageServicePack();
            var sp_id = int.Parse(hidID.Value + "");
            var changed = servicePackService.ChangeSPStatusByID(sp_id, 99);
            var getdetail = servicePackService.GetServicePackByID(sp_id);
            if (getdetail != null)
            {
                servicePackService.AddLogSP(getdetail.ten_goi_dich_vu, 99, getdetail.lai_suat,
                    getdetail.cap_nhat_suc_mua,
                    getdetail.so_ngay_mien_lai, getdetail.ty_le_ky_quy, getdetail.ty_le_duy_tri, getdetail.ty_le_call,
                    getdetail.ty_le_canh_bao, getdetail.ty_le_vay, getdetail.du_no_toi_da,99,
                    getdetail.lai_suat_bang_chu, getdetail.lai_suat_van_hanh, getdetail.adjust_initial_rate, getdetail.adjust_call_rate, getdetail.adjust_force_sell_rate);

            }
            if (changed)
            { 
                winDeleteServicePack.Hide();
                GridDataBind();
            }
        }
    }
}