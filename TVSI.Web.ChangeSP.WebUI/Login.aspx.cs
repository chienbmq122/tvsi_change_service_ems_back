﻿using System;
using TVSI.Web.ChangeSP.Lib.Utility;

namespace TVSI.Web.ChangeSP.WebUI
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var userName = txtUserName.Text.Trim();
            SessionHelper.CreateSession(userName);
            Response.Redirect("~/Menu.aspx");
        }
    }
}